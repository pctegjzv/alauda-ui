import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/angular';
import { ButtonModule, MessageModule } from '../../src/public-api';
import { DemoMessageComponent } from './demo-message.component';

storiesOf('Message', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [MessageModule, BrowserAnimationsModule, ButtonModule],
      declarations: [DemoMessageComponent],
    },
    component: DemoMessageComponent,
    props: {
      action: action('after closed result: '),
    },
  };
});
