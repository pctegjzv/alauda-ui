import { Component, OnDestroy } from '@angular/core';
import { MessageService } from '../../src/public-api';

@Component({
  templateUrl: './demo-message.component.html',
})
export class DemoMessageComponent implements OnDestroy {
  constructor(private messageService: MessageService) {}

  test01() {
    const messageRef = this.messageService.success({ content: 'test' });
    messageRef.instance.beforeClosed.subscribe(() => {});
    messageRef.instance.afterClosed.subscribe(() => {});
  }

  ngOnDestroy(): void {
    this.messageService.messagesFillDatas.forEach(ref => {
      ref.destroy();
    });
    this.messageService.wrapperContainer.elementRef.nativeElement.remove();
  }
}
