import {
  boolean,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { ComponentSize, IconModule, TagModule } from '../../src/public-api';

storiesOf('Tag', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const sizeOptions = {
      [ComponentSize.Large]: ComponentSize.Large,
      [ComponentSize.Medium]: ComponentSize.Medium,
      [ComponentSize.Small]: ComponentSize.Small,
      [ComponentSize.Mini]: ComponentSize.Mini,
    };
    const size = select('size', sizeOptions, ComponentSize.Medium);

    const solid = boolean('solid', false);
    const closeable = boolean('closeable', true);
    const invalid = boolean('invalid', false);
    const round = boolean('round', true);

    return {
      moduleMetadata: {
        imports: [TagModule, IconModule],
      },
      template: `
        <div>
            <aui-tag type="primary" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              <aui-icon icon="check_s"></aui-icon>
              primary
            </aui-tag>
            <aui-tag type="success" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              success
            </aui-tag>
            <aui-tag type="warning" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              warning
            </aui-tag>
            <aui-tag type="error" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              error
            </aui-tag>
            <aui-tag type="info" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              info
            </aui-tag>
            <aui-tag type="info" [size]="size" [solid]="solid" [closeable]="closeable" [invalid]="invalid" [round]="round">
              <a href="javascript:;">link</a>
            </aui-tag>
        </div>`,
      props: {
        size,
        solid,
        closeable,
        invalid,
        round,
      },
    };
  })
  .add('custom color', () => {
    const color = text('color', '#7c70e2,#f2f1fd');
    return {
      moduleMetadata: {
        imports: [TagModule],
      },
      template: `
        <aui-tag [color]="color">custom color</aui-tag>`,
      props: {
        color,
      },
    };
  })
  .add('check tag', () => {
    const checked1 = false;
    const checked2 = true;
    const checked3 = false;
    return {
      moduleMetadata: {
        imports: [TagModule],
      },
      template: `
        <aui-check-tag [(checked)]="checked1">
          check tag
        </aui-check-tag>checked: {{checked1}}<br>
        <aui-check-tag [(checked)]="checked2">
          check tag
        </aui-check-tag>checked: {{checked2}}<br>
        <aui-check-tag [(checked)]="checked3">
          check tag
        </aui-check-tag>checked: {{checked3}}<br>`,
      props: {
        checked1,
        checked2,
        checked3,
      },
    };
  });
