import {
  boolean,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import {
  ButtonModule,
  CheckboxModule,
  FormModule,
  IconModule,
  InputModule,
  LabelPosition,
  RadioModule,
  SelectModule,
  SwitchModule,
} from '../../src/public-api';
import { DemoFormComponent } from './demo-form.component';

storiesOf('Form', module)
  .addDecorator(withKnobs)
  .add('form item', () => {
    const labelWidth = text('labelWidth', '100px');

    const labelPositionOptions = {
      [LabelPosition.Right]: LabelPosition.Right,
      [LabelPosition.Left]: LabelPosition.Left,
      [LabelPosition.Top]: LabelPosition.Top,
    };

    const labelPosition = select(
      'labelPosition',
      labelPositionOptions,
      LabelPosition.Right,
    );

    const inline = boolean('inline', false);

    return {
      moduleMetadata: {
        imports: [
          FormModule,
          InputModule,
          IconModule,
          SelectModule,
          ButtonModule,
          SwitchModule,
          RadioModule,
          CheckboxModule,
        ],
        declarations: [DemoFormComponent],
      },
      component: DemoFormComponent,
      props: {
        labelWidth,
        labelPosition,
        inline,
      },
    };
  });
