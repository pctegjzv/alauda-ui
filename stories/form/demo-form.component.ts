import { Component } from '@angular/core';

@Component({
  templateUrl: './demo-form.component.html',
  preserveWhitespaces: false,
})
export class DemoFormComponent {}
