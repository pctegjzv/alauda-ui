import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/angular';
import { InlineAlertModule } from '../../src/public-api';
import { DemoInlineAlertComponent } from './demo-inline-alert.component';

storiesOf('Inline Alert', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [InlineAlertModule, BrowserAnimationsModule],
      declarations: [DemoInlineAlertComponent],
    },
    component: DemoInlineAlertComponent,
    props: {
      action: action('after closed result: '),
    },
  };
});
