import { Component } from '@angular/core';

@Component({
  templateUrl: './demo-inline-alert.component.html',
  styles: [],
})
export class DemoInlineAlertComponent {
  title = 'demo-title';
  content = 'demo-content';

  showToast() {
    alert('clicked!');
  }

  onClosed() {
    alert('closed!');
  }
}
