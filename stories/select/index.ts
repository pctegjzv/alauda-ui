import { FormsModule } from '@angular/forms';
import {
  boolean,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import {
  ComponentSize,
  OptionComponent,
  SelectModule,
} from '../../src/public-api';

storiesOf('Select', module)
  .addDecorator(withKnobs)
  .add('select', () => {
    const value = { key: 'option12' };

    const trackFn = (val: any) => (val && val.key) || val;

    const sizeOptions = {
      [ComponentSize.Large]: ComponentSize.Large,
      [ComponentSize.Medium]: ComponentSize.Medium,
      [ComponentSize.Small]: ComponentSize.Small,
      [ComponentSize.Mini]: ComponentSize.Mini,
    };
    const size = select('size', sizeOptions, ComponentSize.Medium);
    const placeholder = text('placeholder', 'placeholder');

    const disabled = boolean('disabled', false);
    const loading = boolean('loading', false);
    const filterable = boolean('filterable', true);
    const clearable = boolean('clearable', true);
    const allowCreate = boolean('allowCreate', true);
    return {
      moduleMetadata: {
        imports: [SelectModule, FormsModule],
      },
      template: `
        <div>
          <aui-select
            [(ngModel)]="value"
            includes [size]="size"
            [clearable]="clearable"
            [disabled]="disabled"
            [loading]="loading"
            [filterable]="filterable"
            [allowCreate]="allowCreate"
            [placeholder]="placeholder"
            [trackFn]="trackFn">
              <aui-option-group>
                <div auiOptionGroupTitle>group 1</div>
                <aui-option label="option1" [value]="{key:'option1'}">option1</aui-option>
                <aui-option label="option2" [value]="{key:'option2'}">option2</aui-option>
                <aui-option label="option3" [value]="{key:'option3'}">option3</aui-option>
              </aui-option-group>
              <aui-option-group>
                <div auiOptionGroupTitle>group 2</div>
                <aui-option label="option4" [value]="{key:'option4'}">option4</aui-option>
                <aui-option label="option5" [value]="{key:'option5'}">option5</aui-option>
                <aui-option label="option6" [value]="{key:'option6'}">option6</aui-option>
                <aui-option label="option7" [value]="{key:'option7'}">option7</aui-option>
              </aui-option-group>
              <aui-option-group>
                <aui-option label="option8" [value]="{key:'option8'}">option8</aui-option>
                <aui-option label="option9" [value]="{key:'option9'}">option9</aui-option>
                <aui-option label="option10" [value]="{key:'option10'}">option10</aui-option>
                <aui-option label="option11" [value]="{key:'option11'}">option11</aui-option>
                <aui-option label="option12" [value]="{key:'option12'}">option12</aui-option>
                <aui-option label="option13" [value]="{key:'option13'}">option13</aui-option>
                <aui-option label="option14" [value]="{key:'option14'}">option14</aui-option>
                <aui-option label="option15" [value]="{key:'option15'}">option15</aui-option>
              </aui-option-group>
            <aui-option-placeholder>Empty</aui-option-placeholder>
          </aui-select>
          value: {{ value | json }}
        </div>`,
      props: {
        value,
        size,
        disabled,
        loading,
        placeholder,
        clearable,
        filterable,
        allowCreate,
        trackFn,
      },
    };
  })
  .add('multi-select', () => {
    const sizeOptions = {
      [ComponentSize.Large]: ComponentSize.Large,
      [ComponentSize.Medium]: ComponentSize.Medium,
      [ComponentSize.Small]: ComponentSize.Small,
      [ComponentSize.Mini]: ComponentSize.Mini,
    };

    const value: any[] = [{ key: 'option1' }, { key: 'option3eeeee' }];
    const trackFn = (val: any) => (val && val.key) || val;
    const filterFn = (filter: string, option: OptionComponent) =>
      (option.value.key || option.value).includes(filter);
    const tagClassFn = (label: string, tag: any) => {
      return `tag-${tag.key}`;
    };

    const size = select('size', sizeOptions, ComponentSize.Medium);
    const placeholder = text('placeholder', 'placeholder');

    const disabled = boolean('disabled', false);
    const loading = boolean('loading', false);
    const filterable = boolean('filterable', true);
    const clearable = boolean('clearable', true);
    const allowCreate = boolean('allowCreate', true);
    return {
      moduleMetadata: {
        imports: [SelectModule, FormsModule],
      },
      template: `
        <div>
          <aui-multi-select
            [(ngModel)]="value"
            required
            maxlength="4"
            minlength="2"
            [clearable]="clearable"
            [size]="size"
            [placeholder]="placeholder"
            [disabled]="disabled"
            [loading]="loading"
            [filterable]="filterable"
            [allowCreate]="allowCreate"
            [trackFn]="trackFn"
            [filterFn]="filterFn"
            [tagClassFn]="tagClassFn">
            <aui-option [value]="{key:'option1'}">option1</aui-option>
            <aui-option [value]="{key:'option2'}">option2</aui-option>
            <aui-option [value]="{key:'option3'}">option3</aui-option>
            <aui-option [value]="{key:'option4'}">option4</aui-option>
            <aui-option [value]="{key:'option5'}">option5</aui-option>
            <aui-option [value]="{key:'option6'}">option6</aui-option>
            <aui-option [value]="{key:'option7'}">option7</aui-option>
            <aui-option [value]="{key:'option8'}">option8</aui-option>
            <aui-option-placeholder>无</aui-option-placeholder>
          </aui-multi-select>
          value: {{ value | json }}
        </div>`,
      props: {
        size,
        value,
        placeholder,
        disabled,
        loading,
        clearable,
        filterable,
        allowCreate,
        trackFn,
        filterFn,
        tagClassFn,
      },
    };
  });
