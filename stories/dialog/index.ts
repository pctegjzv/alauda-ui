import { storiesOf } from '@storybook/angular';
import { ButtonModule, DialogModule } from '../../src/public-api';
import { DemoDialogComponent } from './demo-dialog.component';

storiesOf('Dialog', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [ButtonModule, DialogModule],
      declarations: [DemoDialogComponent],
    },
    component: DemoDialogComponent,
  };
});
