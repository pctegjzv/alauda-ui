import { Component, OnDestroy, TemplateRef } from '@angular/core';
import { action } from '@storybook/addon-actions';
import {
  ConfirmType,
  DialogRef,
  DialogService,
  DialogSize,
} from '../../src/public-api';

@Component({
  templateUrl: './demo-dialog.component.html',
})
export class DemoDialogComponent implements OnDestroy {
  dialogRefs: Array<DialogRef<any>> = [];

  constructor(private dialogService: DialogService) {}

  showDialog(templateRef: TemplateRef<any>) {
    const dialogRef = this.dialogService.open(templateRef, null);

    dialogRef.afterClosed().subscribe(result => {
      action('afterClosed:')(result);
    });

    dialogRef.afterClosed().subscribe(() => {
      const index = this.dialogRefs.indexOf(dialogRef);
      this.dialogRefs.splice(index, 1);
    });

    this.dialogRefs.push(dialogRef);
  }

  openNewDialog(templateRef: TemplateRef<any>) {
    this.dialogService.open(templateRef, {
      size: DialogSize.Small,
    });
  }

  confirmDialog() {
    this.dialogService
      .confirm({
        title: 'title',
        content: 'content',
        confirmType: ConfirmType.Danger,
        beforeConfirm: (reslove, reject) => {
          action('beforeConfirm')();
          setTimeout(reslove, 2000);
        },
      })
      .then(() => {
        action('confirm')();
      })
      .catch(() => {
        action('cancel')();
      });
  }

  ngOnDestroy() {
    this.dialogRefs.forEach(ref => {
      ref.close(false);
    });
  }
}
