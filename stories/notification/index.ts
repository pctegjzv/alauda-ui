import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/angular';
import {
  NOTIFICATION_CONFIG,
  NotificationModule,
} from '../../src/notification/public-api';
import { ButtonModule } from '../../src/public-api';
import { DemoNotificationComponent } from './demo-notification.component';

storiesOf('Notification', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [NotificationModule, BrowserAnimationsModule, ButtonModule],
      declarations: [DemoNotificationComponent],
      providers: [
        {
          provide: NOTIFICATION_CONFIG,
          useValue: {
            duration: 3000,
            maxStack: 3,
          },
        },
      ],
    },
    component: DemoNotificationComponent,
    props: {
      action: action('after closed result: '),
    },
  };
});
