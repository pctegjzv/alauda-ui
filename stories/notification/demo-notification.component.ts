import { Component, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { NotificationService } from '../../src/notification/public-api';

@Component({
  templateUrl: './demo-notification.component.html',
})
export class DemoNotificationComponent implements OnDestroy {
  @ViewChild('template') template: TemplateRef<any>;

  constructor(private notificationService: NotificationService) {}

  test01() {
    const notificationRef = this.notificationService.success({
      contentRef: this.template,
    });
    notificationRef.instance.beforeClosed.subscribe(() => {});
    notificationRef.instance.afterClosed.subscribe(() => {});
  }

  ngOnDestroy(): void {
    this.notificationService.messagesFillDatas.forEach(ref => {
      ref.destroy();
    });
    this.notificationService.wrapperContainer.elementRef.nativeElement.remove();
  }
}
