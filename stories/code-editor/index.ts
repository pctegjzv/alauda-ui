import { object, text, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { CodeEditorModule } from '../../src/public-api';

const exampleCode = `
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: db
    image: mysql
    env:
    - name: MYSQL_ROOT_PASSWORD
      value: "password"
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
  - name: wp
    image: wordpress
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
`;

storiesOf('Code Editor', module)
  .addDecorator(withKnobs)
  .add('monaco editor', () => {
    const model = exampleCode;
    return {
      moduleMetadata: {
        imports: [
          CodeEditorModule.forRoot({
            baseUrl: '',
            defaultOptions: {},
          }),
        ],
      },
      template: `
      <h1>原始代码</h1>
      <textarea cols="80" rows="10" [(ngModel)]="model">{{ exampleCode }}</textarea>
      <h1>编辑器</h1>
      <aui-monaco-editor style="height: 300px" [options]="options" [(ngModel)]="model"></aui-monaco-editor>
      `,
      props: {
        options: {
          language: 'yaml',
          folding: true,
          minimap: { enabled: true },
        },
        model,
      },
    };
  })
  .add('code editor', () => {
    const model = exampleCode;
    const options = object('options', {
      language: 'yaml',
      folding: true,
      minimap: { enabled: false },
      readOnly: false,
    });
    return {
      moduleMetadata: {
        imports: [
          CodeEditorModule.forRoot({
            baseUrl: '',
            defaultOptions: {},
          }),
        ],
      },
      template: `
        <h1>原始代码</h1>
        <textarea cols="80" rows="10" [(ngModel)]="model">{{ exampleCode }}</textarea>
        <h1>编辑器</h1>
        <aui-code-editor [(ngModel)]="model" [originalValue]="originalValue" [options]="options"></aui-code-editor>
      `,
      props: {
        options,
        originalValue: text('originalValue', exampleCode),
        model,
      },
    };
  })
  .add('colorize code', () => {
    return {
      moduleMetadata: {
        imports: [
          CodeEditorModule.forRoot({
            baseUrl: '',
            defaultOptions: {},
          }),
        ],
      },
      template: `<pre style="font-family: Consolas, 'Courier New', monospace;"><code auiCodeColorize="yaml">{{ code }}</code></pre>`,
      props: {
        code: exampleCode,
      },
    };
  });
