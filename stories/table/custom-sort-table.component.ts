import { Component } from '@angular/core';
import { Sort } from '../../src/sort/public-api';

interface Element {
  id: number;
  name: string;
  displayName: string;
  value: number;
}

const DATA_SOURCE: Element[] = [
  { id: 1, name: 'element1', displayName: 'Element One', value: 5 },
  { id: 3, name: 'element3', displayName: 'Element Three', value: 2 },
  { id: 4, name: 'element4', displayName: 'Element Four', value: 9 },
  { id: 5, name: 'element5', displayName: 'Element Five', value: 3 },
  { id: 6, name: 'element6', displayName: 'Element Six', value: 4 },
  { id: 2, name: 'element2', displayName: 'Element Two', value: 8 },
];

@Component({
  selector: 'custom-sort-table',
  styleUrls: ['custom-sort-table.component.scss'],
  template: `
    <h1>With Sort</h1>
    <aui-table
      auiSort
      [dataSource]="dataSource"
      (sortChange)="sortData($event)"
    >
      <ng-container auiTableColumnDef="id">
        <aui-table-header-cell
          *auiTableHeaderCellDef
          aui-sort-header
        >
          No.
        </aui-table-header-cell>
        <aui-table-cell *auiTableCellDef="let item">{{ item.id }}</aui-table-cell>
      </ng-container>
      <ng-container auiTableColumnDef="name">
        <aui-table-header-cell
          *auiTableHeaderCellDef
          aui-sort-header
        >
          Name
        </aui-table-header-cell>
        <aui-table-cell *auiTableCellDef="let item">
          {{ item.name }}
          ({{ item.displayName }})
        </aui-table-cell>
      </ng-container>
      <ng-container auiTableColumnDef="value">
        <aui-table-header-cell
          *auiTableHeaderCellDef
          aui-sort-header
          start="desc"
        >
          Value
        </aui-table-header-cell>
        <aui-table-cell *auiTableCellDef="let item">
          {{ item.value }}
        </aui-table-cell>
      </ng-container>
      <aui-table-header-row *auiTableHeaderRowDef="['id', 'name', 'value']"></aui-table-header-row>
      <aui-table-row *auiTableRowDef="let row; columns: ['id', 'name', 'value'];"></aui-table-row>
    </aui-table>
  `,
})
export class CustomSortTableComponent {
  dataSource = DATA_SOURCE.slice();

  sortData(sort: Sort) {
    this.dataSource = DATA_SOURCE.slice().sort((a, b) => {
      return a[sort.active] === b[sort.active]
        ? 0
        : a[sort.active] > b[sort.active]
          ? sort.direction === 'asc'
            ? 1
            : -1
          : sort.direction === 'asc'
            ? -1
            : 1;
    });
  }
}
