import { array, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { SortModule, TableModule } from '../../src/public-api';
import { CustomSortTableComponent } from './custom-sort-table.component';

interface Element {
  id: number;
  name: string;
  displayName: string;
  value: number;
}

const DATA_SOURCE: Element[] = [
  { id: 1, name: 'element1', displayName: 'Element One', value: 5 },
  { id: 2, name: 'element1', displayName: 'Element Two', value: 8 },
  { id: 3, name: 'element1', displayName: 'Element Three', value: 2 },
  { id: 4, name: 'element1', displayName: 'Element Four', value: 9 },
  { id: 5, name: 'element1', displayName: 'Element Five', value: 3 },
  { id: 6, name: 'element1', displayName: 'Element Six', value: 4 },
];

storiesOf('Table', module)
  .addDecorator(withKnobs)
  .add(
    'default',
    (): any => {
      const dataSource = array('dataSource', DATA_SOURCE);
      return {
        moduleMetadata: {
          imports: [TableModule],
        },
        template: `
      <h1>Table</h1>
      <aui-table
        [dataSource]="dataSource"
      >
        <ng-container auiTableColumnDef="id">
          <aui-table-header-cell *auiTableHeaderCellDef>No.</aui-table-header-cell>
          <aui-table-cell *auiTableCellDef="let item">{{ item.id }}</aui-table-cell>
        </ng-container>
        <ng-container auiTableColumnDef="name">
          <aui-table-header-cell *auiTableHeaderCellDef>Name</aui-table-header-cell>
          <aui-table-cell *auiTableCellDef="let item">
            {{ item.name }}
            ({{ item.displayName }})
          </aui-table-cell>
        </ng-container>
        <ng-container auiTableColumnDef="value">
          <aui-table-header-cell *auiTableHeaderCellDef>Value</aui-table-header-cell>
          <aui-table-cell *auiTableCellDef="let item">
            {{ item.value }}
          </aui-table-cell>
        </ng-container>
        <aui-table-header-row *auiTableHeaderRowDef="['id', 'name', 'value']"></aui-table-header-row>
        <aui-table-row *auiTableRowDef="let row; columns: ['id', 'name', 'value'];"></aui-table-row>
      </aui-table>
    `,
        props: {
          dataSource,
        },
      };
    },
  )
  .add(
    'with sort',
    (): any => {
      return {
        moduleMetadata: {
          imports: [TableModule, SortModule],
        },
        component: CustomSortTableComponent,
      };
    },
  );
