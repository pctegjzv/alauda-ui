import { boolean, withKnobs } from '@storybook/addon-knobs/angular';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { LayoutModule, NavModule } from '../../src/public-api';
import { DemoNavComponent } from '../nav/demo-nav.component';
import { DemoLayoutComponent } from './demo-layout.component';

storiesOf('Layout', module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [LayoutModule, NavModule],
      declarations: [DemoNavComponent],
    }),
  )
  .add('default', () => {
    const showDrawer = boolean('showDrawer', true);
    return {
      component: DemoLayoutComponent,
      props: {
        showDrawer,
      },
    };
  });
