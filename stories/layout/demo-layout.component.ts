import { Component, Input, OnInit } from '@angular/core';

@Component({
  template: `
<aui-layout [showDrawer]="showDrawer">
  <div class="logo-container" *auiNavDrawerHeader>
    <img src="http://www.alauda.cn/Public/Home/images/logo.png">
  </div>
  <demo-nav-component *auiNavDrawerContent></demo-nav-component>

  <div *auiToolbarContent>
    Some toolbar content
  </div>
  <div *auiPageHeader>
    Page Header
  </div>
  <div *auiPageContent>
    Page Content
  </div>
</aui-layout>
  `,
  styles: [
    `
      :host {
        width: calc(100vw - 50px);
        height: calc(100vh - 50px);
        display: block;
      }
    `,
    `
      .logo-container {
        display: flex;
        width: 100%;
        height: 100%;
        align-items: center;
        margin-left: 20px;
      }
    `,
  ],
})
export class DemoLayoutComponent implements OnInit {
  @Input() showDrawer: boolean;

  constructor() {}

  ngOnInit() {}
}
