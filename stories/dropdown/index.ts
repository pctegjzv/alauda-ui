import { select, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import {
  ButtonModule,
  ComponentSize,
  DropdownModule,
  IconModule,
} from '../../src/public-api';

storiesOf('Dropdown', module)
  .addDecorator(withKnobs)
  .add('default', () => {
    const sizeOptions = {
      [ComponentSize.Large]: ComponentSize.Large,
      [ComponentSize.Medium]: ComponentSize.Medium,
      [ComponentSize.Small]: ComponentSize.Small,
      [ComponentSize.Mini]: ComponentSize.Mini,
    };
    const size = select('size', sizeOptions, ComponentSize.Medium);

    return {
      moduleMetadata: {
        imports: [DropdownModule, ButtonModule, IconModule],
      },
      template: `
        <div style="padding: 180px;height: 400px;">
          <aui-dropdown-button [size]="size" type="primary">
            dropdown button
            <aui-menu [size]="size">
              <aui-menu-group>
                <span auiMenuGroupTitle>default</span>
                <aui-menu-item>default</aui-menu-item>
              </aui-menu-group>
              <aui-menu-group>
                <aui-menu-item type="success">success</aui-menu-item>
                <aui-menu-item type="warning">warning</aui-menu-item>
                <aui-menu-item type="danger">danger</aui-menu-item>
                <aui-menu-item type="danger" [disabled]="true">disabled</aui-menu-item>
              </aui-menu-group>
              <aui-menu-group>
                <span auiMenuGroupTitle>submenu</span>
                <aui-submenu [size]="size">
                  submenu
                  <aui-menu-item>创建</aui-menu-item>
                  <aui-menu-item>更新</aui-menu-item>
                  <aui-menu-item>删除</aui-menu-item>
                </aui-submenu>
              </aui-menu-group>
            </aui-menu>
          </aui-dropdown-button>
          <button 
            aui-button="primary"
            [plain]="true"
            [size]="size"            
            auiDropdownActive="aui-trigger--actived"
            [auiDropdown]="menu"
            [auiDropdownContext]="{action: '更新'}">
            trigger
            <aui-icon icon="angle_down" margin="left"></aui-icon>
          </button>
          <button 
            aui-button="primary"
            [plain]="true"
            [size]="size"
            [auiDropdown]="menu"
            [auiDropdownContext]="{action: '创建'}">
            trigger
            <aui-icon icon="angle_down" margin="left"></aui-icon>
          </button>
          <aui-menu #menu [size]="size">
            <ng-template auiMenuContent let-action="action">
              <aui-menu-item>{{ action }}</aui-menu-item>
            </ng-template>
          </aui-menu>
        </div>`,
      props: {
        size,
      },
    };
  });
