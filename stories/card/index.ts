import { storiesOf } from '@storybook/angular';
import { CardModule } from '../../src/public-api';

storiesOf('Card', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [CardModule],
    },
    template: `
    <aui-card>
      <div auiCardHeader>header</div>
      <h2>content</h2>
    </aui-card>
    <aui-card>
      <div auiCardFooter>footer</div>    
      <div auiCardHeader>header</div>
      <aui-section>section 1</aui-section>
      <aui-section>section 2</aui-section>
    </aui-card>`,
  };
});
