import { storiesOf } from '@storybook/angular';
import { BreadcrumbModule } from '../../src/public-api';

storiesOf('Breadcrumb', module).add('default', () => {
  return {
    moduleMetadata: {
      imports: [BreadcrumbModule],
    },
    template: `
      <aui-breadcrumb>
        <aui-breadcrumb-item><a href="/">storybook</a></aui-breadcrumb-item>
        <aui-breadcrumb-item><a href="./">breadcrumb</a></aui-breadcrumb-item>
        <aui-breadcrumb-item>default</aui-breadcrumb-item>
      </aui-breadcrumb>
      <aui-breadcrumb separator="|">
        <aui-breadcrumb-item><a href="/">storybook</a></aui-breadcrumb-item>
        <aui-breadcrumb-item><a href="./">breadcrumb</a></aui-breadcrumb-item>
        <aui-breadcrumb-item>default</aui-breadcrumb-item>
      </aui-breadcrumb>
      <aui-breadcrumb separatorIcon="angle_right">
        <aui-breadcrumb-item><a href="/">storybook</a></aui-breadcrumb-item>
        <aui-breadcrumb-item><a href="./">breadcrumb</a></aui-breadcrumb-item>
        <aui-breadcrumb-item>default</aui-breadcrumb-item>
      </aui-breadcrumb>`,
  };
});
