import { Component } from '@angular/core';
import { RadioSize } from '../../src/radio/radio.types';

@Component({
  templateUrl: './demo-radio.component.html',
  styles: [],
})
export class DemoRadioComponent {
  food = '9';
  size = RadioSize;

  onValueChange = (value: any) => {
    // 这里可以加各种判断，来确定要不要改变这个值
    if (value === '7') {
      this.food = value;
    }
  };
}
