import { storiesOf } from '@storybook/angular';
import { RadioModule } from '../../src/public-api';
import { DemoRadioComponent } from './demo-radio.component';

storiesOf('Radio', module).add('default', () => ({
  component: DemoRadioComponent,
  moduleMetadata: {
    imports: [RadioModule],
    declarations: [DemoRadioComponent],
  },
}));
