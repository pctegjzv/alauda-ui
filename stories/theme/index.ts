import { storiesOf } from '@storybook/angular';
import {
  PaletteCardDirective,
  PaletteComponent,
} from './palette/palette.component';

storiesOf('Theme', module).add('Platte', () => ({
  component: PaletteComponent,
  moduleMetadata: {
    declarations: [PaletteCardDirective],
  },
}));
