import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { CheckboxModule } from '../../src/public-api';
import { DemoCheckboxComponent } from './demo-checkbox.component';

storiesOf('Checkbox', module)
  .addDecorator(withKnobs)
  .add('default', () => ({
    component: DemoCheckboxComponent,
    props: {
      action: action(this.checkedMap),
    },
    moduleMetadata: {
      imports: [CheckboxModule],
      declarations: [DemoCheckboxComponent],
    },
  }))
  .add('checkbox group', () => {
    const value = ['app', 'other'];
    return {
      moduleMetadata: {
        imports: [CheckboxModule],
      },
      template: `
        <aui-checkbox-group [(ngModel)]="value" required minlength="2" maxlength="3">
          <aui-checkbox label="app">app</aui-checkbox>
          <aui-checkbox label="sitmap">sitmpap</aui-checkbox>
          <aui-checkbox label="server">server</aui-checkbox>
          <aui-checkbox label="other">other</aui-checkbox>
        </aui-checkbox-group>
        {{ value | json }}`,
      props: {
        value,
      },
    };
  });
