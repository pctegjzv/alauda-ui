import { Component } from '@angular/core';

@Component({
  templateUrl: './demo-checkbox.component.html',
})
export class DemoCheckboxComponent {
  action: (action: any) => void;
  checkedMap = { a: true, b: false };

  modelChange($event: any) {
    this.action($event);
  }
}
