import { action } from '@storybook/addon-actions';
import {
  boolean,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { ButtonModule, TooltipModule } from '../../src/public-api';

storiesOf('Tooltip', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const content = text('content', 'contentText');

    const arr = new Array(1).fill(null);

    const typeOptions = {
      default: 'default',
      primary: 'primary',
      success: 'success',
      warning: 'warning',
      error: 'error',
      info: 'info',
    };
    const type = select('type', typeOptions, 'default');

    const triggerOptions = {
      hover: 'hover',
      click: 'click',
      focus: 'focus',
    };
    const trigger = select('trigger', triggerOptions, 'hover');

    const positionOptions = {
      top: 'top',
      'top start': 'top start',
      'top end': 'top end',
      bottom: 'bottom',
      'bottom start': 'bottom start',
      'bottom end': 'bottom end',
      start: 'start',
      'start top': 'start top',
      'start bottom': 'start bottom',
      end: 'end',
      'end top': 'end top',
      'end bottom': 'end bottom',
    };
    const position = select('position', positionOptions, 'top');

    const disabled = boolean('disabled', false);

    const onShow = action('show');
    const onHide = action('hide');

    return {
      moduleMetadata: {
        imports: [TooltipModule, ButtonModule],
      },
      template: `
      <div style="padding: 100px;">
        <button
          *ngFor="let i of arr"
          aui-button
          [auiTooltip]="content"
          [auiTooltipType]="type"
          [auiTooltipPosition]="position"
          [auiTooltipTrigger]="trigger"
          [auiTooltipDisabled]="disabled"
          (auiTooltipShow)="onShow()"
          (auiTooltipHide)="onHide()"
          auiTooltipActive="tooltip-actived">
          text
        </button>
        <button
          aui-button
          [auiTooltip]="template"
          [auiTooltipType]="type"
          [auiTooltipPosition]="position"
          [auiTooltipTrigger]="trigger"
          [auiTooltipDisabled]="disabled"
          (auiTooltipShow)="onShow()"
          (auiTooltipHide)="onHide()">
          template
        </button>
        <button
          aui-button 
          [auiTooltipCopy]="content"
          [auiTooltipPosition]="'bottom'">
          copy
        </button>
      </div>
      <ng-template #template>
        <button aui-button>{{ content }}</button>
      </ng-template>`,
      props: {
        content,
        type,
        trigger,
        position,
        disabled,
        onHide,
        onShow,
        arr,
      },
    };
  });
