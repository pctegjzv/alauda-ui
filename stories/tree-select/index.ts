import { FormsModule } from '@angular/forms';
import { boolean, text, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { TreeSelectModule } from '../../src/public-api';
import { TreeSelectDemoComponent } from './tree-select-demo.component';

storiesOf('TreeSelect', module)
  .addDecorator(withKnobs)
  .add('tree select', () => {
    const placeholder = text('placeholder', 'placeholder');

    const disabled = boolean('disabled', false);
    const loading = boolean('loading', false);
    const filterable = boolean('filterable', true);
    const clearable = boolean('clearable', true);
    return {
      moduleMetadata: {
        imports: [FormsModule, TreeSelectModule],
      },
      component: TreeSelectDemoComponent,
      props: {
        disabled,
        loading,
        placeholder,
        clearable,
        filterable,
      },
    };
  });
