import { Component, Input, OnInit } from '@angular/core';
import { TreeNode } from '../../src/public-api';

@Component({
  templateUrl: './tree-select-demo.component.html',
})
export class TreeSelectDemoComponent implements OnInit {
  @Input() placeholder: string;
  @Input() filterable: boolean;
  @Input() disabled: boolean;
  @Input() loading: boolean;
  @Input() clearable: boolean;

  treeNodes: TreeNode[];

  value: string;

  ngOnInit() {
    this.treeNodes = [
      {
        label: 'a',
        value: 'a',
        expanded: false,
        children: [
          {
            label: 'a-1',
            value: 'a-1',
          },
          {
            label: 'a-2',
            value: 'a-2',
            children: [
              {
                label: 'a-2-1',
                value: 'a-2-1',
              },
            ],
          },
          { label: 'a-3', value: 'a-3', disabled: true },
          {
            label: 'a-4',
            value: 'a-4',
            disabled: true,
            children: [
              {
                label: 'a-4-1',
                value: 'a-4-1',
              },
            ],
          },
        ],
      },
      {
        label: 'b',
        value: 'b',
        expanded: true,
        children: [
          {
            label: 'b-1',
            value: 'b-1',
          },
          {
            label: 'b-2',
            value: 'b-2',
          },
          {
            label: 'b-3',
            value: 'b-3',
          },
          {
            label: 'b-4',
            value: 'b-4',
          },
        ],
      },
    ];

    this.value = 'b-1';
  }
}
