import { Component } from '@angular/core';
import { ToastService } from '../../src/public-api';

@Component({
  templateUrl: './demo-toast.component.html',
})
export class DemoToastComponent {
  constructor(private toast: ToastService) {}

  alert() {
    this.toast.alertError({ content: 'test' });
  }
}
