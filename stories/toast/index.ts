import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/angular';
import { ToastModule } from '../../src/public-api';
import { DemoToastComponent } from './demo-toast.component';

storiesOf('Toast', module).add('Default', () => {
  return {
    moduleMetadata: {
      imports: [ToastModule, BrowserAnimationsModule],
      declarations: [DemoToastComponent],
    },
    component: DemoToastComponent,
    props: {
      action: action('after closed result: '),
    },
  };
});
