import { Component } from '@angular/core';

@Component({
  templateUrl: './autocomplete-demo.component.html',
})
export class AutocompleteDemoComponent {
  model = 'ab';
}
