import { FormsModule } from '@angular/forms';
import { storiesOf } from '@storybook/angular';
import { AutocompleteModule, InputModule } from '../../src/public-api';
import { AutocompleteDemoComponent } from './autocomplete-demo.component';

storiesOf('Autocomplete', module).add('autocomplete', () => {
  return {
    moduleMetadata: {
      imports: [FormsModule, AutocompleteModule, InputModule],
      declarations: [AutocompleteDemoComponent],
    },
    component: AutocompleteDemoComponent,
  };
});
