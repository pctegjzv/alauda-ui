import { Component } from '@angular/core';
import { action } from '@storybook/addon-actions';

@Component({
  templateUrl: './demo-switch.component.html',
  styles: [],
})
export class DemoSwitchComponent {
  switch1 = true;
  switch2 = true;

  loading = false;

  beforeSwitch = (reslove: any, reject: any) => {
    action('beforeSwitch')();
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      reslove();
    }, 2000);
  };
}
