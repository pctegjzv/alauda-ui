import { storiesOf } from '@storybook/angular';
import { SwitchModule } from '../../src/public-api';
import { DemoSwitchComponent } from './demo-switch.component';

storiesOf('Switch', module).add('default', () => ({
  component: DemoSwitchComponent,
  moduleMetadata: {
    imports: [SwitchModule],
    declarations: [DemoSwitchComponent],
  },
}));
