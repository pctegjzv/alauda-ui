import { FormsModule } from '@angular/forms';
import { action } from '@storybook/addon-actions';
import {
  boolean,
  number,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { ComponentSize, IconModule, InputModule } from '../../src/public-api';

storiesOf('Input', module)
  .addDecorator(withKnobs)
  .add('input', () => {
    return {
      moduleMetadata: { imports: [InputModule, FormsModule] },
      template: `
        <div>
          <input aui-input [(ngModel)]="value" placeholder="placeholder">
          <textarea style="margin-top: 16px;" aui-input placeholder="placeholder"></textarea>
        </div>`,
    };
  })
  .add('input group', () => {
    const disabled = boolean('disabled', false);
    return {
      moduleMetadata: {
        imports: [InputModule, IconModule],
      },
      template: `
        <aui-input-group>
          <span auiInputAddonBefore>HTTPS</span>
          <span auiInputAddonAfter>GB</span>
          <aui-icon auiInputPrefix icon="search_s"></aui-icon>
          <aui-icon auiInputSuffix icon="spinner"></aui-icon>
          <input
            aui-input
            [disabled]="disabled"
            placeholder="placeholder">
        </aui-input-group>`,
      props: {
        disabled,
      },
    };
  })
  .add('tags input', () => {
    const value = ['app', 'service'];
    return {
      moduleMetadata: { imports: [InputModule] },
      template: `
        <div>
          <aui-tags-input [(ngModel)]="value" placeholder="placeholder"></aui-tags-input>
          {{ value | json }}
        </div>`,
      props: {
        value,
      },
    };
  })
  .add('search input', () => {
    const sizeOptions = {
      [ComponentSize.Large]: ComponentSize.Large,
      [ComponentSize.Medium]: ComponentSize.Medium,
      [ComponentSize.Small]: ComponentSize.Small,
      [ComponentSize.Mini]: ComponentSize.Mini,
    };
    const size = select('size', sizeOptions, ComponentSize.Medium);
    const searchButton = boolean('searchButton', false);
    const searching = boolean('searching', false);
    const clearable = boolean('clearable', false);
    const disabled = boolean('disabled', false);
    const keyword = text('keyword', 'keyword');
    const debounce = number('debounce', 0);

    const onChangeHandler = action('change');
    const onSearchHandler = action('search');
    const onClearHandler = action('clear');

    return {
      moduleMetadata: {
        imports: [InputModule],
      },
      template: `
        <aui-search
          [size]="size"
          [searchButton]="searchButton"
          [searching]="searching"
          [clearable]="clearable"
          [disabled]="disabled"
          [debounce]="debounce"
          [(keyword)]="keyword"
          placeholder="placeholder"
          (search)="onSearchHandler($event)"
          (keywordChange)="onChangeHandler($event)"
          (clear)="onClearHandler($event)">
        </aui-search>
        {{ keyword }}`,
      props: {
        size,
        searchButton,
        searching,
        clearable,
        disabled,
        debounce,
        keyword,
        onChangeHandler,
        onSearchHandler,
        onClearHandler,
      },
    };
  });
