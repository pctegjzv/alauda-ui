import { boolean, select, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';

import { DemoTabsComponent } from './demo-tabs.component';

import {
  ButtonModule,
  FormModule,
  IconModule,
  InputModule,
  SwitchModule,
  TabSize,
  TabsModule,
  TabType,
} from '../../src/public-api';

storiesOf('Tabs', module)
  .addDecorator(withKnobs)
  .add('default', () => {
    const sizeOptions = {
      [TabSize.Large]: TabSize.Large,
      [TabSize.Medium]: TabSize.Medium,
      [TabSize.Small]: TabSize.Small,
    };
    const size = select('size', sizeOptions, TabSize.Medium);

    const typeOptions = {
      [TabType.Line]: TabType.Line,
      [TabType.Card]: TabType.Card,
    };
    const type = select('type', typeOptions, TabType.Line);

    const hasAddon = boolean('hasAddon', true);

    return {
      component: DemoTabsComponent,
      moduleMetadata: {
        imports: [
          TabsModule,
          ButtonModule,
          IconModule,
          InputModule,
          SwitchModule,
          FormModule,
        ],
        declarations: [DemoTabsComponent],
      },
      props: {
        size,
        type,
        hasAddon,
      },
    };
  });
