import { Component, Input, OnInit } from '@angular/core';
import { TabSize, TabType } from '../../src/public-api';

interface DemoTab {
  label: string;
  disabled?: boolean;
  closeable?: boolean;
}

@Component({
  templateUrl: './demo-tabs.component.html',
  styles: [
    `
      aui-tab-group {
        display: block;
        width: 50%;
      }

      .tab-content-wrapper {
        display: flex;
        flex-flow: column;
        align-items: center;
        justify-content: center;
        height: 200px;
        background: #eee;
      }
    `,
  ],
})
export class DemoTabsComponent implements OnInit {
  @Input() hasAddon: boolean;
  @Input() size: TabSize;
  @Input() type: TabType;

  tabs: DemoTab[] = [];

  add() {
    const newIndex = this.tabs.length;
    this.tabs = [
      ...this.tabs,
      {
        label: `Tab Label ${newIndex}`,
      },
    ];
  }

  close(index: number) {
    this.tabs = this.tabs.filter((_, _index) => index !== _index);
  }

  ngOnInit() {
    this.add();
    this.add();
    this.add();
  }
}
