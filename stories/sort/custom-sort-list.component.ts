import { Component } from '@angular/core';
import { Sort } from '../../src/public-api';

export interface Element {
  id: number;
  name: string;
  displayName: string;
  value: number;
}

const DATA_SOURCE: Element[] = [
  { id: 1, name: 'element1', displayName: 'Element One', value: 5 },
  { id: 3, name: 'element3', displayName: 'Element Three', value: 2 },
  { id: 4, name: 'element4', displayName: 'Element Four', value: 9 },
  { id: 5, name: 'element5', displayName: 'Element Five', value: 3 },
  { id: 6, name: 'element6', displayName: 'Element Six', value: 4 },
  { id: 2, name: 'element2', displayName: 'Element Two', value: 8 },
];

@Component({
  selector: 'custom-sort-list',
  template: `
    <h1>Sort</h1>
    <table auiSort (sortChange)="sortData($event)">
      <tr>
        <th aui-sort-header="id">No.</th>
        <th aui-sort-header="name">Name</th>
        <th aui-sort-header="value" start="desc">Value</th>
      </tr>
      <tr *ngFor="let item of dataSource">
        <td>{{ item.id }}</td>
        <td>{{ item.name }}</td>
        <td>{{ item.value }}</td>
      </tr>
    </table>
  `,
})
export class CustomSortListComponent {
  dataSource = DATA_SOURCE.slice();

  sortData(sort: Sort) {
    // tslint:disable-next-line
    this.dataSource = this.dataSource.sort((a, b) => {
      return a[sort.active] === b[sort.active]
        ? 0
        : a[sort.active] > b[sort.active]
          ? sort.direction === 'asc'
            ? 1
            : -1
          : sort.direction === 'asc'
            ? -1
            : 1;
    });
  }
}
