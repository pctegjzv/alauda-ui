import { storiesOf } from '@storybook/angular';
import { SortModule } from '../../src/public-api';
import { CustomSortListComponent } from './custom-sort-list.component';

storiesOf('Sort', module).add(
  'default',
  (): any => {
    return {
      moduleMetadata: {
        imports: [SortModule],
      },
      component: CustomSortListComponent,
    };
  },
);
