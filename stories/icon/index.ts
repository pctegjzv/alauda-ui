import { HttpClientModule } from '@angular/common/http';
import { text, withKnobs } from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { IconModule } from '../../src/public-api';
import { DemoIconComponent } from './demo-icon.component';

storiesOf('Icon', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const icon = text('icon', 'spinner');
    const size = text('size', '24px,24px');
    const color = text('color', 'black');
    return {
      moduleMetadata: {
        imports: [IconModule, HttpClientModule],
        declarations: [DemoIconComponent],
      },
      component: DemoIconComponent,
      props: {
        icon,
        size,
        color,
      },
    };
  });
