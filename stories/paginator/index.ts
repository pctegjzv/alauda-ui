import {
  array,
  boolean,
  number,
  withKnobs,
} from '@storybook/addon-knobs/angular';
import { storiesOf } from '@storybook/angular';
import { PaginatorIntl, PaginatorModule } from '../../src/public-api';
import { PaginatorZh } from './paginator-zh';

storiesOf('Paginator', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const pageIndex = number('pageIndex', 0);
    const pageSize = number('pageSize', 20);
    const length = number('length', 150);
    const pageSizeOptions = array('pageSizeOptions', [10, 20, 50] as number[]);
    const hidePageSize = boolean('hidePageSize', false);
    const showFirstLastButtons = boolean('showFirstLastButtons', false);

    return {
      moduleMetadata: {
        imports: [PaginatorModule],
      },
      template: `
        <div>
          <aui-paginator
            [pageIndex]="pageIndex"
            [pageSize]="pageSize"
            [length]="length"
            [pageSizeOptions]="pageSizeOptions"
            [hidePageSize]="hidePageSize"
            [showFirstLastButtons]="showFirstLastButtons"
          ></aui-paginator>
        </div>
      `,
      props: {
        pageIndex,
        pageSize,
        length,
        pageSizeOptions,
        hidePageSize,
        showFirstLastButtons,
      },
    };
  })
  .add('With Intl', () => {
    const pageIndex = number('pageIndex', 0);
    const pageSize = number('pageSize', 20);
    const length = number('length', 150);
    const pageSizeOptions = array('pageSizeOptions', [10, 20, 50] as number[]);
    const hidePageSize = boolean('hidePageSize', false);
    const showFirstLastButtons = boolean('showFirstLastButtons', false);

    return {
      moduleMetadata: {
        imports: [PaginatorModule],
        providers: [
          {
            provide: PaginatorIntl,
            useClass: PaginatorZh,
          },
        ],
      },
      template: `
        <div>
          <aui-paginator
            [pageIndex]="pageIndex"
            [pageSize]="pageSize"
            [length]="length"
            [pageSizeOptions]="pageSizeOptions"
            [hidePageSize]="hidePageSize"
            [showFirstLastButtons]="showFirstLastButtons"
          ></aui-paginator>
        </div>
      `,
      props: {
        pageIndex,
        pageSize,
        length,
        pageSizeOptions,
        hidePageSize,
        showFirstLastButtons,
      },
    };
  });
