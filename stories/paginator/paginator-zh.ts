import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PaginatorIntl } from '../../src/paginator/public-api';

@Injectable()
export class PaginatorZh extends PaginatorIntl {
  /**
   * Stream that emits whenever the labels here are changed. Use this to notify
   * components if the labels have changed after initialization.
   */
  readonly changes: Subject<void> = new Subject<void>();

  /** A label for the page size selector. */
  itemsPerPageLabel = '条 / 每页';

  /** A label for the button that increments the current page. */
  nextPageLabel = '上一页';

  /** A label for the button that decrements the current page. */
  previousPageLabel = '下一页';

  /** A label for the button that moves to the first page. */
  firstPageLabel = '第一页';

  /** A label for the button that moves to the last page. */
  lastPageLabel = '尾页';

  getTotalLabel = (length: number) => {
    return `共${length}条`;
  };
}
