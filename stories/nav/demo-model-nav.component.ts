import { Component } from '@angular/core';

import { DemoNavComponent } from './demo-nav.component';

export interface DemoNavItem {
  label: string;
  path: string;
  items?: DemoNavItem[];
}

@Component({
  templateUrl: './demo-model-nav.component.html',
  styleUrls: ['./demo-nav.component.scss'],
})
export class DemoModelNavComponent extends DemoNavComponent {
  items: DemoNavItem[] = [
    {
      label: '山东',
      path: 'sd',
      items: [
        {
          label: '德州',
          path: 'dz',
        },
        {
          label: '济南',
          path: 'jn',
        },
      ],
    },
    {
      label: '北京',
      path: 'bj',
    },
    {
      label: '上海',
      path: 'sh',
      items: [
        {
          path: 'ljz',
          label: '陆家嘴',
        },
      ],
    },
  ];
}
