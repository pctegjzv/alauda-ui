import { Component } from '@angular/core';

@Component({
  selector: 'demo-nav-component',
  templateUrl: './demo-nav.component.html',
  styleUrls: ['./demo-nav.component.scss'],
  providers: [DemoNavComponent],
})
export class DemoNavComponent {
  expandedMap = {};

  activePath = '';

  handleClick(path: string) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }

  isPathExpanded(path: string) {
    return this.expandedMap[path];
  }
}
