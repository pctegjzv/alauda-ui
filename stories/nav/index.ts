import { CommonModule } from '@angular/common';
import { storiesOf } from '@storybook/angular';
import { NavModule } from '../../src/public-api';
import { DemoModelNavComponent } from './demo-model-nav.component';
import { DemoNavComponent } from './demo-nav.component';

storiesOf('Navigation', module)
  .add('Plain template', () => ({
    component: DemoNavComponent,
    moduleMetadata: {
      imports: [NavModule],
    },
  }))
  .add('With model', () => ({
    component: DemoModelNavComponent,
    moduleMetadata: {
      imports: [CommonModule, NavModule],
    },
  }));
