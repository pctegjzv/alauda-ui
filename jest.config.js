module.exports = {
  preset: 'jest-preset-angular',
  testMatch: ['<rootDir>/src/**/+(*.)+(spec|test).+(ts|js)?(x)'],
  coverageReporters: ['text', 'html'],
  coverageDirectory: '<rootDir>/coverage',
  modulePathIgnorePatterns: ['<rootDir>/release', '<rootDir>/dist'],
  coveragePathIgnorePatterns: [
    '<rootDir>/config',
    '<rootDir>/release',
    '<rootDir>/dist',
    '(\\w*/)*(\\w|\\.)+\\.html',
    '(\\w*/)*public-api\\.ts',
    '/node_modules/',
  ],
  setupTestFrameworkScriptFile: './jest.setup.js',
  globals: {
    'ts-jest': {
      tsConfigFile: 'tsconfig.spec.json',
    },
    __TRANSFORM_HTML__: true,
  },
};
