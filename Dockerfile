FROM index.alauda.cn/alaudaorg/rubick-base:latest

WORKDIR /alauda-ui/
COPY . /alauda-ui/

RUN rm -rf node_modules && \
    npm cache clear --force && \
    npm i -g http-server && \
    npm i && \
    npm run storybook:build && \
    rm -rf node_modules

EXPOSE 9002
CMD ["http-server","./dist","-p","9002"]
