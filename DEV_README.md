# Alauda UI

 仍处于开发阶段的 Alauda 团队内部使用的 UI 框架。

[Demo](http://k8s-cn-alauda-ui.nolimited.haproxy-internet-alaudacn.myalauda.cn/)

## Catalog

- [Getting Started](#getting-started)
- [Development](#development)
- [Test](#test)
- [Build Storybook](#build-storybook)
- [Build Library](#build-library)
- [Read More](#read-more)

## Getting Started

### Install

```bash
npm i alauda-ui

// or

npm i -S alauda-ui
```

### Usage

```typescript
import { AuiModule } from 'alauda-ui';

@NgModule({
  imports: [AuiModule],
})
export class AppModule {}
```

## Development

```bash
git clone git@bitbucket.org:mathildetech/alauda-ui.git
cd alauda-ui
npm install
npm start
```

开发环境基于 [Storybook](https://storybook.js.org/) 运行， 查看 [文档](https://storybook.js.org/basics/guide-angular/)。

## Test

```bash
npm run test
```

or

```bash
npm run test:watch
```

## Build Storybook

```bash
npm run storybook:build
```

## Build Library

```bash
npm run build
```

## Update Icon Files

### 更新内置图标

将图标文件夹拷贝至 `/assets/icons` 目录，删除旧版文件夹， 执行 `npm run update-aui-icons`，修改版本号发布新版。

### 更新其他图标集合

[下载](http://confluence.alaudatech.com/pages/viewpage.action?pageId=24221238)图标文件并拷贝至 `/assets/icons` 目录，删除旧版文件夹，修改版本号并发版，构建时 icon 将自动打包。

文件夹命名规则: `<namespace>-icon_<version>`，新增图标集合需要修改[脚本](./scripts/build-icons.js)中 `iconFloderNames`。

## Read More

- [coding standards](./docs/CODING_STANDARDS.md)
- [contributing](./docs/CONTRIBUTING.md)
- [AOT notes](./docs/AOT_NOTES.md)
