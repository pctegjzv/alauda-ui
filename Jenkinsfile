@Library(['alauda-cicd', 'alaudaee-pipeline']) _

def GIT_BRANCH
def GIT_COMMIT
def IMAGE
def DEBUG = false

pipeline {
  agent {
    label 'all'
  }

  options {
    // 保留多少流水线记录
    buildDiscarder(logRotator(numToKeepStr: '10'))

    // 不允许并行执行
    disableConcurrentBuilds()
  }

  environment {
    // for building an scanning
    REPOSITORY = "alauda-ui"
    OWNER = "mathildetech"

    // needs to change together with the credentialsID
    DEPLOYMENT = "alauda-ui"

    // needs to update aui service on tech
    ENDPOINT = "https://api.alaudatech.cn"
    NAMESPACE = "alaudacn"
    UPDATE_SERVICE_CREDENTIALS = "alaudatechtoken-lq"
    APP_NAME = "k8s-cn-alauda-ui"
    SPACE_NAME = "nolimited"
    REGION = "k8s_cn"

    // feedback bot
    DINGDING_BOT = "frontend-bot"

    // build context
    REPO_ADDRESS = "index.alauda.cn/alaudaorg/alauda-ui"
    ORG_CREDENTIALS = "alaudaorgpush-lq"

    // publish token
    NPM_ORG_CREDENTIALS = "auinpmorg"

    // bitbucket access
    TAG_CREDENTIALS = "aui-bitbucket-owner"
  }

  stages {
    stage("Checkout") {
      steps {
        script {
          def scmVars = checkout scm
          env.GIT_COMMIT = scmVars.GIT_COMMIT
          env.GIT_BRANCH = scmVars.GIT_BRANCH
          GIT_COMMIT = "${scmVars.GIT_COMMIT}"
          GIT_BRANCH = "${scmVars.GIT_BRANCH}"
          def version = readJSON text: sh(returnStdout: true, script: 'npm version').trim()
          RELEASE_VERSION = version['alauda-ui']
          RELEASE_BUILD = "${RELEASE_VERSION}-${env.BUILD_NUMBER}"
          if (GIT_BRANCH != "master") {
            def branch = GIT_BRANCH.replace("/","-").replace("_","-")
            RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}".toLowerCase()
          }
          REMOTE_VERSION = sh(returnStdout: true, script: 'npm show alauda-ui version').trim()
        }
      }
    }

    stage("Module Install") {
      steps {
        script {
          sh "npm install"
        }
      }
    }

    stage("Test") {
      parallel {
        stage("Code Scan") {
          steps {
            script {
              try {
                deploy.scan(
                  REPOSITORY,
                  GIT_BRANCH,
                  "",
                  ".",
                  DEBUG,
                  OWNER,
                  "").startToSonar()
              } catch (Exception exc) {
                echo "scan in sonar failed: ${exc}"
              }
            }
          }
        }
        stage("Test:ci") {
          steps {
            script {
              sh "npm run test:ci"
            }
          }
        }
      }
    }

    stage("Build Image") {
      when {
        expression {
          GIT_BRANCH == "master"
        }
      }
      steps {
        script {
          IMAGE = deploy.dockerBuild(
              "Dockerfile", //Dockerfile
              ".", // build context
              REPO_ADDRESS, // repo address
              GIT_COMMIT, // tag
              ORG_CREDENTIALS, // credentials for pushing
            )
            // start and push
          IMAGE.start().push(GIT_COMMIT)
        }
      }
    }

    stage("CD") {
      when {
        expression {
          GIT_BRANCH == "master"
        }
      }
      parallel {
        stage("Update Service") {
          steps {
            script {
              withCredentials([usernamePassword(credentialsId: UPDATE_SERVICE_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'UPDATE_SERVICE_TOKEN')]) {
                alaudaEE.setup(ENDPOINT, NAMESPACE, UPDATE_SERVICE_TOKEN)
                alaudaEE.updateService(APP_NAME, SPACE_NAME, GIT_COMMIT)
              }
            }
          }
        }
        stage("Publish Package") {
          when {
            expression {
              RELEASE_VERSION != REMOTE_VERSION
            }
          }
          steps {
            script {
              deploy.notificationTest(DEPLOYMENT, DINGDING_BOT, "AUI等待手动确认发版!", RELEASE_VERSION)
              input "Confirm to publish AUI(from ${REMOTE_VERSION} to ${RELEASE_VERSION}) ?"
              withCredentials([usernamePassword(credentialsId: NPM_ORG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'NPM_TOKEN')]) {
                sh """
                  npm run build
                  npm config set //registry.npmjs.org/:_authToken ${NPM_TOKEN}
                  npm publish release
                """
              }
              withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                sh """
                  git config --global user.email ${USERNAME}
                  git config credential.username ${USERNAME}
                  git config credential.helper '!echo password=${PASSWORD}; echo'
                  git tag -a 'v${RELEASE_VERSION}' -m 'Release from pipeline'
                  GIT_ASKPASS=true
                  git push origin --tags
                """
              }
            }
          }
        }
      }
    }
  }

  // happens at the end of the pipeline
  post {
    success {
      echo "Horay!"
      script {
        if (GIT_BRANCH == "master") {
          echo "Success in master"
          deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "AUI上线啦！", "v${RELEASE_VERSION}")
        } else {
          echo "Success"
          deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "AUI流水线完成了", RELEASE_BUILD)
        }
      }
    }
    failure {
        echo "damn!"
        script {
          echo "Failed"
          deploy.notificationFailed(DEPLOYMENT, DINGDING_BOT, "AUI流水线失败了", RELEASE_BUILD)
        }
    }
    aborted {
      echo "aborted!"
    }
  }
}