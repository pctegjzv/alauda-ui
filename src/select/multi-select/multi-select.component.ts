import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { ComponentSize } from '../../types';
import { Bem, buildBem } from '../../utils/bem';
import { BaseSelect } from '../base-select';
import { OptionComponent } from '../option/option.component';
import { TagClassFn } from '../select.types';

@Component({
  selector: 'aui-multi-select,aui-muti-select',
  templateUrl: './multi-select.component.html',
  styleUrls: [
    '../../input/input.component.scss',
    '../../tag/tag.component.scss',
    './multi-select.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiSelectComponent),
      multi: true,
    },
    {
      provide: BaseSelect,
      useExisting: MultiSelectComponent,
    },
  ],
})
export class MultiSelectComponent extends BaseSelect<any[]> {
  bem: Bem = buildBem('aui-multi-select');

  values$: Observable<any[]> = this.value$$.asObservable();

  @Input() tagClassFn: TagClassFn;
  @ViewChild('inputRef') inputRef: ElementRef;

  get rootClass() {
    const size = this.size || ComponentSize.Medium;
    return `aui-input ${this.bem.block(size)} ${
      this.disabled ? 'isDisabled' : ''
    } ${this.focused ? 'isFocused' : ''} ${
      this.displayClearable() ? 'isClearable' : ''
    }`;
  }

  get tagSize() {
    if (this.size === ComponentSize.Medium) {
      return ComponentSize.Small;
    } else if (this.size === ComponentSize.Large) {
      return ComponentSize.Medium;
    } else {
      return ComponentSize.Mini;
    }
  }

  get inputClass() {
    return `${this.bem.element('input', {
      hidden: this.inputReadonly,
    })} aui-tag aui-tag--${this.tagSize}`;
  }

  get displayPlaceholder() {
    return !this.selectedOptions.length && this.inputReadonly;
  }

  focused = false;
  selectedOptions: Array<OptionComponent | { value: string }> = [];
  trackByValue = (_: number, item: OptionComponent) => this.trackFn(item.value);

  constructor(cdr: ChangeDetectorRef, private renderer: Renderer2) {
    super(cdr);
  }

  writeValue(val: any[]) {
    this.value$$.next(val);
    this.updateSelectDisplay(val);
    setTimeout(() => {
      this.tooltipRef.updatePosition();
    });
    this.cdr.detectChanges();
  }

  onRemove(option: OptionComponent) {
    this.deleteOption(option);
    this.emitChange();
  }

  onShowOptions() {
    super.onShowOptions();
    this.inputRef.nativeElement.focus();
  }

  onHideOptions() {
    super.onHideOptions();
    this.inputRef.nativeElement.value = '';
    this.renderer.removeStyle(this.inputRef.nativeElement, 'width');
  }

  onInput(event: Event) {
    super.onInput(event);
    // TODO: optimize performance
    this.renderer.removeStyle(this.inputRef.nativeElement, 'width');
    this.renderer.setStyle(
      this.inputRef.nativeElement,
      'width',
      this.inputRef.nativeElement.scrollWidth + 'px',
    );
    this.tooltipRef.updatePosition();
  }

  onInputFocus() {
    this.focused = true;
  }

  onInputBlur() {
    this.focused = false;
    this.closeOption();
  }

  onKeyDown(event: KeyboardEvent) {
    if (
      event.key === 'Backspace' &&
      this.filterString === '' &&
      this.selectedOptions.length
    ) {
      this.onRemove(this.selectedOptions[
        this.selectedOptions.length - 1
      ] as OptionComponent);
      event.stopPropagation();
      event.preventDefault();
    } else if (event.key === 'Enter') {
      if (
        this.selectedOptions
          .map(option => this.trackFn(option.value))
          .includes((event.target as HTMLInputElement).value)
      ) {
        return;
      }
      this.selectFocusedOption();
      requestAnimationFrame(() => {
        this.autoFocusFirstOption();
      });
      event.stopPropagation();
      event.preventDefault();
    } else {
      super.onKeyDown(event);
    }
  }

  displayClearable() {
    return !this.disabled && this.clearable && this.selectedOptions.length;
  }

  clearValue(event: Event) {
    this.selectedOptions = [];
    this.emitChange();
    event.stopPropagation();
    event.preventDefault();
  }

  updateSelectDisplay(value: any[]) {
    this.selectedOptions = [];
    if (value) {
      value.forEach(item => {
        const pickedOption = this.contentOptions
          ? this.contentOptions.find(
              option => this.trackFn(option.value) === this.trackFn(item),
            )
          : null;
        if (pickedOption) {
          this.pushOption(pickedOption);
        } else {
          this.pushOption({ value: item });
        }
      });
    }
  }

  selectOption(option: OptionComponent) {
    if (this.selectedOptions.includes(option)) {
      this.deleteOption(option);
    } else {
      this.pushOption(option);
    }
    this.resetInput();
    this.emitChange();
  }

  private emitChange() {
    const values = this.selectedOptions.map(option => option.value);
    this.emitValueChange(values);
    setTimeout(() => {
      this.tooltipRef.updatePosition();
    });
  }

  private pushOption(option: OptionComponent | { value: string }) {
    this.selectedOptions = this.selectedOptions.concat(option);
  }

  private deleteOption(option: OptionComponent) {
    this.selectedOptions = this.selectedOptions.filter(item => item !== option);
  }

  private resetInput() {
    this.inputRef.nativeElement.value = '';
    this.filterString = '';
  }
}
