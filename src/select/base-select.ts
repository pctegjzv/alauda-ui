import {
  AfterContentInit,
  AfterViewInit,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  BehaviorSubject,
  combineLatest,
  merge,
  Observable,
  of,
  Subject,
} from 'rxjs';
import {
  distinctUntilChanged,
  map,
  mergeMap,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  switchMapTo,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { CommonForm } from '../form/public-api';
import { TooltipDirective } from '../tooltip/public-api';
import { ComponentSize } from '../types';
import { coerceAttrBoolean } from '../utils/coercion';
import { scrollIntoView } from '../utils/scroll-into-view';
import { OptionComponent } from './option/option.component';
import { OptionFilterFn, TrackFn } from './select.types';

export abstract class BaseSelect<T> extends CommonForm<T>
  implements AfterContentInit, AfterViewInit, OnDestroy {
  @Input()
  get size() {
    return this._size;
  }
  set size(val) {
    if (!val || this._size === val) {
      return;
    }
    this._size = val;
    this.size$$.next(val);
  }
  @Input() loading = false;
  @Input() placeholder = '';
  @Input()
  get filterable() {
    return this._filterable;
  }
  set filterable(val) {
    this._filterable = coerceAttrBoolean(val);
  }
  @Input()
  get clearable() {
    return this._clearable;
  }
  set clearable(val) {
    this._clearable = coerceAttrBoolean(val);
  }
  @Input()
  get filterFn() {
    return this._filterFn;
  }
  set filterFn(val) {
    if (val !== this._filterFn) {
      this._filterFn = val;
      this.filterFn$$.next(val);
    }
  }
  @Input()
  get trackFn() {
    return this._trackFn;
  }
  set trackFn(val) {
    if (val !== this._trackFn) {
      this._trackFn = val;
      this.trackFn$$.next(val);
    }
  }
  @Input()
  get allowCreate() {
    return this._allowCreate;
  }
  set allowCreate(val) {
    this._allowCreate = coerceAttrBoolean(val);
  }
  @Input() defaultFirstOption = true;
  @Output() filterChange = new EventEmitter<string>();
  @Output() show = new EventEmitter<void>();
  @Output() hide = new EventEmitter<void>();

  @ViewChild('selectRef') protected selectRef: ElementRef;
  @ViewChild('tooltipRef') protected tooltipRef: TooltipDirective;
  @ViewChild('optionListRef') protected optionListRef: ElementRef;
  @ViewChild('inputingOption') protected inputingOption: OptionComponent;
  @ViewChildren(OptionComponent) customOptions: QueryList<OptionComponent>;
  @ContentChildren(OptionComponent, { descendants: true })
  contentOptions: QueryList<OptionComponent>;

  /**
   * Utility field to make sure the users always see the value as type array
   */
  abstract readonly values$: Observable<any[]>;

  get allOptions() {
    return [].concat(
      this.customOptions ? this.customOptions.toArray() : [],
      this.contentOptions ? this.contentOptions.toArray() : [],
    );
  }

  allOptions$: Observable<OptionComponent[]>;

  private _size = ComponentSize.Medium;
  private _filterable = false;
  private _clearable = false;
  private _filterString = '';
  private _allowCreate = false;
  private filterString$$ = new BehaviorSubject<string>(this.filterString);
  private filterFn$$ = new BehaviorSubject<OptionFilterFn>(this.filterFn);
  private trackFn$$ = new BehaviorSubject<TrackFn>(this.trackFn);

  protected focusedOption: OptionComponent;
  protected size$$ = new BehaviorSubject<ComponentSize>(this.size);

  /**
   * for includes validator
   */
  protected contentOptionsChanged$$ = new Subject<void>();
  protected destroy$$ = new Subject<void>();

  size$: Observable<ComponentSize> = this.size$$.asObservable();
  trackFn$: Observable<TrackFn> = this.trackFn$$.asObservable();
  filterString$: Observable<string> = this.filterString$$.asObservable();
  filterFn$: Observable<OptionFilterFn> = this.filterFn$$.asObservable();
  contentOptionsChanged$: Observable<
    void
  > = this.contentOptionsChanged$$.asObservable();
  hasVisableOption$: Observable<boolean>;
  hasMatchedOption$: Observable<boolean>;
  customCreatedValues$: Observable<string[]>;
  containerWidth: string;
  get opened() {
    return this.tooltipRef.isCreated;
  }
  get inputReadonly() {
    return !(this.filterable && this.opened);
  }
  get filterString() {
    return this._filterString;
  }
  set filterString(val) {
    if (val !== this._filterString) {
      this._filterString = val;
      this.filterString$$.next(val);
      this.filterChange.emit(val);
    }
  }

  ngAfterContentInit() {
    this.customCreatedValues$ = combineLatest(
      this.values$,
      this.contentOptions.changes.pipe(
        startWith(this.contentOptions),
        switchMap(options =>
          combineLatest(options.map(option => option.value$)),
        ),
      ),
      this.trackFn$,
    ).pipe(
      map(([values, optionValues, trackFn]) =>
        values
          .filter(
            value =>
              !optionValues.map(v => trackFn(v)).includes(trackFn(value)),
          )
          .map(value => trackFn(value)),
      ),
      publishReplay(1),
      refCount(),
    );

    this.contentOptions.changes
      .pipe(
        startWith(this.contentOptions),
        takeUntil(this.destroy$$),
        tap(() => {
          this.contentOptionsChanged$$.next();
        }),
        mergeMap(options =>
          combineLatest(
            ...options.map(option => merge(option.value$, option.label$)),
          ),
        ),
        switchMapTo(this.value$),
      )
      .subscribe(value => {
        this.updateSelectDisplay(value);
        this.cdr.markForCheck();
      });
  }

  ngAfterViewInit() {
    this.allOptions$ = combineLatest(
      this.customOptions.changes.pipe(startWith(this.customOptions)),
      this.contentOptions.changes.pipe(startWith(this.contentOptions)),
    ).pipe(
      map(([customOptions, contentOptions]) => [
        ...customOptions.toArray(),
        ...contentOptions.toArray(),
      ]),
    );

    this.hasVisableOption$ = this.allOptions$.pipe(
      switchMap(options => {
        if (options.length) {
          return combineLatest(options.map(option => option.visable$));
        } else {
          return of([false]);
        }
      }),
      map(values => values.some(value => value)),
      distinctUntilChanged(),
      tap(() => {
        this.cdr.detectChanges();
      }),
      publishReplay(1),
      refCount(),
    );

    this.hasMatchedOption$ = combineLatest(
      this.allOptions$.pipe(
        map(customOptions =>
          customOptions.filter(option => option !== this.inputingOption),
        ),
        switchMap(options =>
          combineLatest(options.map(option => option.value$)),
        ),
      ),
      this.filterString$,
    ).pipe(
      map(([values, filterstring]) =>
        values.some(value => this.trackFn(value) === filterstring),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  openOption() {
    this.tooltipRef.createTooltip();
  }

  closeOption() {
    this.tooltipRef.disposeTooltip();
  }

  onShowOptions() {
    this.containerWidth = this.selectRef.nativeElement.offsetWidth + 'px';
    this.show.emit();

    requestAnimationFrame(() => {
      this.autoFocusFirstOption();
    });
  }

  onHideOptions() {
    if (this.onTouched) {
      this.onTouched();
    }
    this.resetFocusedOption();
    this.filterString = '';
    this.hide.emit();
  }

  onInput(event: Event) {
    const value = (event.target as HTMLInputElement).value;
    this.filterString = value;

    requestAnimationFrame(() => {
      this.autoFocusFirstOption();
    });

    this.cdr.markForCheck();
  }

  onKeyDown(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowDown':
        this.focusOptionDir('down');
        event.stopPropagation();
        event.preventDefault();
        break;
      case 'ArrowUp':
        this.focusOptionDir('up');
        event.stopPropagation();
        event.preventDefault();
        break;
      case 'Enter':
        this.selectFocusedOption();
        event.stopPropagation();
        event.preventDefault();
        break;
      case 'Escape':
        this.escSelect();
        event.stopPropagation();
        event.preventDefault();
        break;
    }
  }

  onOptionClick(option: OptionComponent) {
    this.resetFocusedOption(option);
    this.selectOption(option);
  }

  protected autoFocusFirstOption() {
    if (this.defaultFirstOption && this.allowCreate && this.filterString) {
      const matchedOption = this.contentOptions.find(
        option => this.trackFn(option.value) === this.filterString,
      );
      this.resetFocusedOption(matchedOption || this.customOptions.first);
      return;
    }

    const selectedOption = this.allOptions.find(
      option => option.selected && option.visable,
    );
    if (this.defaultFirstOption) {
      this.resetFocusedOption(
        selectedOption ||
          this.allOptions.find(option => option.visable && !option.disabled),
      );
    } else if (selectedOption) {
      this.scrollToOption(selectedOption);
    }
  }

  protected focusOptionDir(dir: 'down' | 'up') {
    if (!this.opened) {
      this.openOption();
      return;
    }
    const visableOptions = this.allOptions.filter(
      option => option.visable && !option.disabled,
    );
    if (!visableOptions.length) {
      return;
    }
    const step = dir === 'down' ? 1 : -1;
    let i = visableOptions.findIndex(option => option === this.focusedOption);
    i = i + step;
    if (i >= visableOptions.length) {
      i = 0;
    } else if (i < 0) {
      i = visableOptions.length - 1;
    }
    this.resetFocusedOption(visableOptions[i]);
  }

  protected resetFocusedOption(focusedOption: OptionComponent = null) {
    if (this.focusedOption === focusedOption) {
      return;
    }

    if (this.focusedOption) {
      this.focusedOption.blur();
    }
    this.focusedOption = focusedOption;
    if (this.focusedOption) {
      this.focusedOption.focus();
      this.scrollToOption(this.focusedOption);
    }
  }

  protected scrollToOption(option: OptionComponent) {
    if (this.optionListRef) {
      scrollIntoView(
        this.optionListRef.nativeElement,
        option.elRef.nativeElement,
      );
    }
  }

  protected selectFocusedOption() {
    if (!this.opened) {
      this.openOption();
      return;
    }
    if (this.focusedOption) {
      this.selectOption(this.focusedOption);
    }
  }

  protected escSelect() {
    this.closeOption();
  }

  private _filterFn(filterString: string, option: OptionComponent) {
    return (option.label || option.value).toString().includes(filterString);
  }

  private _trackFn(value: any) {
    return value;
  }

  abstract selectOption(option: OptionComponent): void;
  abstract updateSelectDisplay(value: T): void;
  abstract clearValue(event: Event): void;
}
