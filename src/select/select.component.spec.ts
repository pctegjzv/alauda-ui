import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, DebugElement, ViewChild } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ComponentSize } from '../types';
import { sleep } from '../utils/async';
import { SelectComponent, SelectModule } from './public-api';

describe('SelectComponent', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ins: TestComponent;
  let debugEl: DebugElement;
  let el: HTMLElement;
  let inputEl: HTMLInputElement;
  let ocEl: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SelectModule, FormsModule],
      declarations: [TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    ins = fixture.componentInstance;
    debugEl = fixture.debugElement.query(By.css('.aui-select'));
    el = debugEl.nativeElement;
    inputEl = debugEl.query(By.css('.aui-input'))
      .nativeElement as HTMLInputElement;

    inject([OverlayContainer], (overlayContainer: OverlayContainer) => {
      ocEl = overlayContainer.getContainerElement();
    })();
  });

  it('should properties work correctly', () => {
    expect(inputEl.placeholder).toBe('');
    expect(inputEl.disabled).toBeFalsy();
    expect(inputEl.className).toContain('aui-input--medium');
    ins.selectRef.contentOptions.forEach(option => {
      expect(option.size).toBe(ComponentSize.Medium);
    });

    ins.disabled = true;
    ins.loading = true;
    ins.clearable = true;
    ins.size = ComponentSize.Large;
    ins.placeholder = 'placeholder';
    fixture.detectChanges();

    expect(inputEl.placeholder).toBe('placeholder');
    expect(inputEl.disabled).toBeTruthy();
    expect(inputEl.className).toContain('aui-input--large');
    ins.selectRef.contentOptions.forEach(option => {
      expect(option.size).toBe(ComponentSize.Large);
    });
  });

  it('should ngModel work', async done => {
    el.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    const optionEls = ocEl.querySelectorAll('.aui-option');
    expect(optionEls.item(0).className).toContain('isSelected');
    expect(inputEl.value).toBe('1');
    ins.value = 5;
    fixture.detectChanges();
    await sleep();
    expect(inputEl.value).toBe('5');

    optionEls.item(1).dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(ins.value).toBe(2);
    expect(inputEl.value).toBe('2');
    done();
  });

  it('should clearable work', () => {
    ins.clearable = true;
    fixture.detectChanges();

    expect(ins.value).toBe(1);
    expect(inputEl.value).toBe('1');

    const closeEl = el.querySelector('.aui-select__clear');
    closeEl.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(ins.value).toBe('');
    expect(inputEl.value).toBe('');
  });

  it('should validator work', async done => {
    const hostEl = fixture.debugElement.query(By.css('aui-select'))
      .nativeElement;
    expect(hostEl.className).toContain('ng-valid');
    ins.value = 4;
    fixture.detectChanges();
    await sleep();
    fixture.detectChanges();
    expect(hostEl.className).toContain('ng-invalid');
    done();
  });

  it('should label correctly rendered', async done => {
    inputEl = debugEl.query(By.css('.aui-input'))
      .nativeElement as HTMLInputElement;
    ins.customLabelFn = (val: number) => `custom label for ${val}`;
    expect(inputEl.value).toEqual('1');
    fixture.detectChanges();
    await sleep();
    expect(inputEl.value).toEqual('custom label for 1');
    ins.customLabelFn = (val: number) => `new custom label for ${val}`;
    fixture.detectChanges();
    await sleep();
    expect(inputEl.value).toEqual('new custom label for 1');
    done();
  });
});

@Component({
  template: `
    <aui-select
        #selectRef
        [(ngModel)]="value"
        [disabled]="disabled"
        [clearable]="clearable"
        [size]="size"
        [loading]="loading"
        [placeholder]="placeholder"
        includes>
        <aui-option *ngFor="let val of options"
                    [value]="val"
                    [label]="customLabelFn(val)"
                    [disabled]="disabledOptions.includes(val)">
                    {{ val }}
        </aui-option>
    </aui-select>
    `,
})
class TestComponent {
  value = 1;
  disabled: boolean;
  clearable: boolean;
  size: ComponentSize;
  loading: boolean;
  placeholder = '';

  options = [1, 2, 3, 4];
  disabledOptions = [4];

  @ViewChild('selectRef') selectRef: SelectComponent;

  customLabelFn = (val: number) => `${val}`;
}
