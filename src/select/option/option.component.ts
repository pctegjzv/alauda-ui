import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, combineLatest, merge, Observable } from 'rxjs';
import { map, mapTo, publishReplay, refCount, tap } from 'rxjs/operators';
import { ComponentSize } from '../../types';
import { Bem, buildBem } from '../../utils/bem';
import { coerceAttrBoolean } from '../../utils/coercion';
import { BaseSelect } from '../base-select';

@Component({
  selector: 'aui-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class OptionComponent {
  bem: Bem = buildBem('aui-option');

  private _disabled = false;
  private _label: string;
  private _value: any;
  private label$$ = new BehaviorSubject<string>(this.label);
  private value$$ = new BehaviorSubject<any>(this.value);

  @Input()
  get label() {
    return this._label;
  }
  set label(val) {
    this._label = val;
    this.label$$.next(val);
  }
  @Input()
  get value() {
    return this._value;
  }
  set value(val) {
    this._value = val;
    this.value$$.next(val);
  }
  @Input()
  get disabled() {
    return this._disabled;
  }
  set disabled(val: any) {
    this._disabled = coerceAttrBoolean(val);
  }

  @ViewChild('elRef') elRef: ElementRef;

  selected = false;
  visable = true;
  size = ComponentSize.Medium;
  focused = false;

  value$: Observable<any> = this.value$$.asObservable();
  label$: Observable<string> = this.label$$.asObservable();
  size$: Observable<ComponentSize> = this.select.size$.pipe(
    tap(size => {
      this.size = size;
    }),
  );

  selected$: Observable<boolean> = combineLatest(
    this.select.trackFn$,
    this.select.values$,
    this.value$$,
  ).pipe(
    map(
      ([trackFn, selectValue, selfValue]) =>
        selectValue &&
        selectValue
          .filter((value: any) => !!value)
          .map(trackFn)
          .includes(trackFn(selfValue)),
    ),
    tap(selected => {
      this.selected = selected;
    }),
    publishReplay(1),
    refCount(),
  );

  visable$: Observable<boolean> = combineLatest(
    this.select.filterFn$,
    this.select.filterString$,
    merge(this.label$$, this.value$$).pipe(mapTo(this)),
  ).pipe(
    map(([filterFn, filterString, option]) => filterFn(filterString, option)),
    tap(visable => {
      this.visable = visable;
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private cdr: ChangeDetectorRef,
    @Inject(forwardRef(() => BaseSelect))
    private select: BaseSelect<any | any[]>,
  ) {}

  onClick() {
    if (this.disabled) {
      return;
    }
    this.select.onOptionClick(this);
  }

  focus() {
    if (this.disabled) {
      return;
    }
    this.focused = true;
    this.cdr.markForCheck();
  }

  blur() {
    this.focused = false;
    this.cdr.markForCheck();
  }
}
