import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  forwardRef,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { OptionComponent } from '../option/option.component';

@Component({
  selector: 'aui-option-group',
  templateUrl: './option-group.component.html',
  styleUrls: ['./option-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class OptionGroupComponent implements AfterContentInit {
  @ContentChildren(forwardRef(() => OptionComponent))
  options: QueryList<OptionComponent>;

  hasVisableOption$: Observable<boolean>;

  ngAfterContentInit() {
    this.hasVisableOption$ = this.options.changes.pipe(
      startWith(this.options),
      switchMap(options => {
        if (options.length) {
          return combineLatest(options.map(node => node.visable$));
        } else {
          return of([false]);
        }
      }),
      map(values => values.some(value => value)),
      publishReplay(1),
      refCount(),
    );
  }
}
