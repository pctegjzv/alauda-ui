import { OptionComponent } from './option/option.component';

export type OptionFilterFn = (
  filter: string,
  option: OptionComponent,
) => boolean;

export type TrackFn = (value: any) => any;

export type TagClassFn = (
  label: string,
  value: any,
) => string | string[] | Set<string> | { [klass: string]: any };
