import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { InputComponent } from '../input/public-api';
import { coerceString } from '../utils/coercion';
import { BaseSelect } from './base-select';
import { OptionComponent } from './option/option.component';

@Component({
  selector: 'aui-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
    {
      provide: BaseSelect,
      useExisting: SelectComponent,
    },
  ],
})
export class SelectComponent extends BaseSelect<any> {
  @ViewChild('inputRef') inputRef: InputComponent;

  displayText = '';

  values$: Observable<any[]> = this.value$$
    .asObservable()
    .pipe(map(val => [val]));

  get rootClass() {
    return `aui-select aui-select--${this.size} ${
      this.displayClearable() ? 'isClearable' : ''
    }`;
  }

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  writeValue(val: any) {
    this.value$$.next(val);
    this.updateSelectDisplay(val);
    this.cdr.detectChanges();
  }

  onShowOptions() {
    super.onShowOptions();
    this.inputRef.elementRef.nativeElement.focus();
  }

  getPlaceholder() {
    return this.filterable && this.opened ? this.displayText : this.placeholder;
  }

  getInputValue() {
    return this.filterable && this.opened ? '' : this.displayText;
  }

  displayClearable() {
    return !this.disabled && this.clearable && this.getInputValue();
  }

  clearValue(event: Event) {
    this.displayText = '';
    this.emitValueChange('');
    event.stopPropagation();
    event.preventDefault();
  }

  private getLabelFromOption(option: OptionComponent) {
    return option.label || this.trackFn(option.value);
  }

  updateSelectDisplay(value: any) {
    const val = value;
    const pickedOption = this.contentOptions
      ? this.contentOptions.find(
          option => this.trackFn(option.value) === this.trackFn(val),
        )
      : null;
    if (pickedOption) {
      this.displayText = this.getLabelFromOption(pickedOption);
    } else {
      this.displayText = coerceString(val ? this.trackFn(val) : val);
    }

    this.cdr.markForCheck();
  }

  selectOption(option: OptionComponent) {
    if (option.selected) {
      return;
    }
    this.displayText = this.getLabelFromOption(option);
    this.emitValueChange(option.value);
    this.closeOption();
  }
}
