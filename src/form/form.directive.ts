import { Directive, Input } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { LabelPosition } from './form.types';

@Directive({
  selector: 'form[auiForm]',
  exportAs: 'auiForm',
  host: {
    '[class.aui-form]': 'true',
    '[class.aui-form--inline]': 'inline',
  },
})
export class FormDirective {
  private labelWidth$$ = new BehaviorSubject<string>('auto');
  private labelPostion$$ = new BehaviorSubject<LabelPosition>(
    LabelPosition.Right,
  );
  private emptyAddon$$ = new BehaviorSubject<boolean>(false);

  @Input('auiFormLabelWidth')
  set labelWidth(val: string) {
    this.labelWidth$$.next(val);
  }
  @Input('auiFormLabelPosition')
  set labelPosition(val: LabelPosition) {
    this.labelPostion$$.next(val);
  }
  @Input('auiFormEmptyAddon')
  set emptyAddon(val: boolean) {
    this.emptyAddon$$.next(val);
  }
  @Input('auiFormInline') inline = false;

  labelWidth$: Observable<string> = this.labelWidth$$
    .asObservable()
    .pipe(distinctUntilChanged());
  labelPostion$: Observable<
    LabelPosition
  > = this.labelPostion$$.asObservable().pipe(distinctUntilChanged());
  emptyAddon$: Observable<boolean> = this.emptyAddon$$
    .asObservable()
    .pipe(distinctUntilChanged());
}
