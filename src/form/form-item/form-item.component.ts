import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { combineLatest, merge, Observable, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
} from 'rxjs/operators';
import { Bem, buildBem } from '../../utils/bem';
import { FormDirective } from '../form.directive';
import { LabelPosition } from '../form.types';
import {
  FormItemAddonDirective,
  FormItemControlDirective,
  FormItemErrorDirective,
  FormItemHintDirective,
} from '../helper-directives';

@Component({
  selector: 'aui-form-item',
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class FormItemComponent implements OnInit, AfterContentInit, OnDestroy {
  bem: Bem = buildBem('aui-form-item');

  @Input() labelWidth = 'auto';
  @Input() labelPosition = LabelPosition.Right;
  @Input() emptyAddon = false;

  @ContentChild(FormItemControlDirective) itemControl: FormItemControlDirective;
  @ContentChild(NgControl) ngControl: NgControl;
  @ContentChildren(FormItemAddonDirective)
  addons: QueryList<FormItemAddonDirective>;
  @ContentChildren(FormItemErrorDirective)
  errors: QueryList<FormItemErrorDirective>;
  @ContentChildren(FormItemHintDirective)
  hints: QueryList<FormItemHintDirective>;

  hasError$: Observable<boolean>;
  parentForm: NgForm | FormGroupDirective;

  private destory$$ = new Subject<void>();

  constructor(
    private cdr: ChangeDetectorRef,
    @Optional() private auiForm: FormDirective,
    @Optional() ngForm: NgForm,
    @Optional() formGroup: FormGroupDirective,
  ) {
    this.parentForm = formGroup || ngForm;
  }

  ngOnInit() {
    if (this.auiForm) {
      this.auiForm.labelWidth$
        .pipe(takeUntil(this.destory$$))
        .subscribe(width => {
          this.labelWidth = width;
          this.cdr.markForCheck();
        });
      this.auiForm.labelPostion$
        .pipe(takeUntil(this.destory$$))
        .subscribe(position => {
          this.labelPosition = position;
          this.cdr.markForCheck();
        });
      this.auiForm.emptyAddon$
        .pipe(takeUntil(this.destory$$))
        .subscribe(emptyAddon => {
          this.emptyAddon = emptyAddon;
          this.cdr.markForCheck();
        });
    }
  }

  ngAfterContentInit() {
    if (this.ngControl) {
      this.hasError$ = (this.parentForm
        ? combineLatest(
            this.ngControl.statusChanges.pipe(startWith(this.ngControl.status)),
            merge(this.parentForm.statusChanges, this.parentForm.ngSubmit),
          ).pipe(map(([status]: string[]) => status))
        : this.ngControl.statusChanges
      ).pipe(
        map(
          (status: string) =>
            status === 'INVALID' &&
            (this.ngControl.dirty ||
              (this.parentForm && this.parentForm.submitted)),
        ),
        publishReplay(1),
        refCount(),
      );
    }
  }

  ngOnDestroy() {
    this.destory$$.next();
  }
}
