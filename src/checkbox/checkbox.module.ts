import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IconModule } from '../icon/public-api';
import { CheckboxGroupComponent } from './checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './checkbox.component';

@NgModule({
  imports: [CommonModule, FormsModule, IconModule],
  declarations: [CheckboxComponent, CheckboxGroupComponent],
  exports: [CheckboxComponent, CheckboxGroupComponent],
})
export class CheckboxModule {}
