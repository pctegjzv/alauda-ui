import { FocusMonitor } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { CommonForm } from '../form/public-api';
import { CheckboxGroupComponent } from './checkbox-group/checkbox-group.component';

let uniqueId = 0;

@Component({
  selector: 'aui-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
})
export class CheckboxComponent extends CommonForm<boolean>
  implements OnInit, AfterViewInit, OnDestroy {
  id = `aui-checkbox-${uniqueId++}`;

  @Input() name = '';
  @Input()
  get label() {
    return this._label;
  }
  set label(val) {
    this._label = val;
    this.label$$.next(val);
  }

  @ViewChild('elRef') elRef: ElementRef;

  checked = false;

  private _label: any;
  private label$$ = new BehaviorSubject<any>(this.label);
  private destory$$ = new Subject<void>();

  constructor(
    cdr: ChangeDetectorRef,
    @Optional()
    @Inject(forwardRef(() => CheckboxGroupComponent))
    private checkboxGroup: CheckboxGroupComponent,
    private focusMonitor: FocusMonitor,
  ) {
    super(cdr);
  }

  ngOnInit() {
    if (this.checkboxGroup) {
      combineLatest(this.checkboxGroup.value$, this.label$$)
        .pipe(
          takeUntil(this.destory$$),
          map(([value, label]) => value && value.includes(label)),
        )
        .subscribe(checked => {
          this.checked = checked;
          this.cdr.markForCheck();
        });
    }
  }

  ngAfterViewInit() {
    this.focusMonitor.monitor(this.elRef.nativeElement, true);
  }

  ngOnDestroy() {
    this.destory$$.next();
    this.focusMonitor.stopMonitoring(this.elRef.nativeElement);
  }

  writeValue(value: boolean) {
    this.checked = value;
    this.cdr.detectChanges();
  }

  onClick() {
    if (this.disabled) {
      return;
    }
    this.checked = !this.checked;
    this.emitValueChange(this.checked);
    if (this.checkboxGroup) {
      this.checkboxGroup.onCheckboxChange();
    }
  }

  onBlur() {
    if (this.onTouched) {
      this.onTouched();
    }
    if (this.checkboxGroup) {
      this.checkboxGroup.onCheckboxBlur();
    }
  }
}
