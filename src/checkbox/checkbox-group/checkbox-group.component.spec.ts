import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { sleep } from '../../utils/async';
import { CheckboxModule } from '../checkbox.module';

describe('CheckboxGroupComponent', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ins: TestComponent;
  let el: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, CheckboxModule],
      declarations: [TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    ins = fixture.componentInstance;
    el = fixture.debugElement.query(By.css('aui-checkbox-group')).nativeElement;
  });

  it('should checkbox init checked state by value array', async () => {
    ins.value = ['box1', 'box2', 'box3'];
    fixture.detectChanges();
    await sleep();
    fixture.detectChanges();

    const boxes = el.querySelectorAll('.aui-checkbox');
    expect(boxes.item(0).className).not.toContain('isChecked');
    expect(boxes.item(1).className).toContain('isChecked');
    expect(boxes.item(2).className).toContain('isChecked');
    expect(boxes.item(3).className).toContain('isChecked');
  });

  it('should value change when checkbox clicked', () => {
    const boxes = el.querySelectorAll('.aui-checkbox label');
    boxes.item(0).dispatchEvent(new Event('click'));
    boxes.item(1).dispatchEvent(new Event('click'));
    boxes.item(2).dispatchEvent(new Event('click'));
    boxes.item(3).dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(ins.value).toEqual(['box0', 'box1', 'box2']);
  });
});

@Component({
  template: `
  <aui-checkbox-group [(ngModel)]="value">
    <aui-checkbox label="box0">box0</aui-checkbox>
    <aui-checkbox label="box1">box1</aui-checkbox>
    <aui-checkbox label="box2">box2</aui-checkbox>
    <aui-checkbox label="box3" disabled>box3</aui-checkbox>
  </aui-checkbox-group>`,
})
class TestComponent {
  value: string[];
}
