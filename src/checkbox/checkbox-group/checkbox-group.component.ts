import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  forwardRef,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonForm } from '../../form/public-api';
import { CheckboxComponent } from '../checkbox.component';

@Component({
  selector: 'aui-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxGroupComponent),
      multi: true,
    },
  ],
})
export class CheckboxGroupComponent extends CommonForm<any[]> {
  @ContentChildren(forwardRef(() => CheckboxComponent))
  checkboxes: QueryList<CheckboxComponent>;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  writeValue(val: any[]) {
    this.value$$.next(val);
    this.cdr.detectChanges();
  }

  onCheckboxChange() {
    if (this.onTouched) {
      this.onTouched();
    }
    const values = this.checkboxes
      .filter(item => item.checked)
      .map(item => item.label);
    this.emitValueChange(values);
  }

  onCheckboxBlur() {
    if (this.onTouched) {
      this.onTouched();
    }
  }
}
