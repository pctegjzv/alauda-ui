import { TemplatePortal } from '@angular/cdk/portal';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';

import { coerceAttrBoolean } from '../utils/coercion';
import { TabLabelDirective } from './tab-directives';

@Component({
  selector: 'aui-tab',
  exportAs: 'auiTab',
  templateUrl: './tab.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class TabComponent implements OnInit, OnDestroy, OnChanges {
  /** Plain text label for the tab, used when there is no template label. */
  @Input('label') textLabel = '';

  /** Whether or not to show the close button in the header */
  @Input() closeable = false;

  /** Whether or not the tab is disabled  */
  @Input()
  get disabled() {
    return this._disabled;
  }

  set disabled(value: any) {
    this._disabled = coerceAttrBoolean(value);
  }

  @Output() close = new EventEmitter<void>();

  /** Content for the tab label given by `<ng-template [auiTabLabel]>`. */
  @ContentChild(TabLabelDirective) templateLabel: TabLabelDirective;

  /** Template inside the AuiTab view that contains an `<ng-content>`. */
  @ViewChild(TemplateRef) _implicitContent: TemplateRef<any>;

  get content(): TemplatePortal | null {
    return this._contentPortal;
  }

  /**
   * Whether the tab is currently active.
   */
  isActive = false;

  /**
   * The relatively indexed position where 0 represents the center, negative is left, and positive
   * represents the right.
   */
  position: number | null = null;

  /**
   * The initial relatively index origin of the tab if it was created and selected after there
   * was already a selected tab. Provides context of what position the tab should originate from.
   */
  origin: number | null = null;

  /** Emits whenever the label changes. */
  readonly _labelChange = new Subject<void>();

  /** Emits whenever the disable changes */
  readonly _disableChange = new Subject<void>();

  /** Emits whenever the closeable changes */
  readonly _closeableChange = new Subject<void>();

  /** Portal that will be the hosted content of the tab */
  private _contentPortal: TemplatePortal | null = null;

  private _disabled = false;

  constructor(private _viewContainerRef: ViewContainerRef) {}

  ngOnInit(): void {
    this._contentPortal = new TemplatePortal(
      this._implicitContent,
      this._viewContainerRef,
    );
  }

  ngOnDestroy(): void {
    this._disableChange.complete();
    this._labelChange.complete();
    this._closeableChange.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('textLabel')) {
      this._labelChange.next();
    }

    if (changes.hasOwnProperty('disabled')) {
      this._disableChange.next();
    }

    if (changes.hasOwnProperty('closeable')) {
      this._closeableChange.next();
    }
  }
}
