import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { CommonForm } from '../../form/public-api';
import { RadioSize } from '../radio.types';

@Component({
  selector: 'aui-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true,
    },
  ],
})
export class RadioGroupComponent extends CommonForm<any> {
  private size$$ = new BehaviorSubject<RadioSize>(RadioSize.Medium);
  private isPlain$$ = new BehaviorSubject<boolean>(true);
  private name$$ = new BehaviorSubject<string>('');

  @Input()
  set size(val: RadioSize) {
    this.size$$.next(val);
  }

  @Input()
  set isPlain(val: boolean) {
    this.isPlain$$.next(val);
  }

  @Input()
  set name(val: string) {
    this.name$$.next(val);
  }

  size$: Observable<RadioSize> = this.size$$
    .asObservable()
    .pipe(distinctUntilChanged());
  isPlain$: Observable<boolean> = this.isPlain$$
    .asObservable()
    .pipe(distinctUntilChanged());
  name$: Observable<string> = this.name$$
    .asObservable()
    .pipe(distinctUntilChanged());

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  writeValue(value: any) {
    this.value$$.next(value);
    this.cdr.detectChanges();
  }

  async onRadioChange(value: any) {
    this.emitValueChange(value);
  }

  onRadioBlur() {
    if (this.onTouched) {
      this.onTouched();
    }
  }
}
