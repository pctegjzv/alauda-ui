declare module 'css-element-queries';
declare class ResizeSensor {
  constructor(element: Element | Element[], callback: (...args: any[]) => any);
  detach(callback: (...args: any[]) => any): void;
}

declare module 'lodash.debounce';
declare function debounce<F = (...args: any[]) => any>(
  func: F,
  wait: number,
  options: { leading: boolean; maxWait: number; trailing: boolean },
): F;

declare module 'lodash.isequal';
declare function isEqual(value: any, value: any): boolean;
