export enum InlineAlertType {
  Primary = 'primary',
  Success = 'success',
  Warning = 'warning',
  Error = 'error',
}
