import { InlineAlertType } from './inline-alert.types';

export const iconMap = {
  [InlineAlertType.Primary]: 'info_circle_s',
  [InlineAlertType.Success]: 'check_circle_s',
  [InlineAlertType.Warning]: 'exclamation_circle_s',
  [InlineAlertType.Error]: 'exclamation_triangle_s',
};
