import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { sleep } from '../utils/async';
import { SwitchModule } from './public-api';

describe('SwitchComponent', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ins: TestComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SwitchModule, FormsModule],
      declarations: [TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    ins = fixture.componentInstance;
  });

  it('should render correct with click event', () => {
    // not-switched => switched
    fixture.debugElement
      .query(By.css('#switch2 .aui-switch'))
      .nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#switch2 .aui-switch')).nativeElement
        .className,
    ).toContain('aui-switch--checked');

    // switched => not-switched
    fixture.debugElement
      .query(By.css('#switch1 .aui-switch'))
      .nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#switch1 .aui-switch')).nativeElement
        .className,
    ).not.toContain('aui-switch--checked');

    // disabled: switched => switched
    fixture.debugElement
      .query(By.css('#switch3 .aui-switch'))
      .nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#switch3 .aui-switch')).nativeElement
        .className,
    ).toContain('aui-switch--checked');
  });

  it('should render correct with ngModel', async done => {
    fixture.debugElement
      .query(By.css('#switch4 .aui-switch'))
      .nativeElement.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(ins.switchMap.d).toBe(false);
    ins.switchMap.d = true;
    fixture.detectChanges();
    await sleep();
    expect(
      fixture.debugElement.query(By.css('#switch4 .aui-switch')).nativeElement
        .className,
    ).toContain('aui-switch--checked');
    done();
  });
});

@Component({
  template: `
    <aui-switch id="switch1" [(ngModel)]="switchMap.a"></aui-switch>
    <aui-switch id="switch2" [(ngModel)]="switchMap.b"></aui-switch>
    <aui-switch id="switch3" [(ngModel)]="switchMap.c" [disabled]="true"></aui-switch>
    <aui-switch id="switch4" [(ngModel)]="switchMap.d" [loading]="true"></aui-switch>
  `,
})
export class TestComponent {
  switchMap = { a: true, b: false, c: true, d: true };
}
