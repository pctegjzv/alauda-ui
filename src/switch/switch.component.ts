import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonForm } from '../form/public-api';
import { Bem, buildBem } from '../utils/bem';

const prefix = 'aui-switch';

@Component({
  selector: 'aui-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true,
    },
  ],
})
export class SwitchComponent extends CommonForm<boolean> {
  bem: Bem = buildBem(prefix);

  checked: boolean;

  @Input() loading = false;
  @Input() beforeSwitch: (reslove: () => void, reject: () => void) => void;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  get classesMap() {
    return this.bem.block({
      disabled: this.disabled,
      checked: this.checked,
      loading: this.loading,
    });
  }

  writeValue(value: boolean): void {
    this.checked = value;
    this.cdr.detectChanges();
  }

  async onSwitch() {
    if (this.disabled) {
      return;
    }
    if (!this.beforeSwitch) {
      this.switch();
      return;
    }
    try {
      await new Promise(this.beforeSwitch);
      this.switch();
    } catch (err) {
    } finally {
      this.cdr.markForCheck();
    }
  }

  onBlur() {
    if (this.onTouched) {
      this.onTouched();
    }
  }

  private switch() {
    this.checked = !this.checked;
    this.emitValueChange(this.checked);
  }
}
