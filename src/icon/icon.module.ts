import { NgModule } from '@angular/core';
import { ICON_REGISTRY_SERVICE_PROVIDER } from './icon-registry.service';
import { IconComponent } from './icon.component';

@NgModule({
  declarations: [IconComponent],
  exports: [IconComponent],
  providers: [ICON_REGISTRY_SERVICE_PROVIDER],
})
export class IconModule {}
