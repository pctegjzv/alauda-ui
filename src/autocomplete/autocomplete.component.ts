import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { combineLatest, Observable, of, ReplaySubject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { AutocompletePlaceholderComponent } from './autocomplete-placeholder.component';
import { AutoCompleteDirective } from './autocomplete.directive';
import { SuggestionComponent } from './suggestion/suggestion.component';

@Component({
  selector: 'aui-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class AutocompleteComponent implements AfterContentInit {
  @ContentChildren(SuggestionComponent, { descendants: true })
  suggestions: QueryList<SuggestionComponent>;
  @ContentChildren(AutocompletePlaceholderComponent)
  placeholder: QueryList<AutocompletePlaceholderComponent>;
  @ViewChild(TemplateRef) template: TemplateRef<any>;
  @ViewChild('suggestionListRef') suggestionListRef: ElementRef;

  hasVisableSuggestion$: Observable<boolean>;
  hasContent$: Observable<boolean>;

  directive$$ = new ReplaySubject<AutoCompleteDirective>(1);

  constructor(private cdr: ChangeDetectorRef) {}

  ngAfterContentInit() {
    this.hasVisableSuggestion$ = this.suggestions.changes.pipe(
      startWith(this.suggestions),
      switchMap(suggestions => {
        if (suggestions.length) {
          return combineLatest(
            suggestions.map(suggestion => suggestion.visable$),
          );
        } else {
          return of([false]);
        }
      }),
      map(values => values.some(value => value)),
      distinctUntilChanged(),
      tap(() => {
        this.cdr.detectChanges();
      }),
      publishReplay(1),
      refCount(),
    );

    this.hasContent$ = combineLatest(
      this.hasVisableSuggestion$,
      this.placeholder.changes.pipe(
        startWith(this.placeholder),
        map(list => !!list.length),
      ),
    ).pipe(
      map(
        ([hasVisableSuggestion, hasPlaceholder]) =>
          hasVisableSuggestion || hasPlaceholder,
      ),
    );
  }
}
