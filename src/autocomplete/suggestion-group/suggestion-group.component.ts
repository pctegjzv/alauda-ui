import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  forwardRef,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { SuggestionComponent } from '../suggestion/suggestion.component';

@Component({
  selector: 'aui-suggestion-group',
  templateUrl: './suggestion-group.component.html',
  styleUrls: ['./suggestion-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class SuggestionGroupComponent implements AfterContentInit {
  @ContentChildren(forwardRef(() => SuggestionComponent))
  suggestions: QueryList<SuggestionComponent>;

  hasVisableSuggestion$: Observable<boolean>;

  ngAfterContentInit() {
    this.hasVisableSuggestion$ = this.suggestions.changes.pipe(
      startWith(this.suggestions),
      switchMap(options => {
        if (options.length) {
          return combineLatest(options.map(node => node.visable$));
        } else {
          return of([false]);
        }
      }),
      map(values => values.some(value => value)),
      publishReplay(1),
      refCount(),
    );
  }
}
