import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import { Bem, buildBem } from '../../utils/bem';
import { coerceAttrBoolean } from '../../utils/coercion';
import { AutocompleteComponent } from '../autocomplete.component';

@Component({
  selector: 'aui-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class SuggestionComponent {
  bem: Bem = buildBem('aui-suggestion');

  private _disabled = false;
  private _value: string;
  private value$$ = new BehaviorSubject<string>(this.value);

  @Input()
  get value() {
    return this._value;
  }
  set value(val) {
    this._value = val;
    this.value$$.next(val);
  }
  @Input()
  get disabled() {
    return this._disabled;
  }
  set disabled(val: any) {
    this._disabled = coerceAttrBoolean(val);
  }

  @ViewChild('elRef') elRef: ElementRef;

  selected = false;
  visable = true;
  focused = false;

  selected$: Observable<boolean> = combineLatest(
    this.autocomplete.directive$$.pipe(
      switchMap(directive => directive.inputValue$),
    ),
    this.value$$,
  ).pipe(
    map(([inputValue, selfValue]) => inputValue === selfValue),
    tap(selected => {
      this.selected = selected;
    }),
    publishReplay(1),
    refCount(),
  );

  visable$: Observable<boolean> = combineLatest(
    this.autocomplete.directive$$.pipe(
      switchMap(directive => directive.filterFn$),
    ),
    this.autocomplete.directive$$.pipe(
      switchMap(directive => directive.inputValue$),
    ),
    this.value$$,
  ).pipe(
    map(([filterFn, filterString, suggestion]) =>
      filterFn(filterString, suggestion),
    ),
    tap(visable => {
      this.visable = visable;
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private cdr: ChangeDetectorRef,
    @Inject(forwardRef(() => AutocompleteComponent))
    private autocomplete: AutocompleteComponent,
  ) {}

  onClick() {
    if (this.disabled) {
      return;
    }
    this.autocomplete.directive$$.pipe(take(1)).subscribe(directive => {
      directive.onSuggestionClick(this.value);
    });
  }

  focus() {
    this.focused = true;
    this.cdr.markForCheck();
  }

  blur() {
    this.focused = false;
    this.cdr.markForCheck();
  }
}
