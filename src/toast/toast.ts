import { AlertOptions, ToastOptions, ToastType } from './toast.types';

let uniqueId = 0;

export class Toast<T> {
  id: string;
  instance: T;
  config: ToastOptions | AlertOptions;
  type: ToastType;

  constructor(toastType: ToastType, toastOptions: ToastOptions) {
    this.id = toastOptions.id || `aui-toast__${uniqueId++}`;
    this.config = toastOptions;
    this.config.type = toastType;
  }
}
