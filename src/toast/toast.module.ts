import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../icon/public-api';
import { AlertContainerComponent } from './toast-container/alert-container.component';
import { AlertsWrapperComponent } from './toast-container/alerts-wrapper.component';
import { MessageContainerComponent } from './toast-container/message-container.component';
import { MessagesWrapperComponent } from './toast-container/messages-wrapper.component';
import { ToastService } from './toast.service';
/**
 * @deprecated
 */

@NgModule({
  imports: [CommonModule, OverlayModule, PortalModule, IconModule],
  declarations: [
    AlertContainerComponent,
    MessageContainerComponent,
    AlertsWrapperComponent,
    MessagesWrapperComponent,
  ],
  entryComponents: [
    AlertContainerComponent,
    MessageContainerComponent,
    AlertsWrapperComponent,
    MessagesWrapperComponent,
  ],
  providers: [ToastService],
})
export class ToastModule {}
