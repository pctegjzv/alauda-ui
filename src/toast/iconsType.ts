import { ToastType } from './toast.types';

export const iconsType: { [key: string]: string } = {
  [ToastType.Error]: 'exclamation_triangle_s',
  [ToastType.Success]: 'check_circle_s',
  [ToastType.Warning]: 'exclamation_circle_s',
  [ToastType.Info]: 'info_circle_s',
  [ToastType.Explanation]: 'info_circle_s',
};
