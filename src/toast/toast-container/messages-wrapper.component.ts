import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'aui-messages-wrapper',
  styleUrls: ['./messages-wrapper.component.scss'],
  template: '',
  encapsulation: ViewEncapsulation.None,
  // tslint:disable-next-line:validate-decorators
  changeDetection: ChangeDetectionStrategy.Default,
  preserveWhitespaces: false,
  host: {
    '[class.aui-messages-wrapper]': 'true',
  },
})
export class MessagesWrapperComponent {
  constructor(public elementRef: ElementRef) {}
}
