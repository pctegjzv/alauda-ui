import { PortalHostDirective, TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EmbeddedViewRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ComponentFactory } from '@angular/core/src/linker/component_factory';
import { iconsType } from '../iconsType';
import { toastAnimations } from '../toast-animations';
import { ToastType } from '../toast.types';

@Component({
  selector: 'aui-alert-container',
  templateUrl: './alert-container.component.html',
  animations: [toastAnimations.inOut],
  styleUrls: ['./alert-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
  // tslint:disable-next-line:validate-decorators
  changeDetection: ChangeDetectionStrategy.Default,
  preserveWhitespaces: false,
})
export class AlertContainerComponent implements AfterViewInit, OnInit {
  animate = 'flyLeft';
  intervalId: any;
  isHover = false;
  remains: number;
  childComponentInstance: any;
  @Output() beforeClosed: EventEmitter<void> = new EventEmitter();
  @Output() afterClosed: EventEmitter<void> = new EventEmitter();

  @Input() type: ToastType;
  @Input() content: string;
  @Input() title: string;
  @Input() duration = 6;

  @ViewChild(PortalHostDirective) private _portalHost: PortalHostDirective;
  @ViewChild('modal_component', { read: ViewContainerRef })
  private _modalEl: ViewContainerRef;

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.duration <= 0) {
      return;
    }
    this.isHover = true;
    clearInterval(this.intervalId);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.duration <= 0) {
      return;
    }
    this.isHover = false;
    this.remains = this.duration;
    this.countDown();
  }

  countDown() {
    this.intervalId = setInterval(() => {
      this.remains -= 1;
      if (this.remains === 0) {
        this.clear();
        clearInterval(this.intervalId);
      }
    }, 1000);
  }

  get iconType(): string {
    return iconsType[this.type];
  }

  constructor(private viewContainerRef: ViewContainerRef) {}

  ngOnInit(): void {
    this.remains = this.duration;
  }

  ngAfterViewInit() {
    if (this.duration > 0) {
      this.countDown();
    }
  }

  /**
   * Attach a TemplatePortal as content to this modal container.
   * @param portal Portal to be attached as the modal content.
   */
  attachTemplatePortal<C>(portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    return this._portalHost.attachTemplatePortal(portal);
  }

  attchComponentRef(componentRef: ComponentFactory<void>) {
    this.childComponentInstance = this._modalEl.createComponent(
      componentRef,
      null,
      this.viewContainerRef.injector,
    ).instance;
    return this.childComponentInstance;
  }

  clear() {
    clearInterval(this.intervalId);
    this.animate = 'flyUp';
    this.beforeClosed.emit();
  }

  hover(isHover: boolean) {
    this.isHover = isHover;
  }

  onAnimationEnd(e: any) {
    try {
      if (this.viewContainerRef.element && e.toState === 'flyUp') {
        this.viewContainerRef.element.nativeElement.parentElement.removeChild(
          this.viewContainerRef.element.nativeElement,
        );
        this.afterClosed.emit();
      }
    } catch (e) {
      throw Error('No outer layer can be found!');
    }
  }
}
