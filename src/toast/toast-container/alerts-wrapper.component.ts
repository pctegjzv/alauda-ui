import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'aui-alerts-wrapper',
  styleUrls: ['./alerts-wrapper.component.scss'],
  template: '',
  encapsulation: ViewEncapsulation.None,
  // tslint:disable-next-line:validate-decorators
  changeDetection: ChangeDetectionStrategy.Default,
  host: {
    '[class.aui-alerts-wrapper]': 'true',
  },
  preserveWhitespaces: false,
})
export class AlertsWrapperComponent {
  constructor(public elementRef: ElementRef) {}
}
