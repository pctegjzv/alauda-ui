import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { Bem, buildBem } from '../../utils/bem';
import { iconsType } from '../iconsType';
import { toastAnimations } from '../toast-animations';
import { ToastType } from '../toast.types';

@Component({
  selector: 'aui-message-container',
  templateUrl: './message-container.component.html',
  animations: [toastAnimations.inOut],
  styleUrls: ['./message-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
  // tslint:disable-next-line:validate-decorators
  changeDetection: ChangeDetectionStrategy.Default,
  preserveWhitespaces: false,
})
export class MessageContainerComponent implements OnInit, AfterViewInit {
  animate = 'slideDown';
  timerId: any;
  remains: number;
  bem: Bem = buildBem('aui-message');

  @Input() type: ToastType = ToastType.Success;
  @Input() content: string;
  @Input() duration = 3;
  @Output() beforeClosed: EventEmitter<void> = new EventEmitter();
  @Output() afterClosed: EventEmitter<void> = new EventEmitter();

  get iconType(): string {
    return iconsType[this.type];
  }

  constructor(private viewContainerRef: ViewContainerRef) {}

  ngOnInit(): void {
    this.remains = this.duration;
  }

  ngAfterViewInit(): void {
    if (this.duration > 0) {
      this.timerId = setTimeout(() => {
        this.clear();
      }, this.duration * 1000);
    }
  }

  clear() {
    clearTimeout(this.timerId);
    this.animate = 'slideUp';
    this.beforeClosed.emit();
  }

  onAnimationEnd(e: any) {
    try {
      if (this.viewContainerRef.element && e.toState === 'slideUp') {
        this.viewContainerRef.element.nativeElement.parentElement.removeChild(
          this.viewContainerRef.element.nativeElement,
        );
        this.afterClosed.emit();
      }
    } catch (e) {
      throw Error('No outer layer can be found!');
    }
  }
}
