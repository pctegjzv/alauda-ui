import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, NgModule, TemplateRef, ViewChild } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule, ToastService } from './public-api';

describe('ToastService', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ocEl: HTMLElement;
  let oc: OverlayContainer;
  let toastService: ToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestMoudle],
    });

    fixture = TestBed.createComponent(TestComponent);

    inject(
      [OverlayContainer, ToastService],
      (overlayContainer: OverlayContainer, service: ToastService) => {
        (oc = overlayContainer),
          (ocEl = overlayContainer.getContainerElement());
        toastService = service;
      },
    )();
  });

  it('should show a success message with  string', () => {
    toastService.messageSuccess('message success');
    fixture.detectChanges();
    expect(ocEl.querySelector('.aui-message')).not.toBeNull();
    expect(ocEl.querySelector('.aui-message').classList).toContain(
      'aui-message--success',
    );
    expect(ocEl.querySelector('.aui-message__content').innerHTML).toContain(
      'message success',
    );
  });

  it('should show a success message with object', () => {
    toastService.messageSuccess({
      content: 'message success object',
    });
    fixture.detectChanges();
    expect(ocEl.querySelector('.aui-message')).not.toBeNull();
    expect(ocEl.querySelector('.aui-message').classList).toContain(
      'aui-message--success',
    );
    expect(ocEl.querySelector('.aui-message__content').innerHTML).toContain(
      'message success object',
    );
  });

  it('should show a success alert with template', () => {
    toastService.alertError({
      contentRef: fixture.componentInstance.templateRef,
      duration: 10,
    });
    fixture.detectChanges();

    expect(ocEl.querySelector('.aui-alert button')).not.toBeNull();
    expect(ocEl.querySelector('.aui-alert button').classList).toContain(
      'temp-btn',
    );
    expect(ocEl.querySelector('.aui-alert__close').innerHTML).toContain('10s');
  });

  it('should show a success alert with component', () => {
    toastService.alertWarning({
      contentRef: AlertContentComponent,
    });
    fixture.detectChanges();

    expect(ocEl.querySelector('.aui-alert .alert-demo-content')).not.toBeNull();
    expect(
      ocEl.querySelector('.aui-alert .alert-demo-content').innerHTML,
    ).toContain('demo content');
  });
});

@Component({
  template: `
  <div class="alert-demo-content">demo content</div>
    `,
})
class AlertContentComponent {}

@Component({
  template: `
  <div>
    <ng-template #template>
      <button class="temp-btn">点击</button>
    </ng-template>
  </div>
  `,
})
export class TestComponent {
  @ViewChild('template') templateRef: TemplateRef<any>;
}

@NgModule({
  imports: [ToastModule, NoopAnimationsModule],
  declarations: [TestComponent, AlertContentComponent],
  entryComponents: [AlertContentComponent],
})
class TestMoudle {}
