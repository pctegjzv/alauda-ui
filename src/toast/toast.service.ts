/**
 * Message: lightweight toast
 * Alert: Heavyweight toast
 * Detailed comparison documents: http://confluence.alaudatech.com/pages/viewpage.action?pageId=23383163
 */
import { Overlay } from '@angular/cdk/overlay';
import {
  ComponentPortal,
  ComponentType,
  DomPortalHost,
  TemplatePortal,
} from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  Injector,
  TemplateRef,
  Type,
} from '@angular/core';
import { Toast } from './toast';
import { AlertContainerComponent } from './toast-container/alert-container.component';
import { AlertsWrapperComponent } from './toast-container/alerts-wrapper.component';
import { MessageContainerComponent } from './toast-container/message-container.component';
import { MessagesWrapperComponent } from './toast-container/messages-wrapper.component';
import { AlertOptions, ToastOptions, ToastType } from './toast.types';

export interface ToastInstanceModel<T> {
  uniqueCert: {
    config: ToastOptions;
    type: 'message' | 'alert';
  };
  instance: T;
}

@Injectable()
export class ToastService {
  messageOverlayRef: ComponentRef<MessagesWrapperComponent>;
  alertsOverlayRef: ComponentRef<AlertsWrapperComponent>;
  toasts: Array<
    ToastInstanceModel<MessageContainerComponent | AlertContainerComponent>
  > = [];

  constructor(
    private applicationRef: ApplicationRef,
    private injector: Injector,
    private cfr: ComponentFactoryResolver,
    private overlay: Overlay,
  ) {}

  private initComponentRef<T>(
    host: Element,
    container: ComponentType<T>,
    toast: Toast<T>,
  ): ComponentRef<T> {
    const modalContainerPortalHost = new DomPortalHost(
      host,
      this.cfr,
      this.applicationRef,
      this.injector,
    );
    const modalContainerRef = modalContainerPortalHost.attachComponentPortal(
      new ComponentPortal(container),
    );
    Object.assign(modalContainerRef.instance, toast.config);
    return modalContainerRef;
  }

  createMessage(
    toast: Toast<MessageContainerComponent>,
  ): Toast<MessageContainerComponent> {
    if (!this.messageOverlayRef) {
      const overlayRef = this.overlay.create();
      this.messageOverlayRef = overlayRef.attach(
        new ComponentPortal(MessagesWrapperComponent),
      );
    }
    const componentRef = this.initComponentRef(
      this.messageOverlayRef.instance.elementRef.nativeElement,
      MessageContainerComponent,
      toast,
    );
    componentRef.instance.afterClosed.subscribe(() => {
      componentRef.destroy();
      this.clearById(toast.id);
    });

    toast.instance = componentRef.instance;
    this.toasts.push({
      instance: componentRef.instance,
      uniqueCert: {
        config: toast.config,
        type: 'message',
      },
    });
    return toast;
  }

  clearById(id: string) {
    this.toasts = this.toasts.filter(
      ({ uniqueCert }) => uniqueCert.config.id !== id,
    );
  }

  createAlert(
    toast: Toast<AlertContainerComponent>,
  ): Toast<AlertContainerComponent> {
    const options: AlertOptions = toast.config;
    if (!this.alertsOverlayRef) {
      this.alertsOverlayRef = this.overlay
        .create()
        .attach(new ComponentPortal(AlertsWrapperComponent));
    }
    const componentRef = this.initComponentRef(
      this.alertsOverlayRef.instance.elementRef.nativeElement,
      AlertContainerComponent,
      toast,
    );

    if (options.contentRef && options.contentRef instanceof TemplateRef) {
      const portal = new TemplatePortal(options.contentRef, null, {
        $implicit: options,
        ...(options.context || {}),
      });
      componentRef.instance.attachTemplatePortal(portal);
    } else if (options.contentRef && options.contentRef instanceof Type) {
      componentRef.instance.attchComponentRef(
        this.cfr.resolveComponentFactory(options.contentRef),
      );
    }

    componentRef.instance.afterClosed.subscribe(() => {
      componentRef.destroy();
      this.clearById(toast.id);
    });
    toast.instance = componentRef.instance;
    this.toasts.push({
      instance: componentRef.instance,
      uniqueCert: {
        config: toast.config,
        type: 'alert',
      },
    });
    return toast;
  }

  private parseOptions(options: string | ToastOptions | AlertOptions) {
    return typeof options === 'string' ? { content: options } : options;
  }

  // message
  messageSuccess(
    toastOptions: ToastOptions | string,
  ): Toast<MessageContainerComponent> {
    const toast = new Toast<MessageContainerComponent>(
      ToastType.Success,
      this.parseOptions(toastOptions),
    );
    return this.createMessage(toast);
  }
  messageError(
    toastOptions: ToastOptions | string,
  ): Toast<MessageContainerComponent> {
    const toast = new Toast<MessageContainerComponent>(
      ToastType.Error,
      this.parseOptions(toastOptions),
    );
    return this.createMessage(toast);
  }
  messageInfo(
    toastOptions: ToastOptions | string,
  ): Toast<MessageContainerComponent> {
    const toast = new Toast<MessageContainerComponent>(
      ToastType.Info,
      this.parseOptions(toastOptions),
    );
    return this.createMessage(toast);
  }
  messageExplanation(
    toastOptions: ToastOptions | string,
  ): Toast<MessageContainerComponent> {
    const toast = new Toast<MessageContainerComponent>(
      ToastType.Explanation,
      this.parseOptions(toastOptions),
    );
    return this.createMessage(toast);
  }
  messageWarning(
    toastOptions: ToastOptions | string,
  ): Toast<MessageContainerComponent> {
    const toast = new Toast<MessageContainerComponent>(
      ToastType.Warning,
      this.parseOptions(toastOptions),
    );
    return this.createMessage(toast);
  }

  // alert
  alertSuccess(
    alertOptions: AlertOptions | string,
  ): Toast<AlertContainerComponent> {
    const toast = new Toast<AlertContainerComponent>(
      ToastType.Success,
      this.parseOptions(alertOptions),
    );
    return this.createAlert(toast);
  }
  alertError(
    alertOptions: AlertOptions | string,
  ): Toast<AlertContainerComponent> {
    const toast = new Toast<AlertContainerComponent>(
      ToastType.Error,
      this.parseOptions(alertOptions),
    );
    return this.createAlert(toast);
  }
  alertInfo(
    alertOptions: AlertOptions | string,
  ): Toast<AlertContainerComponent> {
    const toast = new Toast<AlertContainerComponent>(
      ToastType.Info,
      this.parseOptions(alertOptions),
    );
    return this.createAlert(toast);
  }
  alertWarning(
    alertOptions: AlertOptions | string,
  ): Toast<AlertContainerComponent> {
    const toast = new Toast<AlertContainerComponent>(
      ToastType.Warning,
      this.parseOptions(alertOptions),
    );
    return this.createAlert(toast);
  }
  alertExplanation(
    alertOptions: AlertOptions | string,
  ): Toast<AlertContainerComponent> {
    const toast = new Toast<AlertContainerComponent>(
      ToastType.Explanation,
      this.parseOptions(alertOptions),
    );
    return this.createAlert(toast);
  }

  closeAll() {
    this.toasts.forEach(({ instance }) => {
      if (instance.clear) {
        instance.clear();
      }
    });
  }
}
