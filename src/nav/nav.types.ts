/**
 * The state model for Nav. Acting as a collection of latest Nav states.
 */
export class NavState {
  items: NavItemState[];
}

/**
 * A single piece of nav item.
 */
export class NavItemState {
  /**
   * Partial path to be bound to this item.
   */
  path: string;

  /**
   * Is this item active?
   */
  active?: boolean;

  /**
   * Whether this item has a menu or not.
   */
  menu?: NavMenuState;
}

export class NavMenuState {
  /**
   * Child items of this menu.
   */
  items: NavItemState[];

  /**
   * Indicates if this menu is expanded or not.
   */
  expanded?: boolean;
}
