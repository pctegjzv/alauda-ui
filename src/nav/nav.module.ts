import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../icon/public-api';
import { NavItemExtDirective } from './nav-item/nav-item-ext.directive';
import { NavItemComponent } from './nav-item/nav-item.component';
import { NavComponent } from './nav.component';

const EXPORTABLES = [NavComponent, NavItemComponent, NavItemExtDirective];

@NgModule({
  imports: [CommonModule, IconModule],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
})
export class NavModule {}
