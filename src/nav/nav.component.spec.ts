import { Component, OnInit } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NavModule } from './nav.module';

describe('NavComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NavModule],
      declarations: [TestSimpleComponent, TestComponent],
    }).compileComponents();
  });

  describe('Check propeties of NavItem when conditions change', () => {
    let fixture: ComponentFixture<TestSimpleComponent>;
    beforeEach(() => {
      fixture = TestBed.createComponent(TestSimpleComponent);
    });

    it('one not expanded', () => {
      fixture.componentInstance.oneActive = true;
      fixture.detectChanges();
      const itemOne = fixture.debugElement.query(
        By.css('#item-one .aui-nav-item'),
      );
      const itemOneExt = fixture.debugElement.query(
        By.css('#item-one .aui-nav-item__ext'),
      );
      expect(itemOne.classes['aui-nav-item--active']).toBeTruthy();
      expect(itemOneExt.classes['aui-nav-item__ext--expanded']).toBeFalsy();
    });

    it('one expanded', () => {
      fixture.componentInstance.oneExpaned = true;
      fixture.detectChanges();

      const itemOne = fixture.debugElement.query(
        By.css('#item-one .aui-nav-item'),
      );
      const itemOneExt = fixture.debugElement.query(
        By.css('#item-one .aui-nav-item__ext'),
      );
      const itemTwo = fixture.debugElement.query(
        By.css('#item-two .aui-nav-item'),
      );
      expect(itemOne.classes['aui-nav-item--active']).not.toBeTruthy();
      expect(itemTwo.classes['aui-nav-item--active']).not.toBeTruthy();
      expect(itemOneExt.classes['aui-nav-item__ext--expanded']).toBeTruthy();

      fixture.componentInstance.twoActive = true;
      fixture.detectChanges();
      expect(itemTwo.classes['aui-nav-item--active']).toBeTruthy();
    });

    it('label one clicked', () => {
      fixture.componentInstance.oneExpaned = true;
      fixture.detectChanges();

      const labelOne = fixture.debugElement.query(By.css('#label-one'));
      const labelTwo = fixture.debugElement.query(By.css('#label-two'));
      labelOne.nativeElement.click();
      labelTwo.nativeElement.click();
      expect(fixture.componentInstance.clickSequence).toEqual(['one', 'two']);
    });
  });

  it('full AuiNav with Template snapshot test', () => {
    const fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });
});

@Component({
  template: `
<aui-nav>
  <aui-nav-item id="item-one" [expanded]="oneExpaned" [active]="oneActive" (click)="onClick('one')">
    <span id="label-one">ITEM ONE</span>
    <ng-container *auiNavItemExt>
      <aui-nav-item id="item-two" [expanded]="twoExpaned" [active]="twoActive" (click)="onClick('two')">
      <span id="label-two">ITEM TWO</span>
      </aui-nav-item>
    </ng-container>
  </aui-nav-item>
</aui-nav>
  `,
})
export class TestSimpleComponent {
  oneExpaned = false;
  oneActive = false;
  twoExpaned = false;
  twoActive = false;

  clickSequence = [];

  onClick(item: string) {
    this.clickSequence.push(item);
  }
}

@Component({
  template: `
  <aui-nav>
  <aui-nav-item [expanded]="expandedMap['first']"
                [active]="isPathActive('first')"
                (click)="handleClick('first')">
    <!-- 不放到nav-menu的部分即为title。 -->
    一级导航1

    <!-- menu本身只提供导航项目的展开和关闭。 -->
    <ng-container *auiNavItemExt>
      <aui-nav-item [active]="isPathActive('first/a')"
                    (click)="handleClick('first/a')">
        二级导航a
      </aui-nav-item>
      <aui-nav-item [active]="isPathActive('first/b')"
                    [expanded]="expandedMap['first/b']"
                    (click)="handleClick('first/b')">
        二级导航b
        <!-- 理论上不会出现三级导航，但在实现上不做限制 -->
        <!-- <ng-container *auiNavItemExt>
          <aui-nav-item [active]="isPathActive('first/b/foo')"
                        (click)="handleClick('first/b/foo')">
            三级导航
          </aui-nav-item>
        </ng-container> -->
      </aui-nav-item>
    </ng-container>
  </aui-nav-item>

  <aui-nav-item [expanded]="expandedMap['second']"
                [active]="isPathActive('second')"
                (click)="handleClick('second')">
                一级导航2
  </aui-nav-item>
</aui-nav>
  `,
})
export class TestComponent implements OnInit {
  expandedMap = {
    first: true,
    'first/b': false,
  };

  activePath = 'first/b';

  constructor() {}
  ngOnInit() {}

  handleClick(path: string) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }
}
