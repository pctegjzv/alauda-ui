import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  Optional,
  Output,
  SkipSelf,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { NavItemExtDirective } from './nav-item-ext.directive';

@Component({
  selector: 'aui-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [NavItemComponent],
})
export class NavItemComponent {
  /**
   * To show active indicator
   */
  @Input() active: boolean;

  /**
   * Whether this nav item is expanded or not.
   */
  @Input() expanded = true;

  /**
   * Captured click event so only the label element will emit click event.
   */
  @Output() click = new EventEmitter<MouseEvent>();

  /**
   * The extension content. i.e., the menu items.
   */
  @ContentChild(NavItemExtDirective, { read: TemplateRef })
  _extTemplate: NavItemExtDirective;

  /**
   * Decorator class to indicates this is a root item (has no nav item parent).
   */
  isRoot: boolean;

  constructor(
    @Optional()
    @SkipSelf()
    private parentItem: NavItemComponent,
  ) {
    this.isRoot = !this.parentItem;
  }

  /**
   * Captured click event so only the label element will emit click event.
   */
  labelClicked(event: MouseEvent) {
    this.click.emit(event);
    event.stopPropagation();
  }
}
