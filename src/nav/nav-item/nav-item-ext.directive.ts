import { Directive, TemplateRef } from '@angular/core';

/**
 * Represents the extended area of a nav item.
 */
@Directive({
  selector: '[auiNavItemExt]',
})
export class NavItemExtDirective {
  constructor(public template: TemplateRef<any>) {}
}
