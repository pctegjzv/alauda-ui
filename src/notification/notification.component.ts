import { PortalHostDirective, TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EmbeddedViewRef,
  HostListener,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ComponentFactory } from '@angular/core/src/linker/component_factory';
import { iconsType } from '../message/iconsType';
import { MessageAnimations } from '../message/message-animations';
import { MessageComponent } from '../message/message.component';

@Component({
  selector: 'aui-notification',
  templateUrl: './notification.component.html',
  animations: [MessageAnimations.inOut],
  styleUrls: ['./notification.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class NotificationComponent extends MessageComponent
  implements AfterViewInit {
  animate = 'flyLeft';
  isHover = false;
  childComponentInstance: any;

  title: string;

  @ViewChild(PortalHostDirective) private _portalHost: PortalHostDirective;
  @ViewChild('modal_component', { read: ViewContainerRef })
  private _modalEl: ViewContainerRef;

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.remains <= 0) {
      return;
    }
    this.isHover = true;
    clearInterval(this.timerId);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.remains <= 0) {
      return;
    }
    this.isHover = false;
    this.remains = Math.ceil(this.duration / 1000);
    this.countDown();
  }

  countDown() {
    this.timerId = setInterval(() => {
      this.remains -= 1;
      if (this.remains === 0) {
        this.clear();
        clearInterval(this.timerId);
      }
      this.cdr.markForCheck();
    }, 1000);
  }

  get iconType(): string {
    return iconsType[this.type];
  }

  constructor(containerRef: ViewContainerRef, cdr: ChangeDetectorRef) {
    super(containerRef, cdr);
  }

  ngAfterViewInit() {
    if (this.remains > 0) {
      this.countDown();
    }
  }

  /**
   * Attach a TemplatePortal as content to this modal container.
   * @param portal Portal to be attached as the modal content.
   */
  attachTemplatePortal<C>(portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    return this._portalHost.attachTemplatePortal(portal);
  }

  attchComponentRef(componentRef: ComponentFactory<void>) {
    this.childComponentInstance = this._modalEl.createComponent(
      componentRef,
      null,
      this.viewContainerRef.injector,
    ).instance;
    return this.childComponentInstance;
  }

  clear() {
    clearInterval(this.timerId);
    this.animate = 'flyUp';
    this.cdr.markForCheck();
    this.beforeClosed.next();
    this.beforeClosed.complete();
  }

  hover(isHover: boolean) {
    this.isHover = isHover;
  }

  onAnimationEnd(e: any) {
    try {
      if (this.viewContainerRef.element && e.toState === 'flyUp') {
        this.viewContainerRef.element.nativeElement.parentElement.removeChild(
          this.viewContainerRef.element.nativeElement,
        );
        this.afterClosed.next();
        this.afterClosed.complete();
      }
    } catch (e) {
      throw Error('No outer layer can be found!');
    }
  }
}
