import { Overlay } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  Injector,
  TemplateRef,
  Type,
} from '@angular/core';
import { MessageBaseService } from '../message/message-base.service';
import { MessageType } from '../message/message.config';
import { NotificationWrapperComponent } from './notification-wrapper.component';
import { NotificationComponent } from './notification.component';
import { NotificationConfig } from './notification.config';

@Injectable({
  providedIn: 'root',
})
export class NotificationService extends MessageBaseService<
  NotificationWrapperComponent,
  NotificationComponent,
  NotificationConfig
> {
  constructor(
    overlay: Overlay,
    injector: Injector,
    applicationRef: ApplicationRef,
    cfr: ComponentFactoryResolver,
  ) {
    super(
      overlay,
      NotificationWrapperComponent,
      NotificationComponent,
      injector,
      applicationRef,
      cfr,
      'aui-notification-',
    );
  }

  protected createMessage(
    notificationConfig: NotificationConfig,
  ): ComponentRef<NotificationComponent> {
    const componentRef = this.initComponentRef(notificationConfig);

    componentRef.instance.afterClosed.subscribe(() => {
      componentRef.destroy();
      this.remove(componentRef.instance.uniqueId);
    });

    if (
      notificationConfig.contentRef &&
      notificationConfig.contentRef instanceof TemplateRef
    ) {
      const portal = new TemplatePortal(notificationConfig.contentRef, null, {
        $implicit: notificationConfig,
        ...(notificationConfig.context || {}),
      });
      componentRef.instance.attachTemplatePortal(portal);
    } else if (
      notificationConfig.contentRef &&
      notificationConfig.contentRef instanceof Type
    ) {
      componentRef.instance.attchComponentRef(
        this.cfr.resolveComponentFactory(notificationConfig.contentRef),
      );
    }

    this.exclude(notificationConfig);
    this.messagesFillDatas.push(componentRef);
    return componentRef;
  }

  // message
  success(messageOptions: NotificationConfig | string) {
    const messageConfig = Object.assign(
      { type: MessageType.Success },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  error(messageOptions: NotificationConfig | string) {
    const messageConfig = Object.assign(
      { type: MessageType.Error },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  info(messageOptions: NotificationConfig | string) {
    const messageConfig = Object.assign(
      { type: MessageType.Info },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  warning(messageOptions: NotificationConfig | string) {
    const messageConfig = Object.assign(
      { type: MessageType.Warning },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }
}
