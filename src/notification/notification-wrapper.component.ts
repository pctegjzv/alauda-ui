import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Optional,
  ViewEncapsulation,
} from '@angular/core';
import { MessageWrapperComponent } from '../message/message-wrapper.component';
import {
  NOTIFICATION_CONFIG,
  NOTIFICATION_DEFAULT_CONFIG,
  NotificationConfig,
} from './notification.config';

@Component({
  selector: 'aui-notification-wrapper',
  styleUrls: ['./notification-wrapper.component.scss'],
  template: '',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class NotificationWrapperComponent extends MessageWrapperComponent {
  config: NotificationConfig;
  constructor(
    public elementRef: ElementRef,
    @Optional()
    @Inject(NOTIFICATION_DEFAULT_CONFIG)
    defaultConfig: NotificationConfig,
    @Optional()
    @Inject(NOTIFICATION_CONFIG)
    config: NotificationConfig,
  ) {
    super(elementRef, defaultConfig, config);
  }
}
