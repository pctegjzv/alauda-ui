import { InjectionToken, TemplateRef, Type } from '@angular/core';
import { MessageConfig } from '../message/public-api';

/**
 * Options for Notification
 * Detailed comparison documents: http://confluence.alaudatech.com/pages/viewpage.action?pageId=23383190
 */
export interface NotificationConfig extends MessageConfig {
  /**
   * for notification ,can use angular template or component
   */
  contentRef?: TemplateRef<any> | Type<void>;

  /**
   * Template context.
   */
  context?: any;

  /**
   * for notification,can use title
   */
  title?: string;

  /**
   * notification content
   */
  content?: string;
}

export const NOTIFICATION_DEFAULT_CONFIG = new InjectionToken<
  NotificationConfig
>('NOTIFICATION_DEFAULT_CONFIG');

export const NOTIFICATION_CONFIG = new InjectionToken<NotificationConfig>(
  'NOTIFICATION_CONFIG',
);

export const NOTIFICATION_DEFAULT_CONFIG_PROVIDER = {
  provide: NOTIFICATION_DEFAULT_CONFIG,
  useValue: {
    duration: 6000,
    maxStack: 8,
  },
};
