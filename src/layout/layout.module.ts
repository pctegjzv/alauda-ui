import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import {
  NavDrawerContentDirective,
  NavDrawerHeaderDirective,
  PageContentDirective,
  PageHeaderDirective,
  ToolbarContentDirective,
} from './helper-directives';
import { LayoutComponent } from './layout.component';
import { LayoutNavDrawerComponent } from './nav-drawer/nav-drawer.component';

const EXPORTABLES = [
  LayoutComponent,
  NavDrawerContentDirective,
  NavDrawerHeaderDirective,
  PageContentDirective,
  PageHeaderDirective,
  ToolbarContentDirective,
];

@NgModule({
  declarations: [...EXPORTABLES, LayoutNavDrawerComponent],
  imports: [CommonModule, ScrollDispatchModule],
  exports: [...EXPORTABLES],
  providers: [],
})
export class LayoutModule {}
