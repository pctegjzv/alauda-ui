import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[auiNavDrawerHeader]',
})
export class NavDrawerHeaderDirective {
  constructor(public template: TemplateRef<any>) {}
}

@Directive({
  selector: '[auiNavDrawerContent]',
})
export class NavDrawerContentDirective {
  constructor(public template: TemplateRef<any>) {}
}

@Directive({
  selector: '[auiToolbarContent]',
})
export class ToolbarContentDirective {
  constructor(public template: TemplateRef<any>) {}
}

@Directive({
  selector: '[auiPageHeader]',
})
export class PageHeaderDirective {
  constructor(public template: TemplateRef<any>) {}
}

@Directive({
  selector: '[auiPageContent]',
})
export class PageContentDirective {
  constructor(public template: TemplateRef<any>) {}
}
