import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { LayoutModule } from './layout.module';

describe('NavComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LayoutModule],
      declarations: [TestComponent],
    }).compileComponents();
  });

  it('should render correct template', () => {
    const fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });
});

@Component({
  template: `
<aui-layout>
  <div *auiNavDrawerHeader>
    Nav Drawer Header
  </div>
  <div *auiNavDrawerContent>
    Nav Drawer Content
  </div>

  <div *auiToolbarContent>
    Toolbar content
  </div>
  <div *auiPageHeader>
    Page Header
  </div>
  <div *auiPageContent>
    Page Content
  </div>
</aui-layout>
  `,
})
export class TestComponent {}
