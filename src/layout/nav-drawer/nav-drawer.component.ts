import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import {
  NavDrawerContentDirective,
  NavDrawerHeaderDirective,
} from '../helper-directives';

/**
 * The nav drawer component. Since we may have some extension over the UX,
 * it groups the header and the content containers.
 */
@Component({
  selector: 'aui-layout-nav-drawer',
  templateUrl: './nav-drawer.component.html',
  styleUrls: ['./nav-drawer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class LayoutNavDrawerComponent {
  @ContentChild(NavDrawerHeaderDirective, { read: TemplateRef })
  _headerTemplate: TemplateRef<any>;

  @ContentChild(NavDrawerContentDirective, { read: TemplateRef })
  _contentTemplate: TemplateRef<any>;
}
