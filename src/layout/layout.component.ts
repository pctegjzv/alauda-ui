import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Input,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import {
  NavDrawerContentDirective,
  NavDrawerHeaderDirective,
  PageContentDirective,
  PageHeaderDirective,
  ToolbarContentDirective,
} from './helper-directives';

/**
 * Layout component is a container wrapper for
 * standardized Alauda products.
 *
 * Usage is pretty simple: place layout related stuff wrapped
 * with the helper directives and see things happen.
 */
@Component({
  selector: 'aui-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class LayoutComponent {
  @ContentChild(NavDrawerHeaderDirective, { read: TemplateRef })
  _navDrawerHeader: TemplateRef<any>;

  @ContentChild(NavDrawerContentDirective, { read: TemplateRef })
  _navDrawerContent: TemplateRef<any>;

  @ContentChild(ToolbarContentDirective, { read: TemplateRef })
  _toolbarContent: TemplateRef<any>;

  @ContentChild(PageContentDirective, { read: TemplateRef })
  _pageContent: TemplateRef<any>;

  @ContentChild(PageHeaderDirective, { read: TemplateRef })
  _pageHeader: TemplateRef<any>;

  @Input() showDrawer = true;
}
