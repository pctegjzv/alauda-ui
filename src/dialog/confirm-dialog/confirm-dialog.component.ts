import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Bem, buildBem } from '../../utils/bem';
import { DialogRef } from '../dialog-ref';
import { ConfirmType } from '../dialog.types';
import { ConfirmDialogConfig } from './confirm-dialog-config';

@Component({
  selector: 'aui-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class ConfirmDialogComponent {
  bem: Bem = buildBem('aui-confirm-dialog');

  config: ConfirmDialogConfig;

  waitConfirm = false;
  waitCancel = false;

  constructor(
    private dialogRef: DialogRef<any>,
    private cdr: ChangeDetectorRef,
  ) {}

  setConfig(config: ConfirmDialogConfig) {
    this.config = { ...new ConfirmDialogConfig(), ...config };
  }

  iconMap(type: ConfirmType) {
    switch (type) {
      case ConfirmType.Success:
        return 'check_circle_s';
      case ConfirmType.Danger:
        return 'exclamation_triangle_s';
      case ConfirmType.Primary:
      case ConfirmType.Warning:
      default:
        return 'exclamation_circle_s';
    }
  }

  async confirm() {
    if (!this.config.beforeConfirm) {
      this.dialogRef.close(true);
      return;
    }
    this.waitConfirm = true;
    try {
      await new Promise(this.config.beforeConfirm);
      this.dialogRef.close(true);
    } catch (err) {
    } finally {
      this.waitConfirm = false;
      this.cdr.markForCheck();
    }
  }

  async cancel() {
    if (!this.config.beforeCancel) {
      this.dialogRef.close(false);
    }
    this.waitCancel = true;
    try {
      await new Promise(this.config.beforeCancel);
      this.dialogRef.close(false);
    } catch (err) {
    } finally {
      this.waitCancel = false;
      this.cdr.markForCheck();
    }
  }
}
