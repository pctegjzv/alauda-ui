import { ConfirmType } from '../dialog.types';

export class ConfirmDialogConfig {
  title: string;
  content?: string;
  cancelButton? = true;
  confirmType?: ConfirmType = ConfirmType.Primary;
  confirmText? = 'OK';
  cancelText? = 'Cancel';

  beforeConfirm?: (reslove: () => void, reject: () => void) => void;
  beforeCancel?: (reslove: () => void, reject: () => void) => void;
}
