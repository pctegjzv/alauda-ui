import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Bem, buildBem } from '../../utils/bem';

@Component({
  selector: 'aui-dialog-content',
  templateUrl: './dialog-content.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class DialogContentComponent {
  bem: Bem = buildBem('aui-dialog');
}
