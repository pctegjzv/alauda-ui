import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, NgModule, TemplateRef, ViewChild } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { sleep } from '../utils/async';
import { DialogModule, DialogService, DialogSize } from './public-api';

describe('DialogService', () => {
  let fixture: ComponentFixture<TestComponent>;
  let oc: OverlayContainer;
  let ocEl: HTMLElement;
  let dialogService: DialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestMoudle],
    });

    fixture = TestBed.createComponent(TestComponent);

    inject(
      [OverlayContainer, DialogService],
      (overlayContainer: OverlayContainer, service: DialogService) => {
        (oc = overlayContainer),
          (ocEl = overlayContainer.getContainerElement());
        dialogService = service;
      },
    )();
  });

  it('should open dialog with component portal', () => {
    dialogService.open(DialogContentComponent, {
      size: DialogSize.Large,
    });

    fixture.detectChanges();

    expect(ocEl.querySelector('.aui-dialog')).not.toBeNull();
    expect(ocEl.querySelector('.aui-dialog').classList).toContain(
      'aui-dialog--large',
    );
    expect(
      ocEl.querySelector('.aui-dialog>test-dialog-content'),
    ).not.toBeNull();
  });

  it('should be closed by click cancel button', () => {
    const dialogRef = dialogService.open(DialogContentComponent);
    dialogRef.afterClosed().subscribe(result => {
      expect(result).toBeFalsy();
      expect(ocEl.querySelector('aui-dialog')).toBeNull();
    });

    fixture.detectChanges();
    ocEl.querySelector('#cancel').dispatchEvent(new Event('click'));
  });

  it('should open dialog with template portal', () => {
    const ins = fixture.componentInstance;
    dialogService.open(ins.templateRef);
    fixture.detectChanges();

    expect(ocEl.querySelector('.aui-dialog button').innerHTML).toContain(
      'close',
    );
  });

  it('should be closed by click close button', done => {
    const ins = fixture.componentInstance;
    ins.result = 'result';
    dialogService
      .open(ins.templateRef)
      .afterClosed()
      .subscribe(result => {
        expect(result).toBe('result');
        expect(ocEl.querySelector('aui-dialog')).toBeNull();
        done();
      });
    fixture.detectChanges();
    ocEl.querySelector('#close').dispatchEvent(new Event('click'));
    fixture.detectChanges();
  });

  it('should open confirm dialog and invoke confirm callback', done => {
    dialogService
      .confirm({
        title: 'title',
        content: 'content',
      })
      .then(() => {
        expect(ocEl.querySelector('aui-dialog')).toBeNull();
        done();
      });
    fixture.detectChanges();

    ocEl
      .querySelector('.aui-confirm-dialog__confirm-button')
      .dispatchEvent(new Event('click'));
  });

  it('should open confirm dialog and invoke cancell callback', done => {
    dialogService
      .confirm({
        title: 'custom title',
        content: 'custom content',
      })
      .catch(() => {
        expect(ocEl.querySelector('aui-dialog')).toBeNull();
        done();
      });
    fixture.detectChanges();

    expect(
      ocEl.querySelector('.aui-confirm-dialog__title').innerHTML,
    ).toContain('custom title');
    expect(
      ocEl.querySelector('.aui-confirm-dialog__content').innerHTML,
    ).toContain('custom content');
    ocEl
      .querySelector('.aui-confirm-dialog__cancel-button')
      .dispatchEvent(new Event('click'));
  });

  it('should before confirm work correctly', done => {
    const t1 = new Date().getTime();
    dialogService
      .confirm({
        title: '',
        beforeConfirm: reslove => {
          setTimeout(reslove, 100);
        },
      })
      .then(() => {
        const t2 = new Date().getTime();
        expect(t2 - t1).toBeGreaterThanOrEqual(100);
        done();
      });
    fixture.detectChanges();

    const confirmBtn = ocEl.querySelector(
      '.aui-confirm-dialog__confirm-button',
    ) as HTMLButtonElement;
    confirmBtn.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(confirmBtn.className).toContain('isLoading');
    expect(confirmBtn.disabled).toBeTruthy();
  });

  it('should before cancel work correctly', done => {
    const t1 = new Date().getTime();
    dialogService
      .confirm({
        title: '',
        beforeCancel: reslove => {
          setTimeout(reslove, 100);
        },
      })
      .catch(() => {
        const t2 = new Date().getTime();
        expect(t2 - t1).toBeGreaterThanOrEqual(100);
        done();
      });
    fixture.detectChanges();

    const cancelBtn = ocEl.querySelector(
      '.aui-confirm-dialog__cancel-button',
    ) as HTMLButtonElement;
    cancelBtn.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(cancelBtn.className).toContain('isLoading');
    expect(cancelBtn.disabled).toBeTruthy();
  });

  it('should before confirm callback could prevent close event', async done => {
    dialogService
      .confirm({
        title: '',
        beforeConfirm: (reslove, reject) => {
          setTimeout(reject, 100);
        },
      })
      .then(() => {
        throw Error('confirm dialog should not be closed');
      })
      .catch(() => {
        throw Error('confirm dialog should not be closed');
      });
    fixture.detectChanges();

    const confirmBtn = ocEl.querySelector(
      '.aui-confirm-dialog__confirm-button',
    ) as HTMLButtonElement;
    const cancelBtn = ocEl.querySelector(
      '.aui-confirm-dialog__cancel-button',
    ) as HTMLButtonElement;
    confirmBtn.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(confirmBtn.className).toContain('isLoading');
    expect(confirmBtn.disabled).toBeTruthy();
    expect(cancelBtn.disabled).toBeTruthy();

    await sleep(100);
    fixture.detectChanges();

    expect(ocEl.querySelector('aui-dialog')).not.toBeNull();
    expect(confirmBtn.className).not.toContain('isLoading');
    expect(confirmBtn.disabled).toBeFalsy();
    expect(cancelBtn.disabled).toBeFalsy();

    done();
  });

  it('should be closed by closeAll method', async () => {
    const ins = fixture.componentInstance;
    dialogService.open(ins.templateRef);
    dialogService.confirm({
      title: 'title',
    });
    fixture.detectChanges();
    dialogService.closeAll();
    fixture.detectChanges();
    await sleep();
  });
});

@Component({
  template: `
  <ng-template #template>
    <aui-dialog-header [closeable]="false">title</aui-dialog-header>
    <button id="close" [auiDialogClose]="result">close</button>
  </ng-template>`,
})
class TestComponent {
  result: any;
  @ViewChild('template') templateRef: TemplateRef<any>;
}

@Component({
  selector: 'test-dialog-content',
  template: `
    <aui-dialog-header>
      title
    </aui-dialog-header>
    <aui-dialog-content>
      content
    </aui-dialog-content>
    <aui-dialog-footer>
      <div>
          <button id="confirm" [auiDialogClose]="true"></button>
          <button id="cancel" [auiDialogClose]="false"></button>
      </div>
    </aui-dialog-footer>`,
})
class DialogContentComponent {}

@NgModule({
  imports: [DialogModule],
  declarations: [DialogContentComponent, TestComponent],
  entryComponents: [DialogContentComponent],
})
class TestMoudle {}
