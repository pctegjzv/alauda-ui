import {
  AfterViewInit,
  ChangeDetectorRef,
  DoCheck,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

import debounce from 'lodash.debounce';
import isEqual from 'lodash.isequal';

import { MonacoProviderService } from './monaco-provider.service';
import { ResizeSensorService } from './resize-sensor.service';

/**
 * Wraps powerful Monaco Editor for simpilicity use in Angular.
 */
export abstract class MonacoCommonEditorComponent
  implements
    OnInit,
    OnChanges,
    AfterViewInit,
    OnDestroy,
    DoCheck,
    ControlValueAccessor {
  protected model: any;
  protected _value = '';
  protected _prevOptions: any;
  protected destroyed = false;
  protected editor: any;

  monacoLoaded = false;

  @ViewChild('monacoContainer') protected monacoContainer: ElementRef;
  @ViewChild('monacoAnchor') protected monacoAnchor: ElementRef;

  /**
   * Raw Monaco editor options
   */
  @Input() options: any;

  /**
   * The URI which will be assigned to monaco-editor's model.
   * See monaco.Uri
   */
  @Input() modelUri: any;

  /**
   * Events emitted when monaco editor changed.
   */
  @Output() monacoEditorChanged = new EventEmitter();

  /**
   * A helper ID to let the user to see the embedded monaco editor ID.
   *
   * E.g., you could use the following to get the embedded value of the editor.
   *     monaco.editor
   *      .getModels()
   *      .find(
   *        model =>
   *          model.id ===
   *          document.querySelector('aui-code-editor [model-id]').attributes[
   *            'model-id'
   *          ].value,
   *      )
   *      .getValue();
   */
  @HostBinding('attr.model-id') modelId: string;

  abstract createEditor(): any;

  ngDoCheck(): void {
    // We should reset the editor when options change.
    if (this._prevOptions && !isEqual(this._prevOptions, this.options)) {
      this.initEditor();
    }
    this._prevOptions = this.options;
  }

  constructor(
    protected zone: NgZone,
    protected monacoProvider: MonacoProviderService,
    protected cdr: ChangeDetectorRef,
    protected resizeSensor: ResizeSensorService,
  ) {}

  ngOnInit() {
    this.initEditor();
  }

  ngAfterViewInit(): void {
    const debouncedEditorResize = debounce(() => {
      if (this.editor) {
        this.editor.layout();
      }
    }, 100);
    this.resizeSensor.registerResize(
      this.monacoContainer.nativeElement,
      debouncedEditorResize,
    );
  }

  ngOnChanges({ options }: SimpleChanges): void {}

  ngOnDestroy(): void {
    this.destroyed = true;
    if (this.editor) {
      this.editor.dispose();
    }
    this.editor = null;
  }

  // Following are APIs required by ControlValueAccessor
  onChange = (_: any) => {};
  onTouched = () => {};

  writeValue(value: any): void {
    this._value = value || '';
    // Fix for value change while dispose in process.
    setTimeout(() => {
      if (this.editor) {
        this.updateEditorValue(this._value);
      }
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  private async initEditor() {
    await this.monacoProvider.initMonaco();
    this.monacoLoaded = true;

    if (this.editor) {
      this.editor.dispose();
    }

    if (!this.destroyed) {
      this.editor = this.createEditor();
      this.listenModelChanges();
      this.monacoEditorChanged.emit(this.editor);
      this.modelId = this.model.id;
      this.cdr.markForCheck();
    }
  }

  private listenModelChanges(): void {
    this.model.onDidChangeContent(() => {
      const value = this.model.getValue();
      if (this._value === value) {
        return;
      }
      this.onChange(value);

      // value is not propagated to parent when executing outside zone.
      this.zone.run(() => (this._value = value));
    });
  }

  private updateEditorValue(value: string): void {
    this.model.setValue(value);
  }
}
