import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CheckboxModule } from '../checkbox/public-api';
import { DialogModule } from '../dialog/dialog.module';
import { IconModule } from '../icon/public-api';
import { TooltipModule } from '../tooltip/tooltip.module';
import { CodeColorizeDirective } from './code-colorize.directive';
import { CODE_EDITOR_INTL_INTL_PROVIDER } from './code-editor-intl.service';
import { CodeEditorComponent } from './code-editor.component';
import { MonacoDiffEditorComponent } from './monaco-diff-editor.component';
import { MonacoEditorConfig } from './monaco-editor-config';
import { MonacoEditorComponent } from './monaco-editor.component';
import { MonacoProviderService } from './monaco-provider.service';
import { ResizeSensorService } from './resize-sensor.service';

const EXPORTABLES = [
  MonacoEditorComponent,
  MonacoDiffEditorComponent,
  CodeEditorComponent,
  CodeColorizeDirective,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TooltipModule,
    DialogModule,
    IconModule,
    CheckboxModule,
  ],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
  entryComponents: [CodeEditorComponent],
})
export class CodeEditorModule {
  public static forRoot(config: MonacoEditorConfig = {}): ModuleWithProviders {
    return {
      ngModule: CodeEditorModule,
      providers: [
        { provide: MonacoEditorConfig, useValue: config },
        MonacoProviderService,
        ResizeSensorService,
        CODE_EDITOR_INTL_INTL_PROVIDER,
      ],
    };
  }
}
