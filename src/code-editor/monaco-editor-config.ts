/**
 * Configuration over monaco editor.
 */
export class MonacoEditorConfig {
  // What is the URL to monaco editor library
  // e.g., node_modules/monaco-editor/min
  baseUrl?: string;

  // Default options when creating editors
  defaultOptions?: { [key: string]: any };
}
