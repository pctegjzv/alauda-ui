import { Injectable, Optional, SkipSelf } from '@angular/core';

/**
 * Texts to render on the code editor.
 */
@Injectable()
export class CodeEditorIntl {
  copyLabel = '复制';

  copiedLabel = '已复制';

  readonlyLabel = '只读';

  readwriteLabel = '读写';

  clearLabel = '清空';

  recoverLabel = '恢复';

  findLabel = '查找';

  formatLabel = '格式化';

  foldLabel = '折叠';

  exportLabel = '导出';

  importLabel = '导入';

  lightThemeLabel = '日间';

  darkThemeLabel = '夜间';

  showDiffLabel = '显示更新';

  fullscreenLabel = '全屏';

  exitFullscreenLabel = '退出全屏';

  // 针对某个语言翻译成面向用户的标签.
  // 比如: yaml -> YAML, jenkinsfile -> Jenkinsfile.
  getLanguageLabel(lang: string) {
    return ('' + lang).toUpperCase();
  }
}

export function CODE_EDITOR_INTL_PROVIDER_FACTORY(parentIntl: CodeEditorIntl) {
  return parentIntl || new CodeEditorIntl();
}

/** @docs-private */
export const CODE_EDITOR_INTL_INTL_PROVIDER = {
  // If there is already an CodeEditorIntl available, use that. Otherwise, provide a new one.
  provide: CodeEditorIntl,
  deps: [[new Optional(), new SkipSelf(), CodeEditorIntl]],
  useFactory: CODE_EDITOR_INTL_PROVIDER_FACTORY,
};
