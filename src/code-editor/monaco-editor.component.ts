import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  NgZone,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { MonacoCommonEditorComponent } from './monaco-common-editor.component';
import { MonacoProviderService } from './monaco-provider.service';
import { ResizeSensorService } from './resize-sensor.service';

declare const monaco: any;

/**
 * Wraps powerful Monaco Editor for simpilicity use in Angular.
 */
@Component({
  selector: 'aui-monaco-editor',
  templateUrl: './monaco-editor.component.html',
  styleUrls: ['./monaco-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MonacoEditorComponent),
      multi: true,
    },
  ],
})
export class MonacoEditorComponent extends MonacoCommonEditorComponent {
  constructor(
    zone: NgZone,
    monacoProvider: MonacoProviderService,
    cdr: ChangeDetectorRef,
    resizeSensor: ResizeSensorService,
  ) {
    super(zone, monacoProvider, cdr, resizeSensor);
  }

  createEditor(): any {
    this.model = monaco.editor.createModel(
      this._value,
      this.options.language,
      this.modelUri,
    );
    return this.monacoProvider.create(this.monacoAnchor.nativeElement, {
      ...this.options,
      model: this.model,
    });
  }
}
