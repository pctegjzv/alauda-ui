import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  Optional,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import clipboard from 'clipboard-polyfill';
import { saveAs } from 'file-saver';

import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
} from '../dialog/public-api';
import { CodeEditorIntl } from './code-editor-intl.service';
import { MonacoProviderService } from './monaco-provider.service';

export interface CodeEditorActionsConfig {
  diffMode?: boolean;
  recover?: boolean;
  clear?: boolean;
  format?: boolean;
  find?: boolean;
  copy?: boolean;
  theme?: boolean;
  fullscreen?: boolean;
  export?: boolean;
  import?: boolean;
}

const DEFAULT_ACTIONS_CONFIG: CodeEditorActionsConfig = {
  diffMode: true,
  recover: true,
  clear: true,
  format: true,
  find: true,
  copy: true,
  theme: true,
  fullscreen: true,
  export: true,
  import: true,
};

/**
 * Code editor.
 */
@Component({
  selector: 'aui-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CodeEditorComponent),
      multi: true,
    },
  ],
})
export class CodeEditorComponent
  implements ControlValueAccessor, AfterViewInit, OnDestroy {
  private editor: any;
  protected _options = {};
  protected _value = '';
  protected _actionsConfig: CodeEditorActionsConfig = DEFAULT_ACTIONS_CONFIG;
  protected _originalValue = '';
  protected _diffMode = false;
  protected _modelUri: string;
  protected _monacoEditorChanged = new EventEmitter<any>();

  /**
   * Raw Monaco editor options
   */
  @Input()
  get options() {
    if (this.source) {
      return this.source.options;
    } else {
      return this._options;
    }
  }

  set options(options: any) {
    if (this.source) {
      this.source.options = options;
    } else {
      this._options = options;
    }
  }

  /**
   * Value input. Optinally, you could access the model via ControlValueAccessor
   * like ngModel.
   */
  @Input()
  set value(value: string) {
    if (this.source) {
      this.source.value = value;
    } else {
      this.writeValue(value);
    }
  }

  get value() {
    if (this.source) {
      return this.source.value;
    } else {
      return this._value;
    }
  }

  /**
   * The original value to recover
   */
  @Input()
  get originalValue() {
    if (this.source) {
      return this.source.originalValue;
    } else {
      return this._originalValue;
    }
  }

  set originalValue(originalValue: string) {
    if (this.source) {
      this.source.originalValue = originalValue;
    } else {
      this._originalValue = originalValue;
    }
  }

  /**
   * Configure action buttons display. All actions are enabled by default.
   */
  @Input()
  get actionsConfig() {
    if (this.source) {
      return this.source.actionsConfig;
    } else {
      return this._actionsConfig;
    }
  }

  set actionsConfig(actionsConfig: CodeEditorActionsConfig) {
    if (this.source) {
      this.source.actionsConfig = actionsConfig;
    } else {
      this._actionsConfig = { ...DEFAULT_ACTIONS_CONFIG, ...actionsConfig };
      this.cdr.markForCheck();
    }
  }

  /**
   * Should show as diff mode or not
   */
  @Input()
  get diffMode(): boolean {
    if (this.source) {
      return this.source.diffMode;
    } else {
      return this._diffMode;
    }
  }

  set diffMode(diffMode: boolean) {
    if (this.source) {
      this.source.diffMode = diffMode;
    } else {
      this._diffMode = diffMode;
    }
  }

  /**
   * The URI which will be assigned to monaco-editor's model.
   */
  @Input() modelUri: string;

  /**
   * Events emitted when monaco editor changed.
   * User may want to use this to have advanced tunnings on the editor.
   */
  @Output()
  get monacoEditorChanged(): EventEmitter<any> {
    if (this.source) {
      return this.source.monacoEditorChanged;
    } else {
      return this._monacoEditorChanged;
    }
  }

  /**
   * The fullscreen dialog.
   */
  fullscreenDialog: DialogRef<CodeEditorComponent>;

  @ViewChild('fileUploadInput')
  fileUploadInputElementRef: ElementRef<HTMLInputElement>;

  constructor(
    public monacoProvider: MonacoProviderService,
    public cdr: ChangeDetectorRef,
    public codeEditorIntl: CodeEditorIntl,
    @Optional() public dialog: DialogService,
    @Optional()
    @Inject(DIALOG_DATA)
    public source: CodeEditorComponent,
  ) {
    // If the provided DIALOG_DATA is not CodeEditorComponent, we believe
    // this code editor instance is not opened within a maximum dialog and
    // it should be reset to null.
    if (!(this.source instanceof CodeEditorComponent)) {
      this.source = undefined;
    }
  }

  onThemeChange(event: Event) {
    const selectEl: HTMLSelectElement = event.target as HTMLSelectElement;
    const theme = selectEl.selectedOptions[0].value;
    this.monacoProvider.changeTheme(theme);
    event.stopPropagation();
  }

  onMonacoEditorChanged(editor: any) {
    this.editor = editor;
    this.monacoEditorChanged.emit(editor);
  }

  onValueChange(value: any) {
    if (this.source) {
      this.source.onValueChange(value);
    } else {
      this.onChange(value);
      this.value = value;
    }
  }

  onClearClicked() {
    this.onValueChange('');
  }

  onExportClicked() {
    const lang = this.monacoProvider.getLanguageExtensionPoint(
      this.options.language,
    );

    const fileExtension = (lang && lang.extensions[0]) || '.txt';
    const mimeType = (lang && lang.mimetypes[0]) || 'text/plain';

    const blob = new Blob([this.value], { type: mimeType + ';charset=utf-8' });

    saveAs(blob, new Date().toISOString() + fileExtension);
  }

  onImportFileChanged(_event: Event) {
    const reader = new FileReader();

    // TODO: handle possible errors?
    reader.onloadend = () => {
      this.onValueChange(reader.result);
    };

    reader.readAsText(this.fileUploadInputElementRef.nativeElement.files[0]);
  }

  onRecoverClicked() {
    this.onValueChange(this.originalValue);
  }

  onFindClicked() {
    return this.checkActionAndRun('actions.find');
  }

  canFormat() {
    const action = this.getEditorAction('editor.action.formatDocument');
    return action && action.isSupported();
  }

  canFullscreen() {
    return !!this.dialog && this.actionsConfig.fullscreen;
  }

  onFormatClicked() {
    return this.checkActionAndRun('editor.action.formatDocument');
  }

  onFoldAllClicked() {
    return this.checkActionAndRun('editor.foldAll');
  }

  getLanguageExtensions() {
    if (this.editor) {
      const lang = this.monacoProvider.getLanguageExtensionPoint(
        this.options.language,
      );
      if (lang) {
        return lang.extensions;
      }
    }
  }

  getInputFileAccepts() {
    const extensions = this.getLanguageExtensions();
    return extensions && extensions.join(', ');
  }

  private checkActionAndRun(actionName: string) {
    const action = this.getEditorAction(actionName);
    return action && action.run();
  }

  private getEditorAction(actionName: string) {
    return this.editor && this.editor.getAction(actionName);
  }

  onCopyClicked() {
    clipboard.writeText(this.value);
  }

  // Following are APIs required by ControlValueAccessor
  onChange = (_: any) => {};
  onTouched = () => {};

  writeValue(value: any): void {
    if (this.source) {
      this.source.writeValue(value);
    } else {
      if (this.value !== value) {
        this._value = value;
        this.cdr.markForCheck();
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  toggleFullscreen() {
    if (this.fullscreenDialog) {
      this.fullscreenDialog.close();
      this.fullscreenDialog = undefined;
      this.cdr.markForCheck();
    } else if (this.source) {
      this.source.toggleFullscreen();
    } else {
      this.fullscreenDialog = this.dialog.open(CodeEditorComponent, {
        size: DialogSize.Fullscreen,
        data: this,
      });
      this.fullscreenDialog.afterClosed().subscribe(() => {
        this.fullscreenDialog = undefined;
      });
      this.cdr.markForCheck();
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy(): void {
    if (this.fullscreenDialog) {
      this.fullscreenDialog.close();
    }
    this.source = undefined;
  }
}
