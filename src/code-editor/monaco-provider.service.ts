import { Injectable } from '@angular/core';

import { MonacoEditorConfig } from './monaco-editor-config';

declare const monaco: any;
let fetchingPromise: Promise<any> = null;

/**
 * Provider for monaco editor.
 */
@Injectable()
export class MonacoProviderService {
  constructor(private monacoEditorConfig: MonacoEditorConfig) {}

  private _theme: string = this.themes[0];

  initMonaco(): Promise<any> {
    if (!fetchingPromise) {
      fetchingPromise = new Promise((resolve, reject) => {
        if ((window as any).monaco) {
          resolve();
        } else {
          const onAmdLoader = () => {
            (window as any).require.config({
              paths: { vs: `${this.monacoEditorConfig.baseUrl}/vs` },
            });
            (window as any).require.config({
              'vs/nls': {
                availableLanguages: {
                  '*': 'zh-cn',
                },
              },
            });
            (window as any).require(['vs/editor/editor.main'], () => {
              this.defineThemes();
              resolve();
            });
          };
          const onAmdLoaderError = (error: ErrorEvent) => {
            console.error('failed to load monaco', error);
            reject(error);
          };

          const loaderScript = document.createElement('script');
          loaderScript.type = 'text/javascript';
          loaderScript.src = `${this.monacoEditorConfig.baseUrl}/vs/loader.js`;
          loaderScript.addEventListener('load', () => onAmdLoader());
          loaderScript.addEventListener('error', error =>
            onAmdLoaderError(error),
          );
          document.body.appendChild(loaderScript);
        }
      });
    }

    return fetchingPromise;
  }

  /**
   * Returns all available themes
   */
  get themes() {
    return ['aui', 'aui-dark'];
  }

  /**
   * Return the current theme
   */
  get theme() {
    return this._theme;
  }

  get isDarkTheme() {
    return this._theme && this._theme.endsWith('-dark');
  }

  toggleTheme() {
    this.changeTheme(this.isDarkTheme ? 'aui' : 'aui-dark');
  }

  /**
   * Check if monaco is ready or not. Note, we are not initialize it here since
   * we want the api calls to monaco stays synchronous.
   */
  checkMonaco() {
    if (!(window as any).monaco) {
      throw new Error('Monaco editor has not yet been initialized!');
    }
  }

  changeTheme(theme: string) {
    this.checkMonaco();
    this._theme = theme;
    monaco.editor.setTheme(theme);
  }

  getEditorOptions(options: any) {
    return {
      ...this.monacoEditorConfig.defaultOptions,
      theme: this.theme,
      ...options,
    };
  }

  create(domElement: HTMLElement, options?: any) {
    this.checkMonaco();
    return monaco.editor.create(domElement, this.getEditorOptions(options));
  }

  createDiffEditor(domElement: HTMLElement, options?: any) {
    this.checkMonaco();
    return monaco.editor.createDiffEditor(domElement, {
      renderSideBySide: false,
      // You can optionally disable the resizing
      enableSplitViewResizing: false,
      ...this.getEditorOptions(options),
    });
  }

  colorizeElement(domElement: HTMLElement, options?: any) {
    this.checkMonaco();
    return monaco.editor.colorizeElement(domElement, {
      theme: this.theme,
      ...options,
    });
  }

  getLanguageExtensionPoint(alias: string) {
    this.checkMonaco();
    return monaco.languages
      .getLanguages()
      .find(
        (language: any) =>
          (language.aliases && language.aliases.includes(alias)) ||
          language.id === alias,
      );
  }

  private defineThemes() {
    monaco.editor.defineTheme('aui', {
      base: 'vs',
      inherit: true,
      rules: [],
      colors: {
        'editor.background': '#ffffff',
        'editorCursor.foreground': '#526FFF',
      },
    });

    monaco.editor.defineTheme('aui-dark', {
      base: 'vs-dark',
      inherit: true,
      rules: [
        {
          token: 'comment',
          foreground: '5c636f',
          fontStyle: 'italic',
        },
        {
          token: 'type',
          foreground: 'DE6D77',
        },
        {
          token: 'string',
          foreground: '99C27C',
        },
      ],
      colors: {
        'editor.foreground': '#ffffff',
        'editor.background': '#273238',
      },
    });
  }
}
