import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  NgZone,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { MonacoCommonEditorComponent } from './monaco-common-editor.component';
import { MonacoProviderService } from './monaco-provider.service';
import { ResizeSensorService } from './resize-sensor.service';

declare const monaco: any;

/**
 * Wraps powerful Monaco Editor for simpilicity use in Angular.
 */
@Component({
  selector: 'aui-monaco-diff-editor',
  templateUrl: './monaco-editor.component.html',
  styleUrls: ['./monaco-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MonacoDiffEditorComponent),
      multi: true,
    },
  ],
})
export class MonacoDiffEditorComponent extends MonacoCommonEditorComponent
  implements OnChanges {
  protected originalModel: any;

  @Input() originalValue: string;

  constructor(
    zone: NgZone,
    monacoProvider: MonacoProviderService,
    cdr: ChangeDetectorRef,
    resizeSensor: ResizeSensorService,
  ) {
    super(zone, monacoProvider, cdr, resizeSensor);
  }

  createEditor(): any {
    this.originalModel = monaco.editor.createModel(
      this.originalValue,
      this.options.language,
      this.modelUri,
    );
    this.model = monaco.editor.createModel(
      this.originalValue,
      this.options.language,
      this.modelUri,
    );

    const editor = this.monacoProvider.createDiffEditor(
      this.monacoAnchor.nativeElement,
      this.options,
    );

    editor.setModel({
      original: this.originalModel,
      modified: this.model,
    });

    return editor.getModifiedEditor();
  }

  ngOnChanges({ originalValue }: SimpleChanges): void {
    super.ngOnChanges({ originalValue });
    if (originalValue && this.originalModel) {
      this.originalModel.setValue(this.originalValue);
    }
  }
}
