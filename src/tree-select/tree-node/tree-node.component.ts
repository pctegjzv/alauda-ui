import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { Bem, buildBem } from '../../utils/bem';
import { scrollIntoView } from '../../utils/scroll-into-view';
import { TreeSelectComponent } from '../tree-select.component';
import { TreeNode } from '../tree-select.types';

@Component({
  selector: 'aui-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class TreeNodeComponent implements AfterViewInit, OnDestroy {
  bem: Bem = buildBem('aui-tree-node');

  private _nodeData: TreeNode;
  private nodeData$$ = new BehaviorSubject<TreeNode>(this.nodeData);
  private destory$$ = new Subject<void>();

  @Input()
  get nodeData() {
    return this._nodeData;
  }
  set nodeData(val) {
    if (val === this._nodeData) {
      return;
    }
    this._nodeData = val;
    this.nodeData$$.next(val);
  }

  @ViewChild('titleRef') titleRef: ElementRef;
  @ViewChildren(TreeNodeComponent) childNodes: QueryList<TreeNodeComponent>;

  selected = false;
  visable = true;

  selected$: Observable<boolean> = combineLatest(
    this.select.trackFn$,
    this.select.value$,
    this.nodeData$$.pipe(map(data => data.value)),
  ).pipe(
    map(
      ([trackFn, selectValue, selfValue]) =>
        selectValue && trackFn(selectValue) === trackFn(selfValue),
    ),
    tap(selected => {
      this.selected = selected;
    }),
    publishReplay(1),
    refCount(),
  );

  selfVisable$: Observable<boolean> = combineLatest(
    this.select.filterFn$,
    this.select.filterString$,
    this.nodeData$$,
  ).pipe(
    map(([filterFn, filterString, nodeData]) =>
      filterFn(filterString, nodeData),
    ),
    publishReplay(1),
    refCount(),
  );

  visable$: Observable<boolean>;

  constructor(
    @Inject(forwardRef(() => TreeSelectComponent))
    private select: TreeSelectComponent,
    private cdr: ChangeDetectorRef,
  ) {}

  ngAfterViewInit() {
    const hasVisableChildNodes$ = this.childNodes.changes.pipe(
      startWith(this.childNodes),
      map(nodes => nodes.filter(node => node !== this)),
      switchMap(nodes => {
        if (nodes.length) {
          return combineLatest(nodes.map(node => node.visable$));
        } else {
          return of([false]);
        }
      }),
      map(values => values.some(value => value)),
    );
    this.visable$ = combineLatest(
      this.selfVisable$,
      hasVisableChildNodes$,
    ).pipe(
      map(values => values.some(value => value)),
      publishReplay(1),
      refCount(),
    );

    this.visable$.pipe(takeUntil(this.destory$$)).subscribe(visable => {
      this.visable = visable;
      this.cdr.markForCheck();
    });
    this.selected$.pipe(takeUntil(this.destory$$)).subscribe(selected => {
      this.selected = selected;
      this.cdr.markForCheck();
    });

    if (this.selected) {
      requestAnimationFrame(() => {
        this.scrollToNode(this);
      });
    }
  }

  ngOnDestroy() {
    this.destory$$.next();
    this.destory$$.complete();
  }

  onClick() {
    if (this.nodeData.disabled) {
      return;
    }
    this.select.onNodeClick(this);
  }

  switchExpanded() {
    this.nodeData.expanded = !this.nodeData.expanded;
    if (this.nodeData.expanded && this.childNodes.last) {
      requestAnimationFrame(() => {
        this.scrollToNode(this.childNodes.last);
      });
    }
    requestAnimationFrame(() => {
      this.select.updatePosition();
    });
  }

  getIcon() {
    return this.nodeData.expanded
      ? this.nodeData.expandedIcon || this.nodeData.icon
      : this.nodeData.icon;
  }

  trackByLabel(_: number, data: TreeNode) {
    return data.label;
  }

  scrollToNode(node: TreeNodeComponent) {
    scrollIntoView(
      this.select.nodeListRef.nativeElement,
      node.titleRef.nativeElement,
    );
  }
}
