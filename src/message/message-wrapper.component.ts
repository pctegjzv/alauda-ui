import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Optional,
  ViewEncapsulation,
} from '@angular/core';
import {
  MESSAGE_CONFIG,
  MESSAGE_DEFAULT_CONFIG,
  MessageConfig,
} from './message.config';

@Component({
  selector: 'aui-message-wrapper',
  styleUrls: ['./message-wrapper.component.scss'],
  template: '',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class MessageWrapperComponent {
  config: MessageConfig;
  constructor(
    public elementRef: ElementRef,
    @Optional()
    @Inject(MESSAGE_DEFAULT_CONFIG)
    defaultConfig: MessageConfig,
    @Optional()
    @Inject(MESSAGE_CONFIG)
    config: MessageConfig,
  ) {
    this.setConfig(defaultConfig, config);
  }

  setConfig(preConfig: MessageConfig, currConfig: MessageConfig) {
    this.config = { ...preConfig, ...currConfig };
  }
}
