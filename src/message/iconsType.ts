import { MessageType } from './message.config';

export const iconsType: { [key: string]: string } = {
  [MessageType.Error]: 'exclamation_triangle_s',
  [MessageType.Success]: 'check_circle_s',
  [MessageType.Warning]: 'exclamation_circle_s',
  [MessageType.Info]: 'info_circle_s',
};
