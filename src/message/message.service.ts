import { Overlay } from '@angular/cdk/overlay';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  Injector,
} from '@angular/core';
import { MessageBaseService } from './message-base.service';
import { MessageWrapperComponent } from './message-wrapper.component';
import { MessageComponent } from './message.component';
import { MessageConfig, MessageType } from './message.config';

@Injectable({
  providedIn: 'root',
})
export class MessageService extends MessageBaseService<
  MessageWrapperComponent,
  MessageComponent,
  MessageConfig
> {
  constructor(
    overlay: Overlay,
    injector: Injector,
    applicationRef: ApplicationRef,
    cfr: ComponentFactoryResolver,
  ) {
    super(
      overlay,
      MessageWrapperComponent,
      MessageComponent,
      injector,
      applicationRef,
      cfr,
      'aui-message-',
    );
  }

  // message
  success(
    messageOptions: MessageConfig | string,
  ): ComponentRef<MessageComponent> {
    const messageConfig = Object.assign(
      { type: MessageType.Success },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  error(
    messageOptions: MessageConfig | string,
  ): ComponentRef<MessageComponent> {
    const messageConfig = Object.assign(
      { type: MessageType.Error },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  warning(
    messageOptions: MessageConfig | string,
  ): ComponentRef<MessageComponent> {
    const messageConfig = Object.assign(
      { type: MessageType.Warning },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }

  info(messageOptions: MessageConfig | string): ComponentRef<MessageComponent> {
    const messageConfig = Object.assign(
      { type: MessageType.Info },
      this.parseOptions(messageOptions),
    );
    return this.createMessage(messageConfig);
  }
}
