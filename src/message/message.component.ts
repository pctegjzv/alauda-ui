import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { Subject } from 'rxjs';
import { iconsType } from './iconsType';
import { MessageAnimations } from './message-animations';
import { MessageType } from './message.config';

@Component({
  selector: 'aui-message',
  templateUrl: './message.component.html',
  animations: [MessageAnimations.inOut],
  styleUrls: ['./message.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class MessageComponent implements OnInit, AfterViewInit {
  animate = 'slideDown';
  timerId: any;
  remains: number;
  uniqueId: string;
  id: string;
  type: MessageType = MessageType.Success;
  content: string;
  duration: number;
  beforeClosed: Subject<void> = new Subject();
  afterClosed: Subject<void> = new Subject();

  get iconType(): string {
    return iconsType[this.type];
  }

  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.remains = Math.ceil(this.duration / 1000);
  }

  ngAfterViewInit(): void {
    if (this.duration > 0) {
      this.timerId = setTimeout(() => {
        this.clear();
      }, this.duration);
    }
  }

  clear() {
    clearTimeout(this.timerId);
    this.animate = 'slideUp';
    this.cdr.markForCheck();
    this.beforeClosed.next();
    this.beforeClosed.complete();
  }

  onAnimationEnd(e: any) {
    try {
      if (this.viewContainerRef.element && e.toState === 'slideUp') {
        this.viewContainerRef.element.nativeElement.parentElement.removeChild(
          this.viewContainerRef.element.nativeElement,
        );
        this.afterClosed.next();
        this.afterClosed.complete();
      }
    } catch (e) {
      throw Error('No outer layer can be found!');
    }
  }
}
