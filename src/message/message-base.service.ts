import { Overlay } from '@angular/cdk/overlay';
import {
  ComponentPortal,
  ComponentType,
  DomPortalHost,
} from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Injector,
} from '@angular/core';
import { MessageWrapperComponent } from './message-wrapper.component';
import { MessageComponent } from './message.component';
import { MessageConfig } from './message.config';

let uniqueId = 0;

export class MessageBaseService<
  ContainerClass extends MessageWrapperComponent,
  ComponentClass extends MessageComponent,
  Config extends MessageConfig
> {
  static readonly MESSAGE_OVERLAY_PANE_CLASS = 'aui-message-overlay-panel';
  wrapperContainer: ContainerClass;
  messagesFillDatas: Array<ComponentRef<ComponentClass>> = [];

  constructor(
    protected overlay: Overlay,
    protected containerClass: ComponentType<ContainerClass>,
    protected componentClass: ComponentType<ComponentClass>,
    protected injector: Injector,
    protected applicationRef: ApplicationRef,
    protected cfr: ComponentFactoryResolver,
    protected uniqueIdPrefix: string = 'aui-message-',
  ) {
    this.initWrapperContainer();
  }

  protected initWrapperContainer() {
    if (!this.wrapperContainer) {
      this.wrapperContainer = this.overlay
        .create({
          panelClass: MessageBaseService.MESSAGE_OVERLAY_PANE_CLASS,
        })
        .attach(new ComponentPortal(this.containerClass)).instance;
    }
  }

  protected initComponentRef(
    messageConfig: Config,
  ): ComponentRef<ComponentClass> {
    const modalContainerPortalHost = new DomPortalHost(
      this.wrapperContainer.elementRef.nativeElement,
      this.cfr,
      this.applicationRef,
      this.injector,
    );
    const componentRef = modalContainerPortalHost.attachComponentPortal(
      new ComponentPortal(this.componentClass),
    );
    Object.assign(
      componentRef.instance,
      this.wrapperContainer.config,
      messageConfig,
      { uniqueId: this.generateUniqueId() },
    );
    return componentRef;
  }

  protected createMessage(messageConfig: Config) {
    const componentRef = this.initComponentRef(messageConfig);

    componentRef.instance.afterClosed.subscribe(() => {
      componentRef.destroy();
      this.remove(componentRef.instance.uniqueId);
    });

    this.exclude(messageConfig);
    this.messagesFillDatas.push(componentRef);
    return componentRef;
  }

  protected generateUniqueId() {
    return `${this.uniqueIdPrefix}${uniqueId++}`;
  }

  protected exclude(messageConfig: Config) {
    if (messageConfig.id) {
      this.remove(messageConfig.id);
    }
    if (
      this.wrapperContainer.config.maxStack &&
      this.messagesFillDatas.length >= this.wrapperContainer.config.maxStack
    ) {
      const toRemoveMessages = this.messagesFillDatas.slice(
        0,
        this.messagesFillDatas.length -
          this.wrapperContainer.config.maxStack +
          1,
      );
      toRemoveMessages.forEach(ref => this.remove(ref.instance.uniqueId));
    }
  }

  removeAll() {
    this.messagesFillDatas.forEach(ref => {
      ref.instance.clear();
    });
    this.messagesFillDatas = [];
  }

  remove(id: string) {
    if (!id) {
      return;
    }
    const index = this.messagesFillDatas.findIndex(
      ref => ref.instance.uniqueId === id || ref.instance.id === id,
    );
    this.messagesFillDatas[index].instance.clear();
    this.messagesFillDatas.splice(index, 1);
  }

  protected parseOptions(options: string | MessageConfig) {
    return typeof options === 'string' ? { content: options } : options;
  }
}
