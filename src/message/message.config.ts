import { InjectionToken } from '@angular/core';

export enum MessageType {
  Error = 'error',
  Success = 'success',
  Warning = 'warning',
  Info = 'info',
}

/**
 * messageOptions: containes Basic configuration
 * Just for some containers like Message-Container
 * Detailed document： http://confluence.alaudatech.com/pages/viewpage.action?pageId=23383163
 */

export interface MessageConfig {
  /**
   * the message type
   */
  type?: MessageType;
  /**
   * The id of this message, The same ID can only have one at the same time
   */
  id?: string;
  /**
   * automatically shut down after a few seconds, if <= 0 ,non automatic closure
   */
  duration?: number;
  /**
   * max instance in one time
   */
  maxStack?: number;
  /**
   * message content
   */
  content?: string;
}

export const MESSAGE_DEFAULT_CONFIG = new InjectionToken<MessageConfig>(
  'MESSAGE_DEFAULT_CONFIG',
);

export const MESSAGE_CONFIG = new InjectionToken<MessageConfig>(
  'MESSAGE_CONFIG',
);

export const MESSAGE_DEFAULT_CONFIG_PROVIDER = {
  provide: MESSAGE_DEFAULT_CONFIG,
  useValue: {
    duration: 6000,
    maxStack: 8,
  },
};
