import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../icon/icon.module';
import { MessageWrapperComponent } from './message-wrapper.component';
import { MessageComponent } from './message.component';
import { MESSAGE_DEFAULT_CONFIG_PROVIDER } from './message.config';
import { MessageService } from './message.service';

@NgModule({
  imports: [CommonModule, OverlayModule, PortalModule, IconModule],
  declarations: [MessageWrapperComponent, MessageComponent],
  entryComponents: [MessageWrapperComponent, MessageComponent],
  providers: [MessageService, MESSAGE_DEFAULT_CONFIG_PROVIDER],
})
export class MessageModule {}
