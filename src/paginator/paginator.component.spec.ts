import { Component, DebugElement, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PaginatorComponent, PaginatorModule } from './public-api';

describe('PaginatorComponent', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ins: TestComponent;
  let debugEl: DebugElement;
  let el: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PaginatorModule],
      declarations: [TestComponent],
    });

    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    ins = fixture.componentInstance;
    debugEl = fixture.debugElement.query(By.css('aui-paginator>div'));
    el = debugEl.nativeElement;
  });

  it('should render component', () => {
    expect(el.className).toContain('aui-paginator');
  });

  it('should render correct pages', () => {
    let pages;
    let previous;
    let next;

    ins.pageIndex = 0;
    ins.pageSize = 10;
    ins.length = 200;
    fixture.detectChanges();

    pages = debugEl.queryAll(By.css('.aui-paginator__page'));
    previous = debugEl.query(By.css('.aui-paginator__previous'));
    next = debugEl.query(By.css('.aui-paginator__next'));
    expect(pages.length).toBe(3);
    expect(previous.properties.disabled).toBeTruthy();
    expect(next.properties.disabled).toBeFalsy();

    ins.pageIndex = 2;
    fixture.detectChanges();
    pages = debugEl.queryAll(By.css('.aui-paginator__page'));
    previous = debugEl.query(By.css('.aui-paginator__previous'));
    next = debugEl.query(By.css('.aui-paginator__next'));
    expect(pages.length).toBe(5);
    expect(previous.properties.disabled).toBeFalsy();
    expect(next.properties.disabled).toBeFalsy();

    ins.pageIndex = 19;
    fixture.detectChanges();
    pages = debugEl.queryAll(By.css('.aui-paginator__page'));
    previous = debugEl.query(By.css('.aui-paginator__previous'));
    next = debugEl.query(By.css('.aui-paginator__next'));
    expect(pages.length).toBe(3);
    expect(previous.properties.disabled).toBeFalsy();
    expect(next.properties.disabled).toBeTruthy();
  });

  it('should render corrent page size options', () => {
    let selectedValueInput;

    ins.length = 200;
    ins.pageIndex = 0;
    ins.pageSize = 20;
    ins.pageSizeOptions = [10, 20, 50];
    fixture.detectChanges();

    selectedValueInput = debugEl.query(
      By.css('.aui-paginator__page-size aui-select input'),
    );
    expect(selectedValueInput.nativeElement.value).toBe('20');
  });

  it('shoud render corrent page by user interactive', () => {
    let page;

    ins.length = 200;
    ins.pageIndex = 0;
    ins.pageSize = 20;
    fixture.detectChanges();

    ins.nextPage();
    fixture.detectChanges();

    page = debugEl.query(By.css('.aui-paginator__page.aui-button--primary'));
    expect(page.nativeElement.textContent.trim()).toBe('2');

    ins.setPage(5);
    fixture.detectChanges();

    page = debugEl.query(By.css('.aui-paginator__page.aui-button--primary'));
    expect(page.nativeElement.textContent.trim()).toBe('6');

    ins.previousPage();
    fixture.detectChanges();

    page = debugEl.query(By.css('.aui-paginator__page.aui-button--primary'));
    expect(page.nativeElement.textContent.trim()).toBe('5');
  });
});

@Component({
  template: `
    <aui-paginator
      [pageIndex]="pageIndex"
      [pageSize]="pageSize"
      [length]="length"
      [pageSizeOptions]="pageSizeOptions"
      #paginator="aui-paginator"
    ></aui-paginator>
  `,
})
class TestComponent {
  pageIndex: number;
  pageSize: number;
  pageSizeOptions: number[];
  length: number;

  @ViewChild('paginator') paginator: PaginatorComponent;

  nextPage() {
    this.paginator.nextPage();
  }

  previousPage() {
    this.paginator.previousPage();
  }

  setPage(page: number) {
    this.paginator.setPage(page);
  }
}
