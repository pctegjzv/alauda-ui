import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { coerceAttrBoolean, coerceNumber } from '../utils/coercion';
import { PaginatorIntl } from './paginator-intl';

/** The default page size if there is no page size and there are no provided page size options. */
const DEFAULT_PAGE_SIZE = 20;

/**
 * Change event object that is emitted when the user selects a
 * different page size or navigates to another page.
 */
export class PageEvent {
  /** The current page index. */
  pageIndex: number;

  /**
   * Index of the page that was selected previously.
   * @deletion-target 7.0.0 To be made into a required property.
   */
  previousPageIndex?: number;

  /** The current page size */
  pageSize: number;

  /** The current total number of items being paged */
  length: number;
}

@Component({
  selector: 'aui-paginator',
  templateUrl: 'paginator.component.html',
  styleUrls: ['paginator.component.scss'],
  exportAs: 'aui-paginator',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class PaginatorComponent implements OnInit, OnDestroy {
  private _initialized = false;
  private _intlChanges: Subscription;

  /** The zero-based page index of the displayed list of items. Defaulted to 0. */
  @Input()
  get pageIndex(): number {
    return this._pageIndex;
  }
  set pageIndex(value: number) {
    this._pageIndex = Math.max(coerceNumber(value), 0);
    this.cdr.markForCheck();
  }
  _pageIndex = 0;

  /** The length of the total number of items that are being paginated. Defaulted to 0. */
  @Input()
  get length(): number {
    return this._length;
  }
  set length(value: number) {
    this._length = Math.max(coerceNumber(value), 0);
    this.cdr.markForCheck();
  }
  _length = 0;

  /** Number of items to display on a page. By default set to 20. */
  @Input()
  get pageSize(): number {
    return this._pageSize;
  }
  set pageSize(value: number) {
    this._pageSize = Math.max(coerceNumber(value), 0);
    this.updateDisplayedPageSizeOptions();
  }
  _pageSize: number;

  /** Number of items to display on a page. By default set to 50. */
  @Input()
  get pageSizeOptions(): number[] {
    return this._pageSizeOptions;
  }
  set pageSizeOptions(value: number[]) {
    this._pageSizeOptions = (value || []).map(p => coerceNumber(p));
    this.updateDisplayedPageSizeOptions();
  }
  _pageSizeOptions: number[] = [];

  /** Whether to hide the page size selection UI from the user. */
  @Input()
  get hidePageSize(): boolean {
    return this._hidePageSize;
  }
  set hidePageSize(value: boolean) {
    this._hidePageSize = coerceAttrBoolean(value);
  }
  _hidePageSize = false;

  /** Whether to show the first/last buttons UI to the user. */
  /** TODO: Not take effect for now. */
  @Input()
  get showFirstLastButtons(): boolean {
    return this._showFirstLastButtons;
  }
  set showFirstLastButtons(value: boolean) {
    this._showFirstLastButtons = coerceAttrBoolean(value);
    this.cdr.markForCheck();
  }
  _showFirstLastButtons = false;

  /** Event emitted when the paginator changes the page size or page index. */
  @Output()
  readonly page: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

  _displayedPageSizeOptions: number[];

  get pages(): number[] {
    const pageLength = this.getPageLength();
    if (pageLength <= 1) {
      return [];
    }
    const min = Math.max(0, this.pageIndex - 2);
    const max = Math.min(pageLength - 1, this.pageIndex + 2);

    return this.createRange(min, max);
  }

  constructor(public intl: PaginatorIntl, private cdr: ChangeDetectorRef) {
    this._intlChanges = intl.changes.subscribe(() => this.cdr.markForCheck());
  }

  ngOnInit() {
    this._initialized = true;
    this.updateDisplayedPageSizeOptions();
  }

  ngOnDestroy() {
    this._intlChanges.unsubscribe();
  }

  previousPage() {
    if (!this.hasPreviousPage()) {
      return;
    }
    const previousPageIndex = this.pageIndex;
    this.pageIndex--;
    this.emitPageEvent(previousPageIndex);
  }

  nextPage() {
    if (!this.hasNextPage()) {
      return;
    }
    const previousPageIndex = this.pageIndex;
    this.pageIndex++;
    this.emitPageEvent(previousPageIndex);
  }

  setPage(page: number) {
    if (!this.hasPage(page)) {
      return;
    }
    const previousPageIndex = this.pageIndex;
    this.pageIndex = page;
    this.emitPageEvent(previousPageIndex);
  }

  getPageLength() {
    return Math.ceil(this.length / this.pageSize);
  }

  hasPreviousPage(): boolean {
    return this.pageIndex >= 1 && this.pageSize !== 0;
  }

  hasPage(page: number): boolean {
    return page >= 0 && page < this.getPageLength();
  }

  hasNextPage(): boolean {
    const pageLength = this.getPageLength();
    return this.pageIndex < pageLength - 1 && this.pageSize !== 0;
  }

  changePageSize(pageSize: number) {
    // Current page needs to be updated to reflect the new page size. Navigate to the page
    // containing the previous page's first item.
    const startIndex = this.pageIndex * this.pageSize;
    const previousPageIndex = this.pageIndex;

    this.pageIndex = Math.floor(startIndex / pageSize) || 0;
    this.pageSize = pageSize;
    this.emitPageEvent(previousPageIndex);
  }

  private updateDisplayedPageSizeOptions() {
    if (!this._initialized) {
      return;
    }

    if (!this.pageSize) {
      this._pageSize =
        (this.pageSizeOptions.length && this.pageSizeOptions[0]) ||
        DEFAULT_PAGE_SIZE;
    }

    this._displayedPageSizeOptions = this.pageSizeOptions.slice();
    if (this._displayedPageSizeOptions.indexOf(this.pageSize) === -1) {
      this._displayedPageSizeOptions.push(this.pageSize);
    }

    this._displayedPageSizeOptions.sort((a, b) => a - b);

    if (!this.hasPage(this._pageIndex) && this._pageIndex > 0) {
      this._pageIndex = this.getPageLength() - 1;
    }

    this.cdr.markForCheck();
  }

  private emitPageEvent(previousPageIndex: number) {
    this.page.emit({
      previousPageIndex,
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      length: this.length,
    });
  }

  private createRange(start: number, end: number): number[] {
    if (start > end) {
      return [];
    }
    return [start, ...this.createRange(start + 1, end)];
  }
}
