import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../icon/public-api';
import { ButtonComponent } from './button.component';

@NgModule({
  imports: [CommonModule, IconModule],
  declarations: [ButtonComponent],
  exports: [ButtonComponent],
})
export class ButtonModule {}
