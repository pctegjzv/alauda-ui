import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IconModule } from '../icon/public-api';
import { TagModule } from '../tag/public-api';
import {
  InputAddonAfterDirective,
  InputAddonBeforeDirective,
  InputPrefixDirective,
  InputSuffixDirective,
} from './helper-directives';
import { InputGroupComponent } from './input-group/input-group.component';
import { InputComponent } from './input.component';
import { SearchComponent } from './search/search.component';
import { TagsInputComponent } from './tags-input/tags-input.component';

@NgModule({
  imports: [CommonModule, FormsModule, IconModule, TagModule],
  declarations: [
    InputComponent,
    InputGroupComponent,
    InputAddonBeforeDirective,
    InputAddonAfterDirective,
    InputPrefixDirective,
    InputSuffixDirective,
    SearchComponent,
    TagsInputComponent,
  ],
  exports: [
    InputComponent,
    InputGroupComponent,
    InputAddonBeforeDirective,
    InputAddonAfterDirective,
    InputPrefixDirective,
    InputSuffixDirective,
    SearchComponent,
    TagsInputComponent,
  ],
})
export class InputModule {}
