import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonForm } from '../../form/public-api';
import { ComponentSize } from '../../types';
import { Bem, buildBem } from '../../utils/bem';

@Component({
  selector: 'aui-tags-input',
  templateUrl: './tags-input.component.html',
  styleUrls: [
    '../../input/input.component.scss',
    '../../tag/tag.component.scss',
    './tags-input.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TagsInputComponent),
      multi: true,
    },
  ],
})
export class TagsInputComponent extends CommonForm<string[]> {
  bem: Bem = buildBem('aui-tags-input');

  @Input() placeholder = '';
  @Input() size = ComponentSize.Medium;
  @Input() allowRepeat = false;
  @Input() allowEmpty = false;
  @ViewChild('inputRef') inputRef: ElementRef;

  tags: string[] = [];
  focused = false;

  get rootClass() {
    const size = this.size || ComponentSize.Medium;
    return `aui-input ${this.bem.block(size)} ${
      this.disabled ? 'isDisabled' : ''
    } ${this.focused ? 'isFocused' : ''}`;
  }

  get tagSize() {
    if (this.size === ComponentSize.Medium) {
      return ComponentSize.Small;
    } else if (this.size === ComponentSize.Large) {
      return ComponentSize.Medium;
    } else {
      return ComponentSize.Mini;
    }
  }

  get inputClass() {
    return `${this.bem.element('input')} aui-tag aui-tag--${this.tagSize}`;
  }

  get displayPlaceholder() {
    return !this.tags.length && !this.focused;
  }

  constructor(cdr: ChangeDetectorRef, private renderer: Renderer2) {
    super(cdr);
  }

  writeValue(val: string[]) {
    this.tags = val || [];
    this.cdr.detectChanges();
  }

  onRemove(tag: string) {
    this.tags = this.tags.filter(item => item !== tag);
    this.emitValueChange(this.tags);
  }

  onInput() {
    // TODO: optimize performance
    this.renderer.removeStyle(this.inputRef.nativeElement, 'width');
    this.renderer.setStyle(
      this.inputRef.nativeElement,
      'width',
      this.inputRef.nativeElement.scrollWidth + 'px',
    );
  }

  onKeyDown(event: KeyboardEvent) {
    const inputEl = event.target as HTMLInputElement;
    if (event.key === 'Backspace' && inputEl.value === '') {
      this.onRemove(this.tags[this.tags.length - 1]);
      event.stopPropagation();
      event.preventDefault();
    } else if (event.key === 'Enter') {
      event.stopPropagation();
      event.preventDefault();
      if (!this.allowEmpty && !inputEl.value) {
        return;
      }
      if (!this.allowRepeat && this.tags.includes(inputEl.value)) {
        return;
      }
      this.tags.push(inputEl.value);
      inputEl.value = '';
      this.emitValueChange(this.tags);
    }
  }

  onInputFocus() {
    this.focused = true;
  }

  onInputBlur() {
    this.focused = false;
    if (this.onTouched) {
      this.onTouched();
    }
  }

  trackByValue(index: number, value: string) {
    return value;
  }
}
