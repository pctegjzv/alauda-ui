import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ViewEncapsulation,
} from '@angular/core';
import { Bem, buildBem } from '../../utils/bem';
import {
  InputAddonAfterDirective,
  InputAddonBeforeDirective,
  InputPrefixDirective,
  InputSuffixDirective,
} from '../helper-directives';
import { InputComponent } from '../input.component';

@Component({
  selector: 'aui-input-group',
  templateUrl: './input-group.component.html',
  styleUrls: ['./input-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class InputGroupComponent {
  bem: Bem = buildBem('aui-input-group');

  @ContentChild(InputAddonBeforeDirective)
  addonBeforeRef: InputAddonBeforeDirective;

  @ContentChild(InputAddonAfterDirective)
  addonAfterRef: InputAddonAfterDirective;

  @ContentChild(InputPrefixDirective) prefixRef: InputPrefixDirective;

  @ContentChild(InputSuffixDirective) suffixRef: InputSuffixDirective;

  @ContentChild(InputComponent) inputRef: InputComponent;
}
