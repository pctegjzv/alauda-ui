import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import debounce from 'lodash.debounce';
import { ComponentSize } from '../../types';
import { Bem, buildBem } from '../../utils/bem';

@Component({
  selector: 'aui-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class SearchComponent {
  bem: Bem = buildBem('aui-search');

  @Input() size = ComponentSize.Medium;
  @Input() searchButton = false;
  @Input() clearable = true;
  @Input() searching = false;
  @Input() placeholder = '';
  @Input() disabled = false;
  @Input()
  get debounce() {
    return this._debounce;
  }
  set debounce(value: number) {
    this._debounce = value;
    this.debouncedEmitKeyword = debounce(
      this.keywordChange.emit.bind(this.keywordChange),
      value,
    );
  }
  @Input()
  get keyword() {
    return this.value;
  }
  set keyword(value: string) {
    this.value = value;
  }
  @Output() keywordChange = new EventEmitter<string>();
  @Output() search = new EventEmitter<string>();
  @Output() clear = new EventEmitter<void>();

  value = '';
  private _debounce: number;
  private debouncedEmitKeyword = debounce(
    this.keywordChange.emit.bind(this.keywordChange),
    0,
  );

  get showSpinner() {
    return this.searching && !this.searchButton;
  }

  get showClear() {
    return !this.disabled && this.clearable && this.value && !this.showSpinner;
  }

  emitChange(value: string): void {
    this.value = value;
    this.debouncedEmitKeyword(value);
  }

  emitSearch(): void {
    if (this.disabled) {
      return;
    }
    this.search.emit(this.value);
  }

  emitClear(): void {
    if (this.disabled) {
      return;
    }
    this.value = '';
    this.keywordChange.emit('');
    this.search.emit('');
    this.clear.emit();
  }
}
