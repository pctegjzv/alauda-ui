import { Component, DebugElement, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { sleep } from '../../utils/async';
import { InputModule, SearchComponent } from '../public-api';

describe('SearchComponent', () => {
  let fixture: ComponentFixture<TestComponent>;
  let ins: TestComponent;
  let debugEl: DebugElement;
  let el: HTMLElement;
  let inputEl: HTMLInputElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [InputModule],
      declarations: [TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    ins = fixture.componentInstance;
    debugEl = fixture.debugElement.query(By.css('.aui-search'));
    el = debugEl.nativeElement;
    inputEl = el.querySelector('input');
  });

  it('should render correct construction', () => {
    expect(el).not.toBeNull();
    expect(el.className).toContain('hasIcon');
    const iconEl = el.querySelector('.aui-search__icon');
    expect(iconEl).not.toBeNull();
    expect(iconEl.querySelector('use').getAttribute('xlink:href')).toBe(
      '#aui-icon-search_s',
    );
    expect(inputEl).not.toBeNull();
    expect(inputEl.className).toContain('aui-input');
    expect(inputEl.className).toContain('aui-input--medium');
    expect(inputEl.placeholder).toBe('placeholder');
  });

  it('should searchButton option work correctly', () => {
    ins.searchButton = true;
    fixture.detectChanges();
    expect(el.className).not.toContain('hasIcon');
    expect(el.className).toContain('hasButton');
    const buttonEl = el.querySelector('.aui-search__button');
    expect(buttonEl).not.toBeNull();
    expect(buttonEl.querySelector('use').getAttribute('xlink:href')).toBe(
      '#aui-icon-search_s',
    );
  });

  it('should clearable option work correctly', () => {
    ins.clearable = true;
    fixture.detectChanges();
    expect(el.querySelector('.aui-search__clear').getAttribute('hidden')).toBe(
      '',
    );
    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const clearEl = el.querySelector('.aui-search__clear');
    expect(clearEl.getAttribute('hidden')).toBeNull();
    expect(clearEl.querySelector('use').getAttribute('xlink:href')).toBe(
      '#aui-icon-close_circle_8_s',
    );
  });

  it('should searching option work correctly', () => {
    ins.searching = true;
    fixture.detectChanges();
    expect(el.querySelector('.aui-search__spinner')).not.toBeNull();
    expect(
      el.querySelector('.aui-search__spinner use').getAttribute('xlink:href'),
    ).toBe('#aui-icon-spinner');

    ins.clearable = true;
    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(el.querySelector('.aui-search__spinner')).not.toBeNull();
    const clearEl = el.querySelector('.aui-search__clear');
    expect(clearEl.getAttribute('hidden')).toBe('');
    expect(clearEl.querySelector('use').getAttribute('xlink:href')).toBe(
      '#aui-icon-close_circle_8_s',
    );

    ins.searchButton = true;
    fixture.detectChanges();
    expect(el.querySelector('.aui-search__spinner')).toBeNull();
    expect(
      el.querySelector('.aui-search__clear').getAttribute('hidden'),
    ).toBeNull();
    expect(
      el.querySelector('.aui-search__button use').getAttribute('xlink:href'),
    ).toBe('#aui-icon-spinner');
  });

  it('should set inner value & input value when keyword changed', async done => {
    ins.keyword = 'keyword';
    fixture.detectChanges();
    await sleep();
    expect(ins.searchRef.keyword).toBe('keyword');
    expect(inputEl.value).toBe('keyword');
    done();
  });

  it('should emit change event when user input', done => {
    ins.searchRef.keywordChange.subscribe(($event: string) => {
      expect($event).toBe('text');
      done();
    });

    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();
  });

  it('should input element be disabled', async done => {
    ins.disabled = true;
    fixture.detectChanges();
    expect(el.className).toContain('isDisabled');
    await sleep();
    expect(inputEl.disabled).toBeTruthy();
    done();
  });

  it('should emit clear event', done => {
    ins.searchRef.clear.subscribe(() => {
      done();
    });

    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    ins.clearable = true;
    fixture.detectChanges();
    el.querySelector('.aui-search__clear').dispatchEvent(new Event('click'));
    expect(ins.searchRef.keyword).toBe('');
  });

  it('should not emit clear event when disabled', () => {
    ins.searchRef.clear.subscribe(() => {
      throw Error('should not emit clear event when disabled');
    });

    ins.disabled = true;
    ins.clearable = true;
    ins.keyword = 'text';
    fixture.detectChanges();
    el.querySelector('.aui-search__clear').dispatchEvent(new Event('click'));
    expect(ins.searchRef.keyword).toBe('text');
    expect(ins.keyword).toBe('text');
  });

  it('should emit search event when click search button', done => {
    ins.searchRef.search.subscribe(($event: string) => {
      expect($event).toBe('text');
      done();
    });

    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    ins.searchButton = true;
    fixture.detectChanges();
    el.querySelector('.aui-search__button').dispatchEvent(new Event('click'));
  });

  it('should not emit search event when click search button', () => {
    ins.searchRef.search.subscribe(() => {
      throw Error('should not emit search event when click search button');
    });

    ins.searchButton = true;
    ins.disabled = true;
    ins.keyword = 'text';
    fixture.detectChanges();
    el.querySelector('.aui-search__button').dispatchEvent(new Event('click'));
  });

  it('should emit search event when press enter', done => {
    ins.searchRef.search.subscribe(($event: string) => {
      expect($event).toBe('text');
      done();
    });

    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    ins.searchButton = true;
    fixture.detectChanges();
    inputEl.dispatchEvent(new KeyboardEvent('keyup', { key: 'enter' }));
  });

  it('should debounce option work correctly', async done => {
    ins.debounce = 10;
    inputEl.value = 'text';
    inputEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(ins.searchRef.keyword).toBe('text');
    expect(ins.keyword).not.toBe('text');
    await sleep(10);
    expect(ins.searchRef.keyword).toBe('text');
    expect(ins.keyword).toBe('text');
    done();
  });
});

@Component({
  template: `
    <aui-search
        #searchRef
        [searchButton]="searchButton"
        [searching]="searching"
        [clearable]="clearable"
        [disabled]="disabled"
        [debounce]="debounce"
        [(keyword)]="keyword"
        [placeholder]="placeholder">
    </aui-search>`,
})
class TestComponent {
  searchButton: boolean;
  searching: boolean;
  clearable: boolean;
  disabled: boolean;
  placeholder = 'placeholder';
  keyword = '';
  debounce = 0;

  @ViewChild('searchRef') searchRef: SearchComponent;
}
