import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[auiHint]',
})
export class HintDirective {
  @Input() auiHintAlign: 'start' | 'end' = 'start';
}

@Directive({
  selector: '[auiErrorHint]',
})
export class ErrorHintDirective {}

@Directive({
  selector: '[auiControlPrefix]',
})
export class ControlPrefixDirective {}

@Directive({
  selector: '[auiControlSuffix]',
})
export class ControlSuffixDirective {}
