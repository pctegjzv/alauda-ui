import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ContentChildren,
  HostBinding,
  Input,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { Bem, buildBem } from '../utils/bem';
import {
  ControlPrefixDirective,
  ControlSuffixDirective,
  ErrorHintDirective,
  HintDirective,
} from './helper-directives';

@Component({
  selector: 'aui-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class FormFieldComponent {
  bem: Bem = buildBem('aui-form-field');

  @Input() label = '';

  @Input() required = false;

  @Input() size: 'min' | 'full' = 'min';

  @Input() error: boolean;

  @HostBinding('class')
  get classname() {
    return this.bem.block({ required: this.required, error: this.error });
  }

  @ContentChildren(ControlPrefixDirective)
  prefixs: QueryList<ControlPrefixDirective>;
  @ContentChildren(ControlSuffixDirective)
  suffixs: QueryList<ControlSuffixDirective>;
  @ContentChildren(HintDirective) hints: QueryList<HintDirective>;
  @ContentChild(ErrorHintDirective) errorHint: ErrorHintDirective;
}
