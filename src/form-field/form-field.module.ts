import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormFieldComponent } from './form-field.component';
import {
  ControlPrefixDirective,
  ControlSuffixDirective,
  ErrorHintDirective,
  HintDirective,
} from './helper-directives';

const EXPORTABLES = [
  FormFieldComponent,
  ControlPrefixDirective,
  ControlSuffixDirective,
  HintDirective,
  ErrorHintDirective,
];

/**
 * @deprecated
 */
@NgModule({
  imports: [CommonModule],
  declarations: EXPORTABLES,
  exports: EXPORTABLES,
})
export class FormFieldModule {}
