const gulp = require('gulp');
const plumber = require('gulp-plumber');
const svgSprit = require('gulp-svg-sprite');

const iconFloderNames = ['basic'];

iconFloderNames.forEach(floder => {
  const config = {
    log: 'info',
    shape: {
      id: {
        separator: '',
        generator: `${floder}-%s`,
      },
    },
    mode: {
      symbol: {
        dest: '.',
        sprite: `${floder}-icons.svg`,
      },
    },
  };

  gulp
    .src(`assets/icons/${floder}-icon_*/*.svg`)
    .pipe(plumber())
    .pipe(svgSprit(config))
    .pipe(gulp.dest('release/assets'));
});
