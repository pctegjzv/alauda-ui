const gulp = require('gulp');
const plumber = require('gulp-plumber');
const svgSprit = require('gulp-svg-sprite');
const each = require('gulp-each');

const config = {
  log: 'info',
  shape: {
    id: {
      separator: '',
      generator: 'aui-icon-%s',
    },
  },
  mode: {
    symbol: {
      dest: '.',
      sprite: 'icons.ts',
    },
  },
};

gulp
  .src(`assets/icons/aui-icon_*/*.svg`)
  .pipe(plumber())
  .pipe(svgSprit(config))
  .pipe(
    each((content, file, callback) => {
      const newContent = `export const auiIcons = '${content}';`;
      callback(null, newContent);
    }),
  )
  .pipe(gulp.dest('src/icon'));
