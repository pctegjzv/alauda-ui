# Contributing

## Submitting a Pull Request

The following conditions must be met before merge to `master`:

* at least 1 approval
* the last commit was successfully built and no failed builds

## Commit Message

### Commit Message Format

```text
<type>(<scope>): <subject>
<blank line>
<body>
<blank line>
<footer>
```

### Type

Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (example: white-space, formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing tests or correcting existing tests
* **build**: Changes that affect the build system, CI configuration or external dependencies
* **chore**: Other changes that don't modify `src` or `test` files (example: stories)

### Scope

The scope could be anything specifying place of the commit change. For example `datepicker`, `dialog`, etc.

### Subject

The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body

Just as in the subject, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.

### Footer

The footer should contain any information about breaking changes and is also the place to reference jira task that this commit Closes.

Breaking changes should start with the word `BREAKING CHANGE`: with a space or two newlines. The rest of the commit message is then used for this.

## References

* [Contributing to Angular Material](https://github.com/angular/material2/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
